# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

### Fixed

-->

## 3.2.0 - 2024-11-21

### Changed

- Affect a radiometry to a slab only possible when K2 is connected
- Gamma measurements are corrected with dead-time

### Fixed

- Radiometry computation corrected

## 3.1.0 - 2024-09-25

### Added

- Radiometry classes are now managed in PostgreSQL database to accelerate layer rendering
- Re-added pan and zoom buttons on mobile version
- Added CtrlCraft version number on atlas pages and window title
- Increase overall font size on mobile version

### Changed

- Changed `RTKube` to `K2`
- Changed labels on slab grade panel
- UI/UX improvements in mobile mode (buttons and font size to fit the tablet screen)
- Update documentation (user and technical)
- Update some dependencies

### Removed

- Removed legend toolbar
- Removed estimated blocs layer
- Removed estimated blocs import and gate import menus
- Removed estimated graphics and table
- Removed the tile scale panel widget

### Fixed

- Layer blinking or disappearing while K2 is connected

## 3.0.0 - 2024-07-09

- message bar to user to suggest disconnection for creating a landmark point
- log-log radiometry computation and its new parameters

### Fixed

- correction of radiometry_probe computation
- big icons in mobile version are back
- remove warning message bar about open sans font not available
- Charts and tables toolbar removed from mobile version (only for admin version)

## 2.7.0 - 2024-04-15

### Added

- charts and tables reporting tools are back
- new sql view "measurements_slabs_not_loaded"
- probe X,Y,Z are now available in NSah custom projection in dedicated tab
- CI packaging and auto upload of binary installer
- Unit and functionnal tests for configuration files and probe connection simulation
- PDF documentation

### Changed

- Updated QGIS version (now 3.34 LTR)
- Updated Python version (now 3.12) and other dependencies (from OSGeo4W packaging)
- New formula computation for radiometry (added log and normalization coefficients + threshold strategy)
- No more default values for gamma coefficients

### Fixed

- -999 radiometry slabs are now correctly imported

## 2.6.4 - 2024-01-29

### Fixed

- Landmark points don't crash any more

### Added

- -999 is now the undefined class (white symbology)

### Changed

- Export database only and always in WGS84

## 2.6.3 - 2023-09-27

### Fixed

- Problem when importing a database

## 2.6.1 - 2023-09-26

### Fixed

- Truck number can now REALLY be alpha-numeric
- Logging problems

### Added

- QGIS 3.28 minimum requirement
- Packaging scripts

## 2.6.0 - 2023-05-31

### Added

- Radiometry value of -999 has a white symbology
- Truck number can now be alpha-numeric

## 2.5.0 - 2023-02-01

### Added

- Pylint refactoring
- Unit testing
- CI/CD for GitLab
- Logging
- Documentation

### Removed

- 3D view
- reporting


## 2.4.4 - 2023-02-01

- First release
- Generated with the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/)
