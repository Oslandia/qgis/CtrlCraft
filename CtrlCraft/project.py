# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

"""
CtrlCraftProject module
"""

import os
import re
from pathlib import Path
from shutil import copyfile

from qgis.core import QgsLayerTreeNode, QgsProject, QgsVectorLayer
from qgis.PyQt.QtGui import QColor

from CtrlCraft import __about__
from CtrlCraft.utils import misc


class CtrlCraftProject:
    """CtrlCraft QGIS project manager"""

    def __init__(self):
        qgisDirname = Path(__file__).absolute().parent / "qgis"
        self.__template = qgisDirname / "template.qgs"
        self.__project = qgisDirname / "ctrlcraft.qgs"
        self.landmarkLayer = None
        self.tr = misc.tr

    def equal(self, filename):
        return self.__project == filename

    def exists(self):
        return self.__project.exists()

    def create(self):
        if not self.exists():
            copyfile(self.__template, self.__project)

    def reset(self):
        self.remove()
        self.create()

    def close(self):
        QgsProject.instance().removeAllMapLayers()
        QgsProject.instance().clear()

    def load(self):
        QgsProject.instance().setFileName(str(self.__project))
        QgsProject.instance().read()
        QgsProject.instance().setTitle("CtrlCraft version " + __about__.__version__)
        self.resetLandmarkLayer()

    def resetLandmarkLayer(self):
        # Find and remove temp layer with translation
        findLayers = QgsProject.instance().mapLayersByName(self.tr("Landmark points"))
        if len(findLayers) == 1:
            QgsProject.instance().removeMapLayer(findLayers[0])
        elif len(findLayers) > 1:
            raise ValueError(self.tr("Error on landmark layer: more than one layer!"))

        # Find and remove temp layer without translation
        findLayers = QgsProject.instance().mapLayersByName("Landmark points")
        if len(findLayers) == 1:
            QgsProject.instance().removeMapLayer(self.landmarkLayer)
        elif len(findLayers) > 1:
            raise ValueError(self.tr("Error on landmark layer: more than one layer!"))

        # Add temp layer
        self.landmarkLayer = QgsVectorLayer("Point", self.tr("Landmark points"), "memory")
        self.landmarkLayer.renderer().symbol().setColor(QColor("black"))
        QgsProject.instance().addMapLayer(self.landmarkLayer)

    def remove(self):
        self.close()
        os.remove(self.__project)

    def isLoaded(self):
        return self.__project.as_posix() == QgsProject.instance().fileName()

    @staticmethod
    def layer(name):
        """
        Returns the corresponding vector layer.
        """

        if not CtrlCraftProject().isLoaded():
            return None

        for lyr in list(QgsProject.instance().mapLayers().values()):
            source = lyr.source()
            if source[:6] == "dbname":
                source = source.replace("'", '"')
                vals = dict(re.findall(r'(\S+)="?(.*?)"? ', source))
                table = vals["table"].split(".")
                schemaName = table[0].strip('"')
                tableName = table[1].strip('"')
                table = "{}.{}".format(schemaName, tableName)

                if table == name:
                    return lyr

        return None

    @staticmethod
    def slabsLayer():
        """
        Returns the slabs layer
        """

        return CtrlCraftProject.layer("ctrlcraft.slabs_available")

    @staticmethod
    def probeLayer():
        """
        Returns the probe (RTKube) layer
        """

        return CtrlCraftProject.layer("ctrlcraft.probe_last_positions")

    @staticmethod
    def topoLayer():
        """
        Returns the topo_control layer
        """

        return CtrlCraftProject.layer("ctrlcraft.topo_control")

    @staticmethod
    def co3Layer():
        """
        Returns the co3 QgsVectorLayer
        """

        return CtrlCraftProject.layer("ctrlcraft.co3")

    @staticmethod
    def levelsLayer():
        """
        Returns the levels QgsVectorLayer
        """

        return CtrlCraftProject.layer("ctrlcraft.levels")

    @staticmethod
    def setDepositGroupName(name):
        """
        Sets the name of the layers group in the layers panel with the deposit name
        """

        children = QgsProject.instance().layerTreeRoot().children()

        for child in children:
            if child.nodeType() == QgsLayerTreeNode.NodeGroup:
                child.setName(name)
                return
