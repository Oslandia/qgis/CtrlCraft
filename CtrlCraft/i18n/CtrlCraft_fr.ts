<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>CtrlCraft</name>
    <message>
        <location filename="../ctrlcraft.py" line="354"/>
        <source>Undo</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="451"/>
        <source>CtrCraft Office Shipments</source>
        <translation>CtrlCraft Cargaisons</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="457"/>
        <source>CtrCraft Office Shipments Graph</source>
        <translation>CtrlCraft Graphiques des cargaisons</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="854"/>
        <source>Load the truck </source>
        <translation>Charger le camion </translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="861"/>
        <source>Unload the truck </source>
        <translation>Décharger le camion </translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="888"/>
        <source>Set lithology </source>
        <translation>Appliquer la lithologie </translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="328"/>
        <source>Deposits</source>
        <translation>Gisements</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="336"/>
        <source>Panels</source>
        <translation>Panneaux</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="344"/>
        <source>Blasts</source>
        <translation>Tirs</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="413"/>
        <source>Open Project</source>
        <translation>Ouvrir projet</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="414"/>
        <source>Reset Project</source>
        <translation>Réinitialiser projet</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="607"/>
        <source>Clear Database</source>
        <translation>Effacer la base de donnée</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="420"/>
        <source>Import Slabs</source>
        <translation>Importer des slabs</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="429"/>
        <source>Import Database</source>
        <translation>Importer une base de donnée</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="421"/>
        <source>Import CO3</source>
        <translation>Importer carbonates</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="430"/>
        <source>Export Database</source>
        <translation>Exporter la base de donnée</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="619"/>
        <source>Clearing database...</source>
        <translation>Suppression en cours...</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="658"/>
        <source>Affect the grade on a slab</source>
        <translation>Appliquer la teneur à une slab</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="402"/>
        <source>Select a truck</source>
        <translation>Sélectionner un camion</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="434"/>
        <source>Export Trucks Loads Status</source>
        <translation>Exporter les status de chargement des camions</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="445"/>
        <source>Exit QGIS</source>
        <translation>Fermer QGIS</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="868"/>
        <source>Probe grade for truck </source>
        <translation>Teneur sonde du chargement </translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="123"/>
        <source>office</source>
        <translation>bureau</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="125"/>
        <source>mobile</source>
        <translation>mobile</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="126"/>
        <source>Initialize CtrlCraft plugin </source>
        <translation>Initialisation de l&apos;extension CtrlCraft </translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="442"/>
        <source>Export Atlas PDF</source>
        <translation>Export de l&apos;atlas en PDF</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="424"/>
        <source>Filter levels</source>
        <translation>Filtre de niveau</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="759"/>
        <source>Remove levels</source>
        <translation>Supprimer les niveaux</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="782"/>
        <source>Revert removed levels</source>
        <translation>Revenir sur les niveaux supprimés</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="760"/>
        <source>Remove levels:</source>
        <translation>Supprimer les niveaux jusqu&apos;au:</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="783"/>
        <source>Revert removed levels:</source>
        <translation>Revenir sur les niveaux supprimés depuis:</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="416"/>
        <source>Define slab threshold symbology</source>
        <translation>Définir la symbologie des slabs</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="379"/>
        <source>Add a landmark point</source>
        <translation>Ajouter un point de repère</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="384"/>
        <source>Remove last landmark point</source>
        <translation>Supprimer le dernier point de repère</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="389"/>
        <source>Clear all landmark points</source>
        <translation>Supprimer tous les points de repère</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="695"/>
        <source>Landmark point added</source>
        <translation>Point de repère ajouté</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="705"/>
        <source>Last landmark point removed</source>
        <translation>Dernier point de repère supprimé</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="707"/>
        <source>No landmark point to remove</source>
        <translation>Aucun point de repère à supprimer</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="717"/>
        <source>Landmark points cleared</source>
        <translation>Tous les points de repère ont été supprimés</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="719"/>
        <source>No landmark point to clear</source>
        <translation>Aucun point de repère à supprimer</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="437"/>
        <source>Export K2 data to CSV</source>
        <translation>Edporter les données K2 en CSV</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="608"/>
        <source>Are you sure you want to delete the database? Underlying data will be lost and K2 connection will be closed.</source>
        <translation>Êtes-vous sûr de vouloir effacer la baes de données ? Les couches seront perdues et la connexion K2 sera fermée.</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="1030"/>
        <source>Old database</source>
        <translation>Ancien dump</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="1030"/>
        <source>The database is from an old dump
and is not compatible with CtrlCraft &gt;= 3.1.0.</source>
        <translation>La base de données est d&apos;un ancien dump
et n&apos;est pas compatible avec CtrlCraft &gt;= 3.1.0.</translation>
    </message>
    <message>
        <location filename="../ctrlcraft.py" line="658"/>
        <source>K2 must be connected</source>
        <translation>Le K2 doit être connecté</translation>
    </message>
</context>
<context>
    <name>CtrlCraftAffectRadiometrySlab</name>
    <message>
        <location filename="../widgets/affect_radiometry_slab.py" line="95"/>
        <source>Acquiring data</source>
        <translation>Acquisition de la donnée</translation>
    </message>
    <message>
        <location filename="../widgets/affect_radiometry_slab.py" line="114"/>
        <source>You can now point the slab</source>
        <translation>Vous pouvez à présent pointer le slab</translation>
    </message>
    <message>
        <location filename="../widgets/affect_radiometry_slab.py" line="125"/>
        <source>Up probe: </source>
        <translation>Sonde haute : </translation>
    </message>
    <message>
        <location filename="../widgets/affect_radiometry_slab.py" line="125"/>
        <source>Down probe: </source>
        <translation>Sonde basse : </translation>
    </message>
</context>
<context>
    <name>CtrlCraftDatabaseExport</name>
    <message>
        <location filename="../widgets/database_export.py" line="36"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <location filename="../widgets/database_export.py" line="66"/>
        <source>Database correctly exported.</source>
        <translation>Export de la base de données réussi.</translation>
    </message>
    <message>
        <location filename="../widgets/database_export.py" line="47"/>
        <source>Dump file</source>
        <translation>Fichier dump</translation>
    </message>
    <message>
        <location filename="../widgets/database_export.py" line="61"/>
        <source>Exporting database...</source>
        <translation>Export de la base de données...</translation>
    </message>
</context>
<context>
    <name>CtrlCraftDatabaseImport</name>
    <message>
        <location filename="../widgets/database_import.py" line="75"/>
        <source>Import database...</source>
        <translation>Import de la base de donnée...</translation>
    </message>
    <message>
        <location filename="../widgets/database_import.py" line="107"/>
        <source>Database correctly imported.</source>
        <translation>Import de la base de donnée réussi.</translation>
    </message>
    <message>
        <location filename="../widgets/database_import.py" line="60"/>
        <source>Dump file</source>
        <translation>Fichier dump</translation>
    </message>
    <message>
        <location filename="../widgets/database_import.py" line="93"/>
        <source>Old dump</source>
        <translation>Ancien dump</translation>
    </message>
    <message>
        <location filename="../widgets/database_import.py" line="93"/>
        <source>The imported dump is not compatible with CtrlCraft &gt;= 3.1.0.</source>
        <translation>Le dump importé n&apos;est pas compatible
avec CtrlCraft &gt;= 3.1.0.</translation>
    </message>
</context>
<context>
    <name>CtrlCraftDialogBase</name>
    <message>
        <location filename="../widgets/ui/chart.ui" line="14"/>
        <source>CtrlCraft Office</source>
        <translation>CtrlCraft Office</translation>
    </message>
    <message>
        <location filename="../widgets/ui/export_db.ui" line="30"/>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../widgets/ui/export_db.ui" line="39"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../widgets/ui/export_db.ui" line="74"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../widgets/ui/import_db.ui" line="117"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../widgets/ui/import.ui" line="61"/>
        <source>Custom Projection</source>
        <translation>Translation</translation>
    </message>
    <message>
        <location filename="../widgets/ui/export_db.ui" line="81"/>
        <source>Export</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CtrlCraftMainParametersDock</name>
    <message>
        <location filename="../widgets/main_parameters_dock.py" line="164"/>
        <source>Truck ID</source>
        <translation>ID camion</translation>
    </message>
    <message>
        <location filename="../widgets/main_parameters_dock.py" line="192"/>
        <source>Truck </source>
        <translation>Camion </translation>
    </message>
    <message>
        <location filename="../widgets/main_parameters_dock.py" line="121"/>
        <source>Correlation coefficients</source>
        <translation>Coefficients de corrélation</translation>
    </message>
    <message>
        <location filename="../widgets/main_parameters_dock.py" line="164"/>
        <source>&apos;{0}&apos; is not a valid name:
Truck ID must be composed of alphanumeric upper case characters.</source>
        <translation>&apos;{0}&apos; n&apos;est pas un nom valide:
L&apos;id camion doit être composé de caractères alphanumériques en majuscule.</translation>
    </message>
    <message>
        <location filename="../widgets/main_parameters_dock.py" line="135"/>
        <source>Normalization coefficients</source>
        <translation>Coefficients de normalisation</translation>
    </message>
</context>
<context>
    <name>CtrlCraftOfficeCO3Import</name>
    <message>
        <location filename="../widgets/office_co3_import.py" line="121"/>
        <source>{} co3 holes imported with success ({} ignored)!</source>
        <translation>{} co3 importés avec succès ({} ignorés)!</translation>
    </message>
</context>
<context>
    <name>CtrlCraftOfficeShipmentsChart</name>
    <message>
        <location filename="../widgets/office_shipments_chart.py" line="79"/>
        <source>Gate grade</source>
        <translation>Teneur au portique</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_chart.py" line="81"/>
        <source>Correlation between probe and gate</source>
        <translation>Corrélation entre la sonde et le portique</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_chart.py" line="80"/>
        <source>Slabs grade (estimated: red, probe: blue)</source>
        <translation>Teneur des slabs (estimée: rouge, sonde: bleu)</translation>
    </message>
</context>
<context>
    <name>CtrlCraftOfficeShipmentsTable</name>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="64"/>
        <source>Slabs</source>
        <translation>Slabs</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="64"/>
        <source>Deposit</source>
        <translation>Gisement</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="64"/>
        <source>Panel</source>
        <translation>Panel</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="64"/>
        <source>Blast</source>
        <translation>Blast</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="64"/>
        <source>Truck</source>
        <translation>Camion</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="64"/>
        <source>Unload Datetime</source>
        <translation>Date / heure de déchargement</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="79"/>
        <source>Number of slabs in the shipment</source>
        <translation>Nombre de slabs dans la cargaison</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="82"/>
        <source>Deposit where slabs have been taken</source>
        <translation>Gisement d&apos;origine des slabs</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="85"/>
        <source>Panel where slabs have been taken</source>
        <translation>Panel d&apos;origine des slabs</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="88"/>
        <source>Blast where slabs have been taken</source>
        <translation>Blast d&apos;origine des slabs</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="91"/>
        <source>Name of the truck</source>
        <translation>Identifiant du camion</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="94"/>
        <source>Datetime of the unloading</source>
        <translation>Date / heure de déchargement</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="97"/>
        <source>Mean estimated grade for all slabs in the shipment</source>
        <translation>Teneur slab trou de tir</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="100"/>
        <source>Mean probe grade for all slabs in the shipment</source>
        <translation>Teneur slab AP</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="103"/>
        <source>Grade of the shipment measured by the gate</source>
        <translation>Teneur de la cargaison mesurée par la porte</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="64"/>
        <source>Mean estimated grade</source>
        <translation>Teneur moyenne estimée</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="64"/>
        <source>Mean probe grade</source>
        <translation>Teneur moyenne sonde</translation>
    </message>
    <message>
        <location filename="../widgets/office_shipments_table.py" line="64"/>
        <source>Gate grade</source>
        <translation>Teneur au portique</translation>
    </message>
</context>
<context>
    <name>CtrlCraftOfficeSlabsImport</name>
    <message>
        <location filename="../widgets/office_slabs_import.py" line="120"/>
        <source>{} slabs imported with success ({} ignored)!</source>
        <translation>{} slabs importés avec succès ({} ignorés)!</translation>
    </message>
</context>
<context>
    <name>CtrlCraftProject</name>
    <message>
        <location filename="../project.py" line="80"/>
        <source>Landmark points</source>
        <translation>Points de repère</translation>
    </message>
    <message>
        <location filename="../project.py" line="77"/>
        <source>Error on landmark layer: more than one layer!</source>
        <translation>Erreur de couche de repères : plus d&apos;une couche !</translation>
    </message>
</context>
<context>
    <name>CtrlCraftSlabThreshold</name>
    <message>
        <location filename="../widgets/slab_threshold.py" line="48"/>
        <source>Slab thresholds</source>
        <translation>Limite valeur slab</translation>
    </message>
    <message>
        <location filename="../widgets/slab_threshold.py" line="98"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../widgets/slab_threshold.py" line="98"/>
        <source>Maximum must be greater than minimum for class {} - {}!</source>
        <translation>Le maximum doit être plus grand que le minimum pour la class {} - {} !</translation>
    </message>
</context>
<context>
    <name>CtrlCraftTruck</name>
    <message>
        <location filename="../truck.py" line="144"/>
        <source>No fixed measure</source>
        <translation>Absence de mesure fixe</translation>
    </message>
    <message>
        <location filename="../truck.py" line="140"/>
        <source>This slab has no fixed measure associated.
Do you really want to load it without a fixed measure ?</source>
        <translation>Attention pas de mesure fixe réalisé sur ce slab !
Êtes-vous sûr de vouloir le charger dans le camion ?</translation>
    </message>
</context>
<context>
    <name>CtrlCraftTruckLoadMapTool</name>
    <message>
        <location filename="../truck.py" line="55"/>
        <source>This truck is filled, you have to empty it first.</source>
        <translation>Le camion est rempli, vous devez le vider d&apos;abord.</translation>
    </message>
    <message>
        <location filename="../truck.py" line="60"/>
        <source>The truck </source>
        <translation>Le camion </translation>
    </message>
    <message>
        <location filename="../truck.py" line="60"/>
        <source> is filled.</source>
        <translation> est rempli.</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../widgets/ui/custom_edition.ui" line="20"/>
        <source>Custom Projection Edition</source>
        <translation>Edition des translations</translation>
    </message>
    <message>
        <location filename="../widgets/ui/custom_edition.ui" line="44"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../widgets/ui/custom_edition.ui" line="51"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../widgets/ui/affect_radiometry_slab.ui" line="14"/>
        <source>Dialog</source>
        <translation>Fenêtre</translation>
    </message>
    <message>
        <location filename="../widgets/ui/affect_radiometry_slab.ui" line="23"/>
        <source>Gamma probe :</source>
        <translation>Sonde gamma :</translation>
    </message>
    <message>
        <location filename="../widgets/ui/affect_radiometry_slab.ui" line="57"/>
        <source>Up</source>
        <translation>Haute</translation>
    </message>
    <message>
        <location filename="../widgets/ui/affect_radiometry_slab.ui" line="62"/>
        <source>Down</source>
        <translation>Basse</translation>
    </message>
    <message>
        <location filename="../widgets/ui/level_filter.ui" line="14"/>
        <source>Level filter</source>
        <translation>Filtre de niveau</translation>
    </message>
    <message>
        <location filename="../widgets/ui/level_filter.ui" line="20"/>
        <source>Show at top level:</source>
        <translation>Montrer jusqu&apos;au niveau :</translation>
    </message>
</context>
<context>
    <name>DockWidget</name>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="29"/>
        <source>CtrlCraft Parameters</source>
        <translation>CtrlCraft Paramètres</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="64"/>
        <source>Trucks</source>
        <translation>Camions</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="111"/>
        <source>Truck ID</source>
        <translation>ID camion</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="121"/>
        <source>Slabs</source>
        <translation>Slabs</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="145"/>
        <source>Rover</source>
        <translation>Pivot</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="178"/>
        <source>GPS mode :</source>
        <translation>Mode GPS :</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="160"/>
        <source>Acquisition max
angle (degree) :</source>
        <translation>Angle maximum
d&apos;acquisition (degré) :</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="186"/>
        <source>Gamma</source>
        <translation>Gamma</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="209"/>
        <source>Communication ports</source>
        <translation>Ports de communication</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="215"/>
        <source>Base com port:</source>
        <translation>Port de la base :</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="228"/>
        <source>Spare com port:</source>
        <translation>Port du module de secours :</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="235"/>
        <source>Pivot com port:</source>
        <translation>Port du pivot :</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="246"/>
        <source>Projection settings</source>
        <translation>Paramètres de projection</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="252"/>
        <source>Edit custom projection</source>
        <translation>Editer les translations</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="44"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="51"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../widgets/ui/probe_radiometry.ui" line="20"/>
        <source>Slab grade</source>
        <translation>Teneur des slabs</translation>
    </message>
    <message>
        <location filename="../widgets/ui/probe_radiometry.ui" line="44"/>
        <source>Blast grade</source>
        <translation>Teneur tir</translation>
    </message>
    <message>
        <location filename="../widgets/ui/probe_radiometry.ui" line="71"/>
        <source>CanOp grade</source>
        <translation>Teneur CanOp</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="74"/>
        <source>Number of trucks :</source>
        <translation>Nombre de camions :</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="192"/>
        <source>Edit Gamma correlation
coefficients</source>
        <translation>Editer les coefficients
de corrélation gamma</translation>
    </message>
    <message>
        <location filename="../widgets/ui/main_parameters.ui" line="200"/>
        <source>Edit Gamma normalization
coefficients</source>
        <translation>Editer les coefficients
de normalisation gamma</translation>
    </message>
</context>
<context>
    <name>ExportAtlas</name>
    <message>
        <location filename="../widgets/export_atlas.py" line="43"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../widgets/export_atlas.py" line="43"/>
        <source>Output destination does not exists!</source>
        <translation>Le dossier de destination n&apos;existe pas !</translation>
    </message>
    <message>
        <location filename="../widgets/export_atlas.py" line="54"/>
        <source>Atlas successfully exported to &lt;a href=&apos;file:///{0}&apos;&gt;{0}&lt;/a&gt;</source>
        <translation>Atlas exporté avec succès vers &amp;lt;a href=&apos;file:///{0}&apos;&amp;gt;{0}&amp;lt;/a&amp;gt;</translation>
    </message>
    <message>
        <location filename="../widgets/export_atlas.py" line="58"/>
        <source>Error during export atlas: {err[1]}</source>
        <translation>Erreur pendant l&apos;export de l&apos;atlas : {err[1]}</translation>
    </message>
</context>
<context>
    <name>RTKube</name>
    <message>
        <location filename="../rtkube/rtkube.py" line="173"/>
        <source>Gamma coefficients error</source>
        <translation>Erreur de coefficients gamma</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube.py" line="173"/>
        <source>For deposit {0}:
Missing values (deposit or coefficient(s)), or wrong keywords, or wrong formula in gamma coefficients parameter file.</source>
        <translation>Pour le gisement {0} :
Valeurs manquantes (gisement ou coefficient(s)), ou mauvais mots-clés, ou mauvais format de formule dans le fichier de configuration gamma.</translation>
    </message>
</context>
<context>
    <name>RTKubeDockWidget</name>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.py" line="316"/>
        <source>Max inclination reached</source>
        <translation>Inclinaison maximum atteinte</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.py" line="361"/>
        <source>Confirm disconnection</source>
        <translation>Confirmer déconnexion</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.py" line="379"/>
        <source>Disconnection</source>
        <translation>Déconnexion</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.py" line="381"/>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.py" line="293"/>
        <source>Gamma values are identical since more than 2 seconds</source>
        <translation>Attention, mesures radiométriques figées</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.py" line="244"/>
        <source>Gamma probe serial number {0} is missing in normalization parameter file</source>
        <translation>Le numéro de série de sonde gamma {0} est absent du fichier de paramètres de normalisation.</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.py" line="253"/>
        <source>A parameter value is not a number in normalization parameter file for serial number {0}</source>
        <translation>Un paramètre n&apos;est pas un nombre dans le fichier de paramètres de normalisation pour le numéro de série {0}</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.py" line="260"/>
        <source>N_sonde_haute or basse is missing in normalization parameter file for serial number {0}</source>
        <translation>N_sonde_haute ou basse est manquant dans le fichier de normalisation pour le numéro de série {0}</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.py" line="214"/>
        <source>Incoherent data. Data is {} precision, but rover settings has {} precision (see settings panel).</source>
        <translation>Données incohérentes. Précision {} reçue, mais la base est paramétrée en précision {} (voir panneau des paramètres).</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.py" line="361"/>
        <source>Do you want to disconnect the CAN-OP?</source>
        <translation>Voulez-vous déconnecter le CAN-OP ?</translation>
    </message>
</context>
<context>
    <name>RTKubeDockWidgetBase</name>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="25"/>
        <source>GNSS view</source>
        <translation>Vue GNSS</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="31"/>
        <source>Calculation:</source>
        <translation>Calcul:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="38"/>
        <source>Planimetrical precision:</source>
        <translation>Précision planimétrique:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="59"/>
        <source>Date:</source>
        <translation>Date:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="80"/>
        <source>Time:</source>
        <translation>Heure:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="108"/>
        <source>Altimetric precision:</source>
        <translation>Précision altimétrique:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="119"/>
        <source>WGS84 Long - lat</source>
        <translation>WGS84 Long - lat</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="171"/>
        <source>UTM 32N WGS84</source>
        <translation>UTM 32N WGS84</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="125"/>
        <source>Lat:</source>
        <translation>Lat:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="139"/>
        <source>Lon:</source>
        <translation>Lon:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="153"/>
        <source>Alt:</source>
        <translation>Alt:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="184"/>
        <source>Z:</source>
        <translation>Z:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="191"/>
        <source>Y:</source>
        <translation>Y:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="212"/>
        <source>X:</source>
        <translation>X:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="285"/>
        <source>Gamma view</source>
        <translation>Vue Gamma</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="291"/>
        <source>Grade (ppm)</source>
        <translation>Teneur (ppm)</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="340"/>
        <source>Gamma 1 (up):</source>
        <translation>Gamma 1 (haut):</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="357"/>
        <source>Gamma 2 (low):</source>
        <translation>Gamma 2 (bas):</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="334"/>
        <source>CPS</source>
        <translation>CPS</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="393"/>
        <source>Inclination:</source>
        <translation>Inclinaison:</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="473"/>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="220"/>
        <source>NSah</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="226"/>
        <source>Z (NSah)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="240"/>
        <source>Y (NSah)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="254"/>
        <source>X (NSah)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="268"/>
        <source>WARNING!
No translation configuration
found for selected deposit!</source>
        <translation>ATTENTION !
Pas de configuration de translations
trouvée pour le dépôt sélectionné !</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="415"/>
        <source>Bottom Z &lt;strong&gt;(WGS84)&lt;/strong&gt;:</source>
        <translation>Z de la sonde basse &lt;strong&gt;(WGS84)&lt;/strong&gt; :</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="439"/>
        <source>Bottom Z &lt;strong&gt;(NSah)&lt;/strong&gt;:</source>
        <translation>Z de la sonde basse &lt;strong&gt;(NSah)&lt;/strong&gt; :</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_dockwidget.ui" line="14"/>
        <source>K2</source>
        <translation>K2</translation>
    </message>
</context>
<context>
    <name>RtkubeSerial</name>
    <message>
        <location filename="../rtkube/rtkube_serial.py" line="131"/>
        <source>Cannot connect to bluetooth device nor spare : {0} {1}</source>
        <translation>Impossible de se connecter : ni au pivot ni au module de secours :{0} {1}</translation>
    </message>
</context>
<context>
    <name>RtkubeSingleMeas</name>
    <message>
        <location filename="../rtkube/rtkube_serial.py" line="335"/>
        <source>Landmark point error: make sure K2 connection is not running</source>
        <translation>Erreur point de repère : vérifier que la connexion K2 est désactivée</translation>
    </message>
    <message>
        <location filename="../rtkube/rtkube_serial.py" line="355"/>
        <source>Single Gamma error: make sure K2 connection is not running</source>
        <translation>Erreur gamma unique : vérifier que la connexion K2 est désactivée</translation>
    </message>
</context>
<context>
    <name>self.affect</name>
    <message>
        <location filename="../widgets/affect_radiometry_slab.py" line="45"/>
        <source>Up probe: </source>
        <translation>Sonde haute : </translation>
    </message>
    <message>
        <location filename="../widgets/affect_radiometry_slab.py" line="45"/>
        <source>Down probe: </source>
        <translation>Sonde basse : </translation>
    </message>
</context>
</TS>
