FORMS = ../widgets/ui/custom_edition.ui \
../widgets/ui/import.ui \
../widgets/ui/import_db.ui \
../widgets/ui/export_db.ui \
../widgets/ui/main_parameters.ui \
../widgets/ui/probe_radiometry.ui \
../widgets/ui/affect_radiometry_slab.ui \
../widgets/ui/level_filter.ui \
../rtkube/rtkube_dockwidget.ui \
../widgets/ui/base.ui \
../widgets/ui/chart.ui

SOURCES = ../ctrlcraft.py \
../truck.py \
../project.py \
../data.py \
../widgets/custom_edition.py \
../widgets/database_export.py \
../widgets/database_import.py \
../widgets/main_parameters_dock.py \
../widgets/slab_threshold.py \
../widgets/office_co3_import.py \
../widgets/export_atlas.py \
../widgets/office_slabs_import.py \
../widgets/probe_radiometry_dock.py \
../widgets/affect_radiometry_slab.py \
../widgets/level_filter.py \
../rtkube/rtkube.py \
../rtkube/rtkube_dockwidget.py \
../rtkube/rtkube_serial.py \
../widgets/office_shipments_chart.py \
../widgets/office_shipments_table.py


TRANSLATIONS = CtrlCraft_fr.ts \
