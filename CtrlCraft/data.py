# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Module defining CtrlCraftData class.
"""

import os

import pglite as pl
import psycopg2
from qgis.core import Qgis

from CtrlCraft.logger import CtrlCraftLogger
from CtrlCraft.settings import DB_NAME, INITDB_SQL


class CtrlCraftData:
    """
    Interface with Postgres.
    """

    isPgliteAvailable = False

    def __init__(self, connStr: str = ""):
        self.__con = None
        self.lastProbeDate = None
        self.srid = 32632
        if connStr == "":
            self.connString = f"{pl.cluster_params()} dbname={DB_NAME}"
        else:
            self.connString = connStr

        self.logger = CtrlCraftLogger()
        self.logger.info(f"Will use connection: {self.connString}")

    @staticmethod
    def checkPglite():
        try:
            if not pl.check_cluster():
                pl.init_cluster()
            pl.start_cluster()
            CtrlCraftData.isPgliteAvailable = True
        except Exception as eCheck:
            CtrlCraftLogger().error(f"pglite cluster not available. Reason: {eCheck}")

    def isValid(self):
        """
        Checks if the underlying database is ready and well formed.
        """

        sql = (
            "SELECT EXISTS ("
            "SELECT 1 FROM information_schema.tables "
            "WHERE  table_schema = 'ctrlcraft'  "
            "AND    table_name = 'panels')"
        )

        valid = False
        if self.__con and self.__fetchOne(sql):
            valid = True

        return valid

    def isPre310(self):
        """
        Checks if the underlying database is of the format before CtrlCraft 3.1.0 version
        which did not have the radiometry classes stored in the database
        """

        sql = (
            "SELECT EXISTS ("
            "SELECT 1 FROM information_schema.tables "
            "WHERE  table_schema = 'ctrlcraft'  "
            "AND    table_name = 'radiometry_class_thresholds')"
        )

        pre310 = True
        if self.__con and self.__fetchOne(sql):
            pre310 = False

        return pre310

    def create(self, values=["1", "2", "3", "4"]):  # pylint: disable=W0102
        """
        Creates the database.
        """

        if CtrlCraftData.isPgliteAvailable:
            pl.create_db(DB_NAME)
        else:
            con = psycopg2.connect(self.connString)
            cur = con.cursor()
            con.set_isolation_level(0)
            cur.execute(f"CREATE DATABASE {DB_NAME}")
            con.commit()
            con.close()

        if self.open():
            self.__initDb()
            for i in values:
                self.truckCreate("{}".format(i))

    def clear(self):
        """
        Clears the database.
        """

        self.close()
        self.__removeDb()
        self.create()

    def open(self):
        """
        Opens the database.
        """

        if self.__con is None:
            # Try to connect
            try:
                self.__con = psycopg2.connect(self.connString)
                CtrlCraftLogger().info("Open database succeed")
            except Exception as eOpen:
                CtrlCraftLogger().error(f"Open database failed: {eOpen}")
                self.__con = None
                return False

        return True

    def close(self):
        """
        Closes the database.
        """

        if self.__con:
            self.__con.close()
        self.__con = None

    def export(self, filename):
        """
        Exports the database to a dump file
        """

        if CtrlCraftData.isPgliteAvailable:
            if not filename.endswith(".dump"):
                filename += ".dump"
            pl.export_db("ctrlcraft", filename)
        else:
            raise NotImplementedError("export_db not implemented")

    def importDump(self, filename):
        """
        Imports the dump file.
        """

        if not CtrlCraftData.isPgliteAvailable:
            raise NotImplementedError("import_db not implemented")

        if not self.isValid():
            return

        nbAttempts = 0
        while True:
            if nbAttempts > 19:
                raise RuntimeError("Impossible to drop existing database after 20 attemps")
            try:
                nbAttempts += 1
                self.close()
                self.__removeDb()
                pl.import_db(filename, "ctrlcraft")
                break
            except RuntimeError:
                self.logger.warning("Database exists, not dropped, trying again", Qgis.Warning)

    def translate(self, customProj, inOut=True):
        tables = ['"co3"', "probe", "slabs", "fixed_measure"]
        if not inOut:
            customProj = [-i for i in customProj]
        for table in tables:
            sql = """UPDATE ctrlcraft.{} SET geom=(SELECT ST_Translate(geom, {},{},{}));""".format(
                table, customProj[0], customProj[1], customProj[2]
            )
            self.__commit(sql)

    def shipments(self, deposit, panel, blast):  # pylint: disable=W0613
        """
        Returns all slabs loaded in trucks within deposit/panel/blast.
        """

        sql = (
            "select count, deposit, panel, blast, truck, unload_time, "
            "radiometry, radiometry_probe, slabs "
            "from ctrlcraft.truck_loads_status "
            "where panel = '{panel}' "
            "and blast = '{blast}'".format(panel=panel, blast=blast)
        )

        cur = self.__con.cursor()
        cur.execute(sql)

        return cur.fetchall()

    def getCurrentProbeRadiometry(self, truckid):
        """
        Returns the probe radiometry of the current truck
        """
        sql = (
            "select radiometry_probe "
            "from ctrlcraft.truck_loads_status "
            "where truck = '{truckid}' "
            "and unload_time =''".format(truckid=truckid)
        )

        cur = self.__con.cursor()
        cur.execute(sql)
        res = [i[0] for i in cur.fetchall()]
        return res[0] if len(res) > 0 else None

    def getMaxSlabsShipment(self, deposit, panel, blast):  # pylint: disable=W0613
        """
        Returns all slabs loaded in trucks within deposit/panel/blast.
        """

        sql = (
            "select count "
            "from ctrlcraft.truck_loads_status "
            "where panel = '{panel}' "
            "and blast = '{blast}'".format(panel=panel, blast=blast)
        )

        cur = self.__con.cursor()
        cur.execute(sql)
        res = [int(i[0]) for i in cur.fetchall()]
        if len(res) > 0:
            res = max(res)
        else:
            res = 0
        return res

    def slabCreate(  # pylint: disable=R0913,R0914,
        self,
        smuid,
        deposit,
        panel,
        blast,
        quality,
        lith,
        perc,
        unit,
        x,
        y,
        z,
        transX,
        transY,
        transZ,
    ):
        """
        Creates a slabs thanks to a SMU centroid.
        """

        sql = (
            "select ctrlcraft.insert_smu('{}', '{}', '{}', '{}', "
            "{}, '{}', {}, '{}', {}, {}, {}, {}, {}, {})".format(
                smuid,
                deposit,
                panel,
                blast,
                quality,
                lith,
                perc,
                unit,
                x,
                y,
                z,
                transX,
                transY,
                transZ,
            )
        )
        self.__commit(sql)

    def slabsCreateIndex(self):
        sql = "CREATE INDEX IF NOT EXISTS smus_geom_idx ON ctrlcraft.smus USING gist (geom)"
        self.__commit(sql)

        sql = "CREATE INDEX IF NOT EXISTS slabs_geom_idx ON ctrlcraft.slabs USING gist (geom)"
        self.__commit(sql)

    def slabWkt(self, slabId):
        """
        Returns the position in WKT for the corresponding slab.
        """

        sql = "select st_astext(st_envelope(geom)) from ctrlcraft.slabs " "where id = '{slabId}'".format(slabId=slabId)

        return self.__fetchAll(sql)[0]

    def slabRadiometry(self, x, y):
        """
        Returns the radiometry of the upper slab at the position (x, y).
        """

        sql = (
            "select radiometry, radiometry_probe from "
            "ctrlcraft.slabs_available "
            "where st_intersects(geom, st_setsrid(st_point({}, {}), {})) "
            "order by centroid_z DESC limit 1".format(x, y, self.srid)
        )

        cur = self.__con.cursor()
        cur.execute(sql)

        return cur.fetchone()

    def smu(self, smuId, deposit, panel, blast):  # pylint: disable=W0613
        """
        Returns the smu id if found in database.
        """

        sql = (
            "select id from ctrlcraft.smus "
            "where smu_id = '{smuId}' "
            "and panel='{panel}' "
            "and blast='{blast}'".format(smuId=smuId, blast=blast, panel=panel)
        )

        cur = self.__con.cursor()
        cur.execute(sql)

        return cur.fetchone()

    def lithologies(self):
        """
        Returns a list of available lithologies.
        """

        sql = "select unnest(enum_range(NULL::lithology))"

        records = []
        for lithology in self.__fetchAll(sql):
            records.append(lithology)

        return records

    def lithologyUpdate(self, name, point):
        """
        Updates the lithology of the more above slab at the given position.
        """

        wkt = point.asWkt()
        sql = "select ctrlcraft.lithology_update('{}', '{}')".format(name, wkt)
        self.__commit(sql)

    def affectRadiometrySlab(self, isUpSonde):
        """
        Affect the radiometry of a slab using last positions from the GeoCube.
        """
        sql = "SELECT ctrlcraft.add_fix_measure({}, {})".format(int(isUpSonde), int(not isUpSonde))
        self.__commit(sql)

    def affectRadiometrySlabDegraded(self, teneurUp, teneurLow, inclination, wkt):
        """
        Affect the radiometry of a slabe using last positions from the GeoCube.
        Degraded mode.
        """
        sql = "SELECT ctrlcraft.add_fix_measure_degraded({}, {}, {}, '{}')".format(
            teneurUp, teneurLow, inclination, wkt
        )

        self.__commit(sql)

    def getAverageLastFourRadiometry(self):
        """
        Returns the average of last 4 radiometries recorded
        """
        sql = """WITH radio as
                  (SELECT teneurup, teneurdown FROM ctrlcraft.probe
                  ORDER BY "time" DESC
                  LIMIT 4)
                  SELECT avg(teneurup) as up, avg(teneurdown) as down
                  FROM radio"""

        return self.__fetchOne(sql, entireRow=True)

    def deposits(self):
        """
        Returns a list of all deposits.
        """

        sql = "select id from ctrlcraft.deposits order by id"

        return self.__fetchAll(sql)

    def panels(self, depositid):
        """
        Returns a list of all panels in the deposit.
        """

        sql = "select id from ctrlcraft.panels " "where deposit = '{}' order by id".format(depositid)

        return self.__fetchAll(sql)

    def blasts(self, panelid):
        """
        Returns a list of all blasts.
        """

        sql = "select id from ctrlcraft.blasts where panel = '{}' order " "by id".format(panelid)

        return self.__fetchAll(sql)

    def truckCreate(self, name):
        """
        Creates a new truck.
        """

        sql = "insert into ctrlcraft.trucks values('{}')".format(name)
        self.__commit(sql)

    def trucks(self):
        """
        Returns a list of trucks.
        """

        sql = "select id from ctrlcraft.trucks order by id"

        return self.__fetchAll(sql)

    def trucksNoOrder(self):
        """
        Returns a list of trucks.
        """

        sql = "select id from ctrlcraft.trucks"

        return self.__fetchAll(sql)

    def truckSlabs(self, truckid):
        """
        Returns slabs currently loaded in the truck.
        """
        if truckid in self.trucks():
            sql = "select array_length(loads, 1) from ctrlcraft.trucks where id = '{}'".format(truckid)

            rec = self.__fetchAll(sql)[0]
            if not rec:
                rec = 0
        else:
            self.logger.warning(f"Truck {truckid} is unknown.", Qgis.Warning)
            rec = 0

        return rec

    def checkFixedMeasuresOnSlabs(self, point):
        """
        Check that a fixed measure has been taken before loading the slab
        """
        wkt = point.asWkt()
        sql = "select ctrlcraft.check_fixed_meaures_on_slabs('{}')".format(wkt)
        return self.__fetchOne(sql)

    def truckLoadSlab(self, truckid, point):
        """
        Loads a slab in the specific truck.
        """

        wkt = point.asWkt()
        sql = "select ctrlcraft.truck_load_slab('{}', '{}')".format(truckid, wkt)
        self.__commit(sql)

    def truckUnloadSlabs(self, truckid):
        """
        Unloads all slabs in the specific truck.
        """

        sql = "select ctrlcraft.truck_unload_slabs('{}')".format(truckid)

        self.__commit(sql)

    def truckUnloadLastSlab(self):
        """
        Unload the last slab loaded in a truck.
        """

        sql = "select ctrlcraft.truck_unload_last_slab()"

        return self.__fetchOne(sql, True)

    def truckIdsChange(self, newIds):
        """
        Change trucks ID when the parameters is changed
        """
        currentIds = self.trucks()
        for curId in currentIds:
            sql = "delete from ctrlcraft.trucks where id='{}'".format(curId)
            self.__commit(sql)
        for newId in newIds:
            self.truckCreate("{}".format(newId))

    def slabUpdateProbeRadiometry(self, x, y, radiometry):
        """
        Add radiometry when position is not known
        """
        sql = "SELECT ctrlcraft.slab_update_probe_radiometry({}, {}, {})".format(x, y, radiometry)
        self.__commit(sql)

    def probeNewData(  # pylint: disable=R0913,R0914,
        self,
        date,
        serialNumberGps,
        lat,
        lon,
        alt,
        calculation,
        serialNumberGamma,
        radiometry,
        gammaup,
        gammadown,
        teneurup,
        teneurdown,
        inclination,
        coeffAUp,
        coeffALow,
        coeffBUp,
        coeffBLow,
        coeffNUp,
        coeffNLow,
    ):
        self.lastProbeDate = date
        sql = (
            "select ctrlcraft.probe_new_data('{}', '{}', '{}', {}, {}, {}, "
            "{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {})"
        ).format(
            date,
            serialNumberGps,
            serialNumberGamma,
            float(lat),
            float(lon),
            float(alt),
            float(radiometry),
            float(gammaup),
            float(gammadown),
            float(teneurup),
            float(teneurdown),
            float(inclination),
            calculation,
            coeffAUp,
            coeffALow,
            coeffBUp,
            coeffBLow,
            coeffNUp,
            coeffNLow,
        )
        self.__commit(sql)

    def probeExport(self, filename):
        """
        Exports probe data in a CSV file.
        """

        sql = "select ctrlcraft.probe_to_csv('{}')".format(filename)
        self.__execute(sql)

    def co3(self, x, y):
        sql = "select * from ctrlcraft.co3" " where st_x(geom) = {x} and st_y(geom) = {y} limit 1".format(x=x, y=y)

        cur = self.__con.cursor()
        cur.execute(sql)

        return cur.fetchone()

    def co3CreateIndex(self):
        """
        Creates a gist index on co3 data.
        """

        sql = "CREATE INDEX IF NOT EXISTS co3_geom_idx ON ctrlcraft.co3 USING gist (geom)"
        self.__commit(sql)

    def co3Create(self, deposit, panel, blast, depth, co3, x, y, z):
        """
        Creates a new CO3 hole.
        """

        geom = "St_SetSrid(ST_MakePoint({x},{y},{z}), {srid})".format(x=x, y=y, z=z, srid=self.srid)

        sql = "INSERT INTO ctrlcraft.co3 VALUES('{}', '{}', '{}', {}, " "{}, {})".format(
            deposit, panel, blast, depth, co3, geom
        )
        self.__execute(sql)

    def trucksLoadsStatusExport(self, filename):
        """
        Exports trucks loads status view in a CSV file.
        """

        sql = "select ctrlcraft.trucks_loads_status_to_csv('{}')".format(filename)
        self.__execute(sql)

    def getLevels(self, deposit, panel, blast):
        """
        Get levels and altitude to filter slabs layer.
        """
        sql = """WITH sl as
                (SELECT DISTINCT s.centroid_z
                FROM ctrlcraft.slabs_available s
                {deposit}
                {panel}
                {blast}
                GROUP BY s.centroid_z ORDER BY s.centroid_z DESC)
                SELECT s.lvl, sl.centroid_z
                from sl
                LEFT JOIN ctrlcraft.levels s
                ON s.centroid_z = sl.centroid_z
                {deposit}
                {panel}
                {blast}
                ORDER BY centroid_z DESC""".format(
            deposit="where s.deposit='" + deposit + "'" if deposit else "",
            panel="and s.panel='" + panel + "'" if panel else "",
            blast="and s.blast='" + blast + "'" if blast else "",
        )
        return self.__fetchAll(sql, True)

    def getRemovedLevels(self, deposit, panel, blast):
        sql = """WITH removed as
                (SELECT s.load, s.id, s.centroid_z
                 FROM ctrlcraft.slabs s, ctrlcraft.truck_loads t
                 WHERE s.load is not null and t.truck = '-1' and t.id = s.load
                      {deposit}
                      {panel}
                      {blast}
                )
                SELECT s.lvl, s.centroid_z
                FROM ctrlcraft.levels s
                WHERE s.centroid_z IN (SELECT centroid_z
                                        FROM removed)
                      {deposit}
                      {panel}
                      {blast}
                                        ORDER BY lvl DESC""".format(
            deposit="and s.deposit='" + deposit + "'" if deposit else "",
            panel="and s.panel='" + panel + "'" if panel else "",
            blast="and s.blast='" + blast + "'" if blast else "",
        )
        return self.__fetchAll(sql, True)

    def removeLevel(self, centroidZ, deposit, panel, blast):
        """
        Remove level at Z : centroidZ
        """

        # Set truck id = -1 for slabs in the level we want to remove (so it's in a fictive truck)
        sql = """SELECT ctrlcraft.truck_load_slab('-1',
                   st_astext(st_makepoint(st_x(st_centroid(geom)), st_y(st_centroid(geom)), centroid_z))
                 )
                 FROM ctrlcraft.slabs_available
                 WHERE centroid_z >= {centroidZ}
                 and load is NULL
                 {deposit}
                 {panel}
                 {blast}
              """.format(
            centroidZ=centroidZ,
            deposit="and deposit='" + deposit + "'" if deposit else "",
            panel="and panel='" + panel + "'" if panel else "",
            blast="and blast='" + blast + "'" if blast else "",
        )
        self.__commit(sql)

        # Set load and unload time of all slabs in removed levels to the latest load time
        sql = """WITH m_load_time as
                (SELECT max(load_time) FROM ctrlcraft.truck_loads)
                UPDATE ctrlcraft.truck_loads
                SET load_time = (SELECT max FROM m_load_time),
                    unload_time = (SELECT max FROM m_load_time)
                WHERE unload_time is NULL and truck = '-1'
               """
        self.__commit(sql)

    def revertRemovedLevel(self, centroidZ, deposit, panel, blast):
        """
        Re-add the removed level
        """

        # Get loads from slabs which are in truck id -1
        sql = """SELECT s.load
                  FROM ctrlcraft.slabs s, ctrlcraft.truck_loads t
                  WHERE s.centroid_z <= {centroidZ}
                  and s.load is not null
                  and t.truck = '-1'
                  and t.id = s.load
                  {deposit}
                  {panel}
                  {blast}
               """.format(
            centroidZ=centroidZ,
            deposit="and s.deposit='" + deposit + "'" if deposit else "",
            panel="and s.panel='" + panel + "'" if panel else "",
            blast="and s.blast='" + blast + "'" if blast else "",
        )

        # For each load, delete their reference in table slabs, and delete them
        for load in self.__fetchAll(sql):
            sql = """UPDATE ctrlcraft.slabs
                      SET load = NULL
                      WHERE load = '{load}'
                      {deposit}
                      {panel}
                      {blast}
                   """.format(
                load=load,
                deposit="and deposit='" + deposit + "'" if deposit else "",
                panel="and panel='" + panel + "'" if panel else "",
                blast="and blast='" + blast + "'" if blast else "",
            )
            self.__commit(sql)
            sql = """DELETE FROM ctrlcraft.truck_loads
                      WHERE id = '{}'""".format(
                load
            )
            self.__commit(sql)

    def getClassThresholds(self):
        """
        Returns the radiometry class thresholds
        of the form {i: {'min': <value>, 'max': <value>', 'checked': <value>, 'color': <value>}}
        """
        sql = "SELECT id, min, max, checked, color from ctrlcraft.radiometry_class_thresholds order by id"
        return {
            i: {"min": float(min), "max": float(max), "checked": bool(checked), "color": color}
            for i, min, max, checked, color in self.__fetchAll(sql, True)
        }

    def setClassThresholds(self, thresholds: dict):
        """
        Set the database radiometry class thresholds

        :param thresholds: dict of the form {i: {'max': <value>, 'min': <value>, 'checked': <value>}}
        """
        sql = ""
        for i, values in thresholds.items():
            sql += (
                "UPDATE ctrlcraft.radiometry_class_thresholds "
                f"set min = {values['min']}, max = {values['max']}, checked = {values['checked']} "
                f"where id = {i};"
            )
        self.__commit(sql)

    def __execute(self, sql):
        if not self.__con:
            raise psycopg2.ProgrammingError("No opened connection to database!")

        cur = self.__con.cursor()
        cur.execute(sql)

    def __fetchOne(self, sql, commit=False, entireRow=False):
        res = None
        if not self.__con:
            raise psycopg2.ProgrammingError("No opened connection to database!")

        cur = self.__con.cursor()
        cur.execute(sql)
        if entireRow:
            res = cur.fetchone()
        else:
            res = cur.fetchone()[0]

        if commit:
            self.__con.commit()

        return res

    def __fetchAll(self, sql, entireRow=False):
        records = []
        if not self.__con:
            raise psycopg2.ProgrammingError("No opened connection to database!")

        cur = self.__con.cursor()
        cur.execute(sql)

        if entireRow:
            return cur.fetchall()

        for record in cur.fetchall():
            records.append(record[0])

        return records

    def __commit(self, sql):
        if not self.__con:
            raise psycopg2.ProgrammingError("No opened connection to database!")

        try:
            cur = self.__con.cursor()
            cur.execute(sql)
            self.__con.commit()
        except Exception as e:
            CtrlCraftLogger().error(f"Failed to commit: {e}")
            self.close()
            raise e

    def __initDb(self):
        dirname = os.path.dirname(os.path.abspath(__file__))
        initdbSql = os.path.join(dirname, "utils", INITDB_SQL)
        with open(initdbSql, encoding="utf-8") as sqlfile:
            statements = sqlfile.read().split("\n;\n")

        statements = list(filter(lambda x: x != "", statements))
        for statement in statements:
            self.__commit(statement)

    def __closeSessions(self):
        try:
            con = psycopg2.connect(self.connString)
            cur = con.cursor()
            con.set_isolation_level(0)
            cur.execute(
                "select pg_terminate_backend(pg_stat_activity.pid)"
                " from pg_stat_activity"
                " where pg_stat_activity.datname = '{}'".format(DB_NAME)
            )
            con.commit()
            con.close()  # should not be reached
        except psycopg2.OperationalError:
            pass  # it is ok: we have been disconnected

    def __removeDb(self):
        self.__closeSessions()
        connDict = {k: v for k, v in [c.split("=") for c in self.connString.split(" ")]}
        connDict["dbname"] = "postgres"
        connString = " ".join(["=".join([k, v]) for k, v in connDict.items()])
        con = psycopg2.connect(connString)
        con.set_isolation_level(0)
        cur = con.cursor()
        cur.execute(
            "select pg_terminate_backend(pg_stat_activity.pid)"
            " from pg_stat_activity"
            f" where pg_stat_activity.datname = '{DB_NAME}'"
        )
        con.commit()
        cur.execute(f"DROP DATABASE IF EXISTS {DB_NAME}")
        con.commit()
        con.close()


# finaly run the pglite check
CtrlCraftData.checkPglite()
