# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Module for bluetooth communication and simulation
"""

import os
import threading
import time
from binascii import unhexlify
from datetime import datetime

import serial
from qgis.PyQt import QtCore
from qgis.PyQt.QtCore import QObject, Qt
from qgis.PyQt.QtWidgets import QApplication
from serial import serialutil

from CtrlCraft.logger import CtrlCraftLogger
from CtrlCraft.rtkube.constantes import (
    BAUDRATE,
    BAUDRATE_BACKUP,
    FIELD_SEPARATOR,
    INC,
    LOW_D_GAM,
    MINIMAL_NB_FIELDS,
    NBFIELDS,
    SENSOR,
    SENSORS,
    SN_GAM,
    TIMEOUT,
    UP_D_GAM,
)

SIMU = False
SIMU_SPARE = False
SIMU_LIGHT = False
SIMU_GAMMA_FREEZE = False
if "SIMU" in os.environ:
    import random

    if int(os.environ["SIMU"]) == 1:
        SIMU = True
    if int(os.environ["SIMU"]) == 2:
        SIMU_SPARE = True
    if int(os.environ["SIMU"]) == 3:
        SIMU_LIGHT = True
    if int(os.environ["SIMU"]) == 4:
        SIMU_GAMMA_FREEZE = True
else:
    random = None  # pylint:disable=invalid-name


class RtkubeSerial(QObject):
    """Class reading sensors and sending measurements"""

    sendGNSS = QtCore.pyqtSignal(
        str,  # DATE
        str,  # TIME
        str,  # LON
        str,  # LAT
        str,  # ALT
        str,  # SERIAL
        str,  # PREC PLANI
        str,  # PREC ALTI
        str,  # Calculation
    )

    sendGammaInc = QtCore.pyqtSignal(str, str, str, str)  # SERIAL  # UP_D_GAM  # LOW_D_GAM  # INC
    sendError = QtCore.pyqtSignal(str)
    settings = QtCore.QSettings()

    def __init__(self):
        QObject.__init__(self)

        self.ser = serial.Serial()
        self.running = False
        self.threadStopEvt = None

        self.logger = CtrlCraftLogger("rtkubeSerial")

        self.nbError = 0

    def connect(self):
        normalConnection = False
        error = ""
        target = None
        if SIMU:
            target = self.workerThreadSIMU
            self.logger.info("Simulation mode")
        elif SIMU_SPARE:
            target = self.workerThreadBackupSIMUSpare
            self.logger.info("Simulation spare mode")
        elif SIMU_LIGHT:
            target = self.workerThreadSIMULight
            self.logger.info("Simulation light mode")
        elif SIMU_GAMMA_FREEZE:
            target = self.workerThreadSIMUGammaFreeze
        else:
            try:
                self.ser = serial.Serial(
                    self.settings.value("/GeoCube/serialportrover", ""), baudrate=BAUDRATE, timeout=TIMEOUT
                )
                normalConnection = True
                target = self.workerThread
            except serialutil.SerialException as e:
                error = str(e)

            if not normalConnection:
                try:
                    self.ser = serial.Serial(
                        self.settings.value("/GeoCube/serialportspare", ""), baudrate=BAUDRATE_BACKUP, timeout=TIMEOUT
                    )
                    target = self.workerThreadBackup
                except serialutil.SerialException as e:
                    if not isinstance(self, RtkubeSingleMeas):
                        self.disconnect()
                    if "PermissionError" in error:
                        error = "Serial port already in use"
                    self.logger.critical(error)
                    self.sendError.emit(
                        self.tr("Cannot connect to bluetooth device nor spare : {0} {1}").format(e, error)
                    )

        if target:
            self.running = True
            if not SIMU and not SIMU_SPARE and not SIMU_LIGHT and not SIMU_GAMMA_FREEZE:
                self.ser.reset_input_buffer()
            self.threadStopEvt = threading.Event()
            threading.Thread(target=target, args=(self.threadStopEvt,)).start()

    def disconnect(self):
        self.logger.info("stopping")
        if self.threadStopEvt:
            self.threadStopEvt.set()
        self.running = False
        self.ser.close()

    def workerThread(self, stopEvt):
        while not stopEvt.is_set() and self.running and self.ser.isOpen():
            msg = None
            try:
                msg = self.ser.readline().decode("ascii")
            except serial.SerialException as e:
                self.disconnect()
                self.logger.error(e)
                self.sendError.emit(str(e))
            except Exception as e:
                self.disconnect()
                self.logger.error(e)
                self.sendError.emit(f"Unknown error: {e}")

            if msg and isinstance(msg, str) and len(msg) > 0:
                self.logger.info(msg)
                fields = msg.split(FIELD_SEPARATOR)
                if len(fields) > MINIMAL_NB_FIELDS:
                    self.nbError = 0
                    if fields[SENSOR] == SENSORS["GNSS"] and len(fields) == NBFIELDS["GNSS"]:
                        self.sendGNSS.emit(*fields[MINIMAL_NB_FIELDS:])
                    elif fields[SENSOR] == SENSORS["GAMMA"] and len(fields) == NBFIELDS["GAMMA"]:
                        self.sendGammaInc.emit(fields[SN_GAM], fields[UP_D_GAM], fields[LOW_D_GAM], fields[INC])
                else:
                    self.nbError += 1
                    self.disconnect()
                    errorMsg = f"Incorrect value: {msg}\nVerify your com port."
                    self.logger.error(errorMsg)
                    self.sendError.emit(errorMsg)

            else:
                self.nbError += 1
                if self.nbError == 3:
                    self.disconnect()
                    msg = "No data received since 2 seconds."
                    self.logger.error(msg)
                    self.sendError.emit(msg)

    def getDataBackup(self):
        ret = []
        string = "054449"  # It's kind of magic
        # 0x05 : ENQ
        # 0x44 : Gimme data
        # 0x49 : Checksum = 0x05+0x44
        self.ser.write(unhexlify(string))

        ackNak = self.ser.read(1)
        ret.append(ackNak)  # ack_nak
        if ackNak == b"\x06":  # Ack : 0x06
            ret.append(self.ser.read(2))  # ggdmSerial
            ret.append(self.ser.read(3))  # upperDeltaCount
            ret.append(self.ser.read(2))  # upperDeltaTime
            ret.append(self.ser.read(4))  # upperKfactor
            ret.append(self.ser.read(3))  # lowerDeltaCount
            ret.append(self.ser.read(2))  # lowerDeltaTime
            ret.append(self.ser.read(4))  # lowerKfactor
            ret.append(self.ser.read(2))  # inclination
            ret.append(self.ser.read(1))  # csum

        return ret

    def workerThreadBackup(self, stopEvt):
        while not stopEvt.is_set() and self.running and self.ser.isOpen():
            utc = datetime.utcnow()
            dateOnly = utc.strftime("%d/%m/%Y")
            timeOnly = utc.strftime("%H:%M:%S")
            try:
                ret = self.getDataBackup()
                if len(ret) > 1:
                    (  # pylint: disable=unbalanced-tuple-unpacking
                        _,  # ack_nak,
                        ggdmSerial,
                        upperDeltaCount,
                        _,  # upperDeltaTime,
                        _,  # upperKfactor,
                        lowerDeltaCount,
                        _,  # lowerDeltaTime,
                        _,  # lowerKfactor,
                        inclination,
                        _,  # csum,
                    ) = ret
                    self.logger.info("upper_delta_count: %d", sum(upperDeltaCount))
                    self.logger.info("lower_delta_count: %d", sum(lowerDeltaCount))
                    self.logger.info("inclination: %d", sum(inclination))
                    self.sendGNSS.emit(dateOnly, timeOnly, "-1", "-1", "-1", "-1", "-1", "0")
                    time.sleep(1)
                    self.sendGammaInc.emit(
                        str(ggdmSerial),
                        str(sum(upperDeltaCount)),
                        str(sum(lowerDeltaCount)),
                        str(sum(inclination)),
                    )
                else:
                    self.logger.error("NAK")
                    self.sendError.emit("Cannot get data from Spare")
            except serial.SerialException as e:
                self.disconnect()
                self.logger.error(e)
                self.sendError.emit(str(e))

    def workerThreadSIMU(self, stopEvt):
        while not stopEvt.is_set() and self.running:
            utc = datetime.utcnow()
            dateOnly = utc.strftime("%d/%m/%Y")
            timeOnly = utc.strftime("%H:%M:%S")
            lon = str(round(random.uniform(18.7330921394, 18.7330923394), 8))
            lat = str(round(random.uniform(7.311018811, 7.311019011), 8))

            time.sleep(1)
            self.sendGNSS.emit(dateOnly, timeOnly, lat, lon, "666", "12345AB", "-1", "-1", "23")
            self.sendGammaInc.emit(
                "54321BA",
                str(random.randrange(0, 3500)),
                str(random.randrange(0, 3500)),
                str(random.randint(0, 10)),
            )

    def workerThreadBackupSIMUSpare(self, stopEvt):
        while not stopEvt.is_set() and self.running:
            utc = datetime.utcnow()
            dateOnly = utc.strftime("%d/%m/%Y")
            timeOnly = utc.strftime("%H:%M:%S")
            self.sendGNSS.emit(dateOnly, timeOnly, "-1", "-1", "-1", "12345AB", "-1", "-1", "0")
            time.sleep(1)
            self.sendGammaInc.emit(
                "54321BA", str(random.randrange(5, 1500)), str(random.randrange(5, 1500)), str(random.randint(0, 10))
            )

    def workerThreadSIMULight(self, stopEvt):
        while not stopEvt.is_set() and self.running:
            utc = datetime.utcnow()
            dateOnly = utc.strftime("%d/%m/%Y")
            timeOnly = utc.strftime("%H:%M:%S")
            lon = str(round(random.uniform(18.7769863, 18.7770074), 8))
            lat = str(round(random.uniform(7.2925811, 7.2926041), 8))

            self.sendGNSS.emit(dateOnly, timeOnly, lat, lon, "666", "12345AB", "-1", "-1", "23")
            time.sleep(1)
            self.sendGammaInc.emit("54321BA", "-1", "-1", "-1")

    def workerThreadSIMUGammaFreeze(self, stopEvt):
        lastTime = time.time()
        count = 0
        while not stopEvt.is_set() and self.running:
            utc = datetime.utcnow()
            dateOnly = utc.strftime("%d/%m/%Y")
            timeOnly = utc.strftime("%H:%M:%S")
            lon = str(round(random.uniform(18.7769863, 18.7770074), 8))
            lat = str(round(random.uniform(7.2925811, 7.2926041), 8))

            self.sendGNSS.emit(dateOnly, timeOnly, lat, lon, "666", "12345AB", "-1", "-1", "23")
            time.sleep(1)
            if lastTime < time.time() - 10:
                self.sendGammaInc.emit("54321BA", str(600), str(300), str(random.randint(0, 10)))
                count += 1
                if count > 5:
                    lastTime = time.time()
                    count = 0
            else:
                self.sendGammaInc.emit(
                    "54321BA",
                    str(random.randrange(5, 1500)),
                    str(random.randrange(5, 1500)),
                    str(random.randint(0, 10)),
                )


class RtkubeSingleMeas(RtkubeSerial):
    """Class reading sensors just one time"""

    sendSingleGNSS = QtCore.pyqtSignal(tuple)
    sendSingleGamma = QtCore.pyqtSignal(tuple)

    def connect(self):
        raise NotImplementedError("Don't use this method for single measurement")

    def disconnect(self):
        raise NotImplementedError("Don't use this method for single measurement")

    def getSingleGNSS(self):
        if self.running:
            return  # Already measuring

        QApplication.setOverrideCursor(Qt.WaitCursor)
        super().connect()
        if not self.running:
            errorMsg = self.tr("Landmark point error: make sure K2 connection is not running")
            self.logger.error(errorMsg)
            self.sendError.emit(errorMsg)
            QApplication.restoreOverrideCursor()

        self.sendGNSS.connect(self.gnssRecieved)

    def gnssRecieved(self, *data):
        super().disconnect()
        self.sendGNSS.disconnect(self.gnssRecieved)
        self.sendSingleGNSS.emit(data)
        QApplication.restoreOverrideCursor()

    def getSingleGamma(self):
        if self.running:
            return  # Already measuring

        QApplication.setOverrideCursor(Qt.WaitCursor)
        super().connect()
        if not self.running:
            errorMsg = self.tr("Single Gamma error: make sure K2 connection is not running")
            self.logger.error(errorMsg)
            self.sendError.emit(errorMsg)
            QApplication.restoreOverrideCursor()

        self.sendGammaInc.connect(self.gammaRecieved)

    def gammaRecieved(self, *data):
        super().disconnect()
        self.sendGammaInc.disconnect(self.gammaRecieved)
        self.sendSingleGamma.emit(data)
        QApplication.restoreOverrideCursor()
