# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Module for RTKube DockWidget
"""

import configparser
import os
import time

from qgis.core import (
    Qgis,
    QgsCoordinateReferenceSystem,
    QgsCoordinateTransform,
    QgsGeometry,
    QgsPointXY,
    QgsProject,
    QgsSettings,
)
from qgis.PyQt import QtCore, QtWidgets, uic
from qgis.utils import iface

from CtrlCraft.logger import CtrlCraftLogger
from CtrlCraft.rtkube.constantes import (
    DURATION,
    EPSG,
    GPS_CALCULATION,
    GPS_COLOR,
)
from CtrlCraft.rtkube.rtkube_serial import RtkubeSerial
from CtrlCraft.utils import misc

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), "rtkube_dockwidget.ui"))

NB_FIELDS_DATA = 19


class RTKubeDockWidget(QtWidgets.QDockWidget, FORM_CLASS):  # pylint: disable=too-many-instance-attributes
    """
    DockWidget for displaying RTKube data
    """

    # send data to other plugins
    dataToSMU = QtCore.pyqtSignal(
        str,  # date
        str,  # serial number GHSS
        float,  # x
        float,  # y
        float,  # z
        int,  # calculation
        str,  # serial number Gamma
        float,  # radiometry
        float,  # inclin
        float,  # gammaup (cps up)
        float,  # gammadown (cps down)
        float,  # teneurUp
        float,  # teneurLow
        float,  # coeffAUp
        float,  # coeffALow
        float,  # coeffBUp
        float,  # coeffBLow
        float,  # coeffNUp
        float,  # coeffNLow
    )
    closed = QtCore.pyqtSignal()
    opened = QtCore.pyqtSignal()

    def __init__(self, gammaNIni, database, parent=None):
        """
        :param gammaNIni Path: file path to the normalization coefficients file (probe_normalization.ini)
        """
        super().__init__(parent)
        self.tr = misc.tr
        self.setupUi(self)

        self.ser = RtkubeSerial()

        self.ser.sendGNSS.connect(self.setDataGNSS)
        self.ser.sendGammaInc.connect(self.setDataGammaInc)
        self.ser.sendError.connect(self.errors)

        self.connectButton.clicked.connect(self.connection)

        self.isConnected = False

        self.lastAlertIncli = time.time()

        self.hTeneurLow = [None, None, None]
        self.hTeneurUp = [None, None, None]
        self.lastAlertGammaFreeze = time.time()

        self.data = []

        self.logger = CtrlCraftLogger("RtkubeSerial_gui", iface)

        # translations for Nord Sahara projection
        self.projDelta: tuple | None = None

        # coefficients for gamma radiometry
        self.gammaCoeffs: list[float] | None = None

        # normalization for gamma radiometry
        self.gammaNIniData: configparser.ConfigParser | None = None
        self.gammaNIni = gammaNIni

        self.depositWarning.setVisible(False)
        self.depositWarning.setStyleSheet("background-color:" + "rgb(255, 0, 0)")
        self.projTabWidget.currentChanged.connect(self.syncBottomZLabels)
        self.syncBottomZLabels(self.projTabWidget.currentIndex())

        self.settings = QgsSettings()
        self.database = database
        self.thresholds: dict | None = None

    def clear(self):
        self.GNSSDate.clear()
        self.GNSSTime.clear()
        self.GNSSLat.clear()
        self.GNSSLon.clear()
        self.GNSSAlt.clear()
        self.GNSSPlaniPrec.clear()
        self.GNSSAltiPrec.clear()
        self.GNSSCalculation.clear()
        self.X.clear()
        self.Y.clear()
        self.Z.clear()
        self.ZFixed.clear()
        self.gammaGUp.clear()
        self.gammaTUp.clear()
        self.gammaGLow.clear()
        self.gammaTLow.clear()
        self.gammaInc.clear()
        self.hTeneurUp = [None, None, None]
        self.hTeneurLow = [None, None, None]
        self.qualRadiometry.clear()
        self.qualRadiometry.setStyleSheet("background-color:" + "rgb(119, 119, 119)")

    def setDataGNSS(
        self, date, timeStr, lon, lat, alt, serialNumber, prPlan, prAlti, calc
    ):  # pylint: disable=too-many-locals
        if not self.isConnected:
            return

        def convertToEpsg(x, y):
            geom = QgsGeometry.fromPointXY(QgsPointXY(x, y))
            trans = QgsCoordinateTransform(
                QgsCoordinateReferenceSystem("EPSG:4326"), QgsCoordinateReferenceSystem(EPSG), QgsProject.instance()
            )
            geom.transform(trans)
            point = geom.asPoint()
            return (point.x(), point.y())

        self.GNSSDate.setText(date)
        self.GNSSTime.setText(timeStr)
        self.GNSSLat.setText(lat)
        self.GNSSLon.setText(lon)
        self.GNSSAlt.setText(alt)
        self.GNSSPlaniPrec.setText(prPlan)
        self.GNSSAltiPrec.setText(prAlti)

        calc = int(calc)
        while calc not in GPS_CALCULATION.keys() and calc >= 0:
            calc -= 1
        calculation = GPS_CALCULATION[calc]
        self.GNSSCalculation.setText(calculation)
        self.GNSSCalculation.setStyleSheet("color: " + GPS_COLOR[calc])

        gpsmode = self.settings.value("/GeoCube/GPSmode", GPS_CALCULATION[23])
        if calculation == gpsmode:
            # no data or radio, incli + gammaup, gammadown, cpsup, cpsdown
            if len(self.data) == 0 or len(self.data) == 13:
                lonTrans, latTrans = convertToEpsg(float(lon), float(lat))
                altTrans = float(alt) - 2
                self.X.setText(str(round(lonTrans, 3)))
                self.Y.setText(str(round(latTrans, 3)))
                self.Z.setText(alt)
                self.ZFixed.setText(str(round(altTrans, 2)))
                if self.projDelta is None:
                    self.depositWarning.setVisible(True)
                    self.XNSah.setText("N/A")
                    self.YNSah.setText("N/A")
                    self.ZNSah.setText("N/A")
                    self.ZFixedNSah.setText("N/A")
                else:
                    dx, dy, dz = self.projDelta  # pylint:disable=unpacking-non-sequence
                    self.depositWarning.setVisible(False)
                    self.XNSah.setText(str(round(lonTrans + dx, 3)))
                    self.YNSah.setText(str(round(latTrans + dy, 3)))
                    self.ZNSah.setText(str(round(float(alt) + dz, 3)))
                    self.ZFixedNSah.setText(str(round(altTrans + dz, 2)))
                self.data = [date + " " + timeStr, serialNumber, lonTrans, latTrans, altTrans, int(calc)] + self.data
            else:
                self.logger.warning(
                    f"Set GNSS: Incoherent data. Actual: '{self.data}' -- "
                    f"Received: '{[date, serialNumber, timeStr, lon, lat, alt, prPlan, prAlti, calc]}'"
                )
                self.data.clear()
            if len(self.data) == NB_FIELDS_DATA:
                self.logger.debug(f"Send: '{self.data}'")
                self.dataToSMU.emit(*self.data)  # type: ignore
                self.data.clear()
        else:
            self.data.clear()
            self.logger.logBar(
                self.tr(
                    "Incoherent data. Data is {} precision, but rover settings has {} precision (see settings panel)."
                ).format(calculation, gpsmode),
                level=Qgis.MessageLevel.Warning,
            )

    def setDataGammaInc(self, snGamma, upGamma, lowGamma, incli):
        if not self.isConnected or self.gammaNIniData is None:
            return

        # material send the inclination on 4 integer
        inclination = float(incli) / 10.0
        self.gammaGUp.setText(upGamma)
        self.gammaGLow.setText(lowGamma)
        self.gammaInc.setText(str(inclination))

        # if no coeffcients for current deposit or bad configuration: exit
        if self.gammaCoeffs is None:
            self.errors(
                "Missing values (deposit or coefficient(s)), or wrong keywords, "
                "or wrong formula in gamma coefficients parameter file."
            )
            return

        # pylint:disable=unpacking-non-sequence
        aUp1, aLow1, bUp1, bLow1, aUp2, aLow2, bUp2, bLow2, tUp, tLow = self.gammaCoeffs

        # find normalization coefficient from serial number of current gamma probe
        if snGamma not in self.gammaNIniData:
            self.errors(
                self.tr("Gamma probe serial number {0} is missing in normalization parameter file").format(snGamma)
            )
            return

        try:
            nUp = self.gammaNIniData[snGamma].getfloat("N_sonde_haute", fallback=None)
            nLow = self.gammaNIniData[snGamma].getfloat("N_sonde_basse", fallback=None)
        except ValueError:
            self.errors(
                self.tr(
                    "A parameter value is not a number in normalization parameter file for serial number {0}"
                ).format(snGamma)
            )
            return
        if None in [nUp, nLow]:
            self.errors(
                self.tr(
                    "N_sonde_haute or basse is missing in normalization parameter file for serial number {0}"
                ).format(snGamma)
            )
            return

        # Time out
        upGamma = int(upGamma) / (1 - int(upGamma) * 0.000005)
        lowGamma = int(lowGamma) / (1 - int(lowGamma) * 0.000005)

        # normalization of gamma
        upGammaNormalized = nUp * upGamma  # type: ignore
        lowGammaNormalized = nLow * lowGamma  # type: ignore

        # select correct coefficients according to if the value is upper or lower than the threshold
        aUp, bUp = (aUp1, bUp1) if upGammaNormalized < tUp else (aUp2, bUp2)
        aLow, bLow = (aLow1, bLow1) if lowGammaNormalized < tLow else (aLow2, bLow2)

        # compute radiometry
        teneurUp = aUp * pow(upGammaNormalized, bUp)
        teneurLow = aLow * pow(lowGammaNormalized, bLow)

        self.setQualRadiometry(teneurUp)
        self.gammaTUp.setText(str(round(teneurUp, 3)))
        self.gammaTLow.setText(str(round(teneurLow, 3)))

        self.updateLastGammaValues(teneurUp, teneurLow)
        if (
            self.hTeneurUp[1:] == self.hTeneurUp[:-1] and self.hTeneurLow[1:] == self.hTeneurLow[:-1]
        ) and teneurUp is not None:
            self.logger.warning("Gamma values freeze ")
            if self.lastAlertGammaFreeze < time.time() - 2:
                self.logger.logBar(
                    self.tr("Gamma values are identical since more than 2 seconds"),
                    level=Qgis.MessageLevel.Warning,
                    duration=DURATION,
                )
                self.lastAlertGammaFreeze = time.time()

        if inclination <= float(self.settings.value("/GeoCube/angle", 10)):
            if len(self.data) == (NB_FIELDS_DATA - 13):
                cps = [upGamma, lowGamma, teneurUp, teneurLow]
                self.data = self.data + [snGamma, teneurUp, inclination] + cps + [aUp, aLow, bUp, bLow, nUp, nLow]
            else:
                self.logger.warning(
                    f"Set Gamma: Incoherent data Actual: '{self.data}' -- Received: '{[upGamma, inclination]}'"
                )
                self.data.clear()
            if len(self.data) == NB_FIELDS_DATA:
                self.logger.debug(f"Send: '{self.data}'")
                self.dataToSMU.emit(*self.data)  # type: ignore
                self.data.clear()
        else:
            self.logger.warning("Max inclination reached")
            if self.lastAlertIncli < time.time() - DURATION:
                self.logger.logBar(
                    self.tr("Max inclination reached"), level=Qgis.MessageLevel.Warning, duration=DURATION
                )
                self.lastAlertIncli = time.time()
            self.data.clear()

    def setQualRadiometry(self, rad):
        if self.thresholds is None:
            return
        self.qualRadiometry.setStyleSheet('font: 28pt "Arial Black"; background-color:' + "rgb(119, 119, 119)")
        self.qualRadiometry.setText(str(rad))
        for i in range(7):
            if i == 0:
                minTh, maxTh = float("-inf"), self.thresholds[i + 1]["min"]
            else:
                minTh, maxTh = self.thresholds[i]["min"], self.thresholds[i]["max"]
            if minTh < rad <= maxTh:
                self.qualRadiometry.setText(f"M{i}")
                self.qualRadiometry.setStyleSheet(
                    'font: 28pt "Arial Black"; background-color: ' + self.thresholds[i]["color"]
                )
                break

    def updateLastGammaValues(self, teneurUp, teneurLow):
        self.hTeneurUp.pop(0)
        self.hTeneurUp.append(teneurUp)
        self.hTeneurLow.pop(0)
        self.hTeneurLow.append(teneurLow)

    def connection(self):
        # pylint: disable=import-outside-toplevel,global-statement

        # To reload the treshold values
        self.thresholds = self.database.getClassThresholds()

        if not self.isConnected:
            # Reload N coefficients to be always up to date
            self.gammaNIniData = configparser.ConfigParser()
            self.gammaNIniData.read(self.gammaNIni)

            self.isConnected = True
            self.ser.connect()
            self.opened.emit()  # type: ignore

        else:
            answer = QtWidgets.QMessageBox.question(
                self, self.tr("Confirm disconnection"), self.tr("Do you want to disconnect the CAN-OP?")
            )
            if answer == QtWidgets.QMessageBox.No:
                self.isConnected = True
                return
            self.disconnection()

        self.changeButtonText()

    def disconnection(self):
        self.isConnected = False
        self.ser.disconnect()
        self.closed.emit()  # type: ignore
        self.clear()

    def changeButtonText(self):
        if self.isConnected:
            self.connectButton.setText(self.tr("Disconnection"))
        else:
            self.connectButton.setText(self.tr("Connection"))

    def errors(self, msg):
        self.logger.logBar(msg, level=Qgis.MessageLevel.Critical, duration=DURATION)
        self.disconnection()
        self.changeButtonText()

    def syncBottomZLabels(self, currentTabIndex):
        """Display relevant bottom Z fixed with the appropriate projection"""

        cond = currentTabIndex == 2
        self.labelZFixed.setHidden(cond)
        self.ZFixed.setHidden(cond)
        self.labelZFixedNSah.setVisible(cond)
        self.ZFixedNSah.setVisible(cond)
