# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Constants for RTKube
"""

SERIALPORT = "COM17"  # Port pour la sonde/mobile
SERIALPORT_BASE = "COM19"  # Port pour la base
SERIALPORT_BACKUP = "COM16"  # Bluetooth spare
BAUDRATE = 115200  # Bluetooth
BAUDRATE_BACKUP = 57600  # Bluetooth spare
TIMEOUT = 3

EPSG = "EPSG:32632"

DURATION = 5  # durée des messages d'erreur

# LINE GLOBALS
MINIMAL_NB_FIELDS = 5
FIELD_SEPARATOR = ","

# POSITION OF MANDATORY FIELDS
# Tow_quality is an indicator of quality of measurement of ToW, integer from 0 to 4 Only 4 is a reliable measurement.
QT = 0
TOW = 1  # stands for Time of week, which is in seconds the time elapsed from the begining of week. float
SENSOR = 2  # sensor_id is the name of the sensor,string
REV = 3  # sensor_version, its version,integer
BITMASK = 4  # bit_mask is a filter of property displayed, integer (hex format)

# List of Sensor types and corresponding codes
SENSORS = {
    "GNSS": "GPS",
    "GAMMA": "GAM",
}
NBFIELDS = {
    "GNSS": MINIMAL_NB_FIELDS + 9,
    "GAMMA": MINIMAL_NB_FIELDS + 8,
}

# GPS Protocole version 1
# DATE,TIME,LAT,LON,ALT,SN,HZPREC,VTPREC,CALCULATION
GPS_CALCULATION = {
    0: "Undetermined",
    1: "GNSS",
    6: "Diff",
    7: "DGNSS",
    15: "Float",
    23: "Fixed",
}
# Colors. TODO: refacto
# red, orange, yellow, green
GPS_COLOR = {
    0: "#000000",
    1: "#FF0000",
    6: "#FF0000",
    7: "#FF8C00",
    15: "#FFFF00",
    23: "#008000",
}

# GAMMA Protocole version 1
SN_GAM = MINIMAL_NB_FIELDS + 0
UP_D_GAM = MINIMAL_NB_FIELDS + 1
LOW_D_GAM = MINIMAL_NB_FIELDS + 4
INC = MINIMAL_NB_FIELDS + 7  # inclination
