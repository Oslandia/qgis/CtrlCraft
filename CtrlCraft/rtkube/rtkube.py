# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

"""
Module with some tools to manipulate RTKube plugin.
"""

import configparser
import os

from qgis.PyQt.QtCore import Qt, QTimer
from qgis.PyQt.QtWidgets import QFileDialog, QMessageBox

from CtrlCraft.rtkube.rtkube_dockwidget import RTKubeDockWidget
from CtrlCraft.utils import misc

REPAINT_SEC = 1.0


class RTKube:  # pylint: disable=too-many-instance-attributes
    """
    RTKube communication plugin
    """

    def __init__(self, iface, data, radWidget, projIni, gammaIni, gammaNIni, layer=None, topoLayer=None):
        super().__init__()
        self.tr = misc.tr
        self.iface = iface
        self.data = data
        self.layer = layer
        self.topoLayer = topoLayer
        self.radWidget = radWidget
        self.ready = False
        self.save = True
        self.widget: RTKubeDockWidget | None = None
        self.repainter = QTimer()
        self.repainter.setInterval(int(REPAINT_SEC * 1000))
        self.repainter.timeout.connect(self.repaint)
        self.projIni = projIni
        self.gammaIni = gammaIni
        self.gammaNIni = gammaNIni

    def init(self):
        # init widget
        self.widget = RTKubeDockWidget(self.gammaNIni, self.data)
        self.widget.dataToSMU.connect(self.received)
        self.iface.addDockWidget(Qt.DockWidgetArea.RightDockWidgetArea, self.widget)

        # launch map repainter
        self.repainter.start()

    def repaint(self):
        if self.widget is None:
            return

        if self.iface.mapCanvas().isDrawing():
            return
        try:
            if self.layer and (self.widget.ser.ser.is_open or ("SIMU" in os.environ and self.widget.ser.running)):
                self.layer.triggerRepaint()
                if self.topoLayer:
                    self.topoLayer.triggerRepaint()
        except RuntimeError:
            # self.layer variable exists but the underlying C++ object is not accessible
            # (for instance if the project is being reloaded)
            pass

    def export(self):
        csv, _ = QFileDialog.getSaveFileName(None, "RTKube data", "", "CSV (*.csv);;All Files (*)")
        if csv:
            if not csv.endswith(".csv"):
                csv = "{}.{}".format(csv, "csv")
            self.data.probeExport(csv)

    def received(  # pylint: disable=too-many-arguments, too-many-locals
        self,
        date,
        serialNumberGps,
        lat,
        lon,
        alt,
        calc,
        serialNumberGamma,
        radiometry,
        inclin,
        gammaup,
        gammadown,
        teneurup,
        teneurdown,
        coeffAup,
        coeffAlow,
        coeffBup,
        coeffBlow,
        coeffNup,
        coeffNlow,
    ):
        if self.data.isValid() and self.save:
            self.data.probeNewData(
                date,
                serialNumberGps,
                lat,
                lon,
                alt,
                calc,
                serialNumberGamma,
                radiometry,
                gammaup,
                gammadown,
                teneurup,
                teneurdown,
                inclin,
                coeffAup,
                coeffAlow,
                coeffBup,
                coeffBlow,
                coeffNup,
                coeffNlow,
            )

            radiometry = self.data.slabRadiometry(lat, lon)
            if radiometry:
                self.radWidget.mRadiometry.setText(str(radiometry[0]))
                self.radWidget.mProbeRadiometry.setText(
                    str(round(radiometry[1], 2)) if radiometry[1] is not None else "NULL"
                )

    def setDeposit(self, deposit):
        """
        Update deposit translations for NSah x,y,z labels
        Update deposit gamma coefficients for radiometry computation
        """

        if self.widget is None:
            return

        config = configparser.ConfigParser()
        config.read(self.projIni)
        self.widget.projDelta = None
        if deposit in config:
            try:
                self.widget.projDelta = tuple(float(config[deposit][c]) for c in "xyz")
            except (KeyError, ValueError):  # missing value or bad format value
                pass

        # Reload gamma coefficients to be always up to date
        config = configparser.ConfigParser(converters={"coeffs": misc.teneur_coeffs})
        config.read(self.gammaIni)

        # get all gamma coefficients from settings, if error in the file, set to None
        aUp1, aLow1, bUp1, bLow1, aUp2, aLow2, bUp2, bLow2, tUp, tLow = [None] * 10
        if deposit in config:
            try:
                aUp1, bUp1 = config[deposit].getcoeffs("teneur_sonde_haute_1", fallback=[None, None])
                aLow1, bLow1 = config[deposit].getcoeffs("teneur_sonde_basse_1", fallback=[None, None])
                aUp2, bUp2 = config[deposit].getcoeffs("teneur_sonde_haute_2", fallback=[None, None])
                aLow2, bLow2 = config[deposit].getcoeffs("teneur_sonde_basse_2", fallback=[None, None])
                tUp = config[deposit].getfloat("seuil_cps_sonde_haute", fallback=None)
                tLow = config[deposit].getfloat("seuil_cps_sonde_basse", fallback=None)
            except ValueError:  # bad format value
                pass
        if any(coeff is None for coeff in [aUp1, bUp1, aLow1, bLow1, aUp2, bUp2, aLow2, bLow2, tUp, tLow]):
            self.widget.gammaCoeffs = None
            QMessageBox.warning(
                None,
                self.tr("Gamma coefficients error"),
                self.tr(
                    "For deposit {0}:\n"
                    "Missing values (deposit or coefficient(s)), or wrong keywords, "
                    "or wrong formula in gamma coefficients parameter file."
                ).format(deposit),
            )
        else:
            self.widget.gammaCoeffs = aUp1, aLow1, bUp1, bLow1, aUp2, aLow2, bUp2, bLow2, tUp, tLow  # type: ignore
