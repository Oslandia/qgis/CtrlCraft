# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

# pylint: disable=import-outside-toplevel
# pylint: disable=invalid-name


def classFactory(iface):
    """Load CtrlCraft class from file CtrlCraft.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """

    from CtrlCraft.ctrlcraft import CtrlCraft

    return CtrlCraft(iface)
