# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Truck class
"""

from pathlib import Path

from qgis.core import Qgis
from qgis.gui import QgsMapToolEmitPoint
from qgis.PyQt.QtCore import (
    QObject,
    QPointF,
    QRectF,
    QSettings,
    pyqtSignal,
)
from qgis.PyQt.QtGui import (
    QBrush,
    QColor,
    QFont,
    QIcon,
    QPainter,
    QPixmap,
)
from qgis.PyQt.QtWidgets import QMessageBox

from CtrlCraft.logger import CtrlCraftLogger

qgisSettings = QSettings()


class CtrlCraftTruckLoadMapTool(QgsMapToolEmitPoint):  # pylint: disable=too-few-public-methods
    """
    Map tool allowing to add a slab in a truck.
    """

    def __init__(self, parent):
        self.truck = parent
        super().__init__(self.truck.mapCanvas)
        self.logger = CtrlCraftLogger()

    def canvasReleaseEvent(self, event):
        if self.truck.isFull():
            self.logger.logBar(self.tr("This truck is filled, you have to empty it first."), level=Qgis.Warning)

        else:
            isFull = self.truck.addLoad(event.mapPoint())
            if isFull:
                self.logger.logBar(self.tr("The truck ") + "{}".format(self.truck.id) + self.tr(" is filled."))


class CtrlCraftTruck(QObject):  # pylint: disable=too-many-instance-attributes
    """
    Truck class
    """

    updated = pyqtSignal()

    def __init__(self, tid, database, mapCanvas):
        """
        Constructor.
        """
        QObject.__init__(self)
        self.id = tid
        self.database = database
        self.mapCanvas = mapCanvas
        self.maxSlab = self.getMaxSlab()
        self.__actionLoad = None
        self.__actionUnload = None
        self.__actionState = None
        self.logger = CtrlCraftLogger()
        self.mapToolLoad = CtrlCraftTruckLoadMapTool(self)

        self.init()

    @property
    def loads(self):
        return self.database.truckSlabs(self.id)

    @property
    def actionLoad(self):
        return self.__actionLoad

    @actionLoad.setter
    def actionLoad(self, action):
        """Associate action fill to truck"""
        self.__actionLoad = action
        self.updateIconLoad()

    @property
    def actionUnload(self):
        return self.__actionUnload

    @actionUnload.setter
    def actionUnload(self, action):
        self.__actionUnload = action
        self.updateIconUnload()

    @property
    def actionState(self):
        return self.__actionState

    @actionState.setter
    def actionState(self, action):
        """Associate action fill to truck"""
        self.__actionState = action
        self.updateIconState()

    def activateMapTool(self):
        self.mapToolLoad.setAction(self.__actionLoad)
        self.mapCanvas.setMapTool(self.mapToolLoad)

    def removeLoads(self, params):
        """
        Empty the truck by removing loads

        :param params: list of [deposit, panel, blast]
        """
        self.database.truckUnloadSlabs(self.id)
        self.updateIconLoad()
        self.updateIconUnload()
        self.updateIconState()
        self.updated.emit()

    def addLoad(self, point):
        """Add a load in the truck"""
        if self.loads < self.maxSlab:
            if self.database.checkFixedMeasuresOnSlabs(point) == 0:
                text = self.tr(
                    "This slab has no fixed measure associated.\n"
                    "Do you really want to load it without a fixed measure ?"
                )
                dlg = QMessageBox(
                    QMessageBox.Information, self.tr("No fixed measure"), text, QMessageBox.Yes | QMessageBox.No
                )
                returnValue = dlg.exec()
                if returnValue == QMessageBox.No:
                    return False

            self.database.truckLoadSlab(self.id, point)
            self.updateIconLoad()
            self.updateIconUnload()
            self.updateIconState()

            self.updated.emit()

        return self.isFull()

    def updateIconLoad(self):
        """Update fill button icon"""
        if not self.__actionLoad:
            return

        name = self.loadIcon()
        pix = QPixmap(str(Path(__file__).parent / "img" / name))
        font = QFont()
        font.setPointSizeF(10)
        textPix = self.id

        position = QPointF(0, 60)
        painter = QPainter()
        painter.begin(pix)
        painter.setFont(font)
        painter.drawText(position, textPix)

        width = pix.width()
        if not self.isFull():
            width = (pix.width() / self.maxSlab) * self.loads
        rect = QRectF(0.0, 0.0, width, pix.height())
        brush = QBrush(QColor(255, 0, 0, 150))
        painter.fillRect(rect, brush)

        painter.end()

        icon = QIcon(pix)

        self.__actionLoad.setIcon(icon)

    def updateIconUnload(self):
        """Update empty button icon"""
        if not self.__actionUnload:
            return

        iconName = self.unloadIcon()

        pix = QPixmap(str(Path(__file__).parent / "img" / iconName))
        font = QFont()
        font.setPointSizeF(10)
        textPix = self.id

        position = QPointF(0, 60)
        painter = QPainter()
        painter.begin(pix)
        painter.setFont(font)
        painter.drawText(position, textPix)
        painter.end()

        icon = QIcon(pix)

        self.__actionUnload.setIcon(icon)

        self.__actionUnload.setEnabled(not self.isEmpty())

    def updateIconState(self):
        """Update fill button icon"""
        if not self.__actionState:
            return

        name = self.stateIcon()
        pix = QPixmap(str(Path(__file__).parent / "img" / name))
        font = QFont()
        font.setPointSizeF(10)
        probe = self.database.getCurrentProbeRadiometry(self.id)
        if probe:
            textPix = str(probe)[:5]
            probe = float(probe)

            rect = QRectF(0.0, 0.0, pix.width(), pix.height())
            if probe <= 0.6:
                brush = QBrush(QColor(140, 140, 140))
            elif 0.6 < probe <= 0.9:
                brush = QBrush(QColor(222, 200, 0))
            elif 0.9 < probe <= 1.2:
                brush = QBrush(QColor(0, 217, 0))
            elif 1.2 < probe <= 1.8:
                brush = QBrush(QColor(206, 0, 0))
            elif 1.8 < probe <= 3:
                brush = QBrush(QColor(0, 95, 213))
            elif 3 < probe:
                brush = QBrush(QColor(142, 0, 215))
            painter = QPainter()
            painter.begin(pix)
            painter.fillRect(rect, brush)

            position = QPointF(0, 60)
            painter.setFont(font)
            painter.drawText(position, textPix)

            painter.end()
        else:
            textPix = "ND"
            position = QPointF(0, 60)
            painter = QPainter()
            painter.begin(pix)
            painter.setFont(font)
            painter.drawText(position, textPix)
            rect = QRectF(0.0, 0.0, pix.width(), pix.height())
            brush = QBrush(QColor(255, 255, 255, 0))
            painter.fillRect(rect, brush)

            painter.end()

        icon = QIcon(pix)

        self.__actionState.setIcon(icon)

    def loadIcon(self):
        """Returns the initial fill icon"""
        return "icon_truck.png"

    def unloadIcon(self):
        """Returns the empty icon"""
        return "icon_truck_empty.png"

    def stateIcon(self):
        """Returns the white icon"""
        return "white_logo.png"

    def init(self):
        """Init the state of the truck"""
        self.updateIconLoad()
        self.updateIconUnload()
        self.updateIconState()

    def getMaxSlab(self):
        """Retrieve slab capacity from params.ini file"""
        maxSlab = 8
        for ind in range(1, 99):
            if qgisSettings.value("CtrlCraft/Truck{}/id".format(ind), ind) == str(self.id):
                maxSlab = int(qgisSettings.value("CtrlCraft/Truck{}/sc".format(ind), 8))
                break
        return maxSlab

    def isFull(self):
        """Check if the truck is full"""
        return self.loads == self.maxSlab

    def isEmpty(self):
        return self.loads == 0
