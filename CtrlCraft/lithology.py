# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Module with some tools to manipulate lithologies.
"""

from qgis.gui import QgsMapToolEmitPoint
from qgis.PyQt.QtCore import pyqtSignal


class CtrlCraftLithologyUpdateMapTool(QgsMapToolEmitPoint):  # pylint: disable=too-few-public-methods
    """
    Map tool to set a lithology on a clicked slab
    """

    lithologyUpdated = pyqtSignal()

    def __init__(self, name, mapCanvas, database):
        super().__init__(mapCanvas)
        self.database = database
        self.name = name

    def canvasReleaseEvent(self, event):
        point = event.mapPoint()

        if self.database.isValid():
            self.database.lithologyUpdate(self.name, point)

        self.lithologyUpdated.emit()
