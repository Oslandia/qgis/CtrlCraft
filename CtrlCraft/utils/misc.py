# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

"""
Miscellaneous utils functions
"""

import re
import unicodedata
from functools import lru_cache
from inspect import currentframe

from qgis.PyQt.QtWidgets import QApplication, qApp


class CtrlCraftProgress:
    """
    wraps progress bar behevior. Used to separate UI from business code.
    """

    def __init__(self, label, progressBar):
        self.__label = label
        self.__progressBar = progressBar

    def reset(self, label):
        self.__label.setText(tr(label))
        qApp.processEvents()
        self.__progressBar.setValue(0)
        self.__progressBar.setMinimum(0)
        self.__progressBar.setMaximum(100)

    def setValue(self, val):
        self.__progressBar.setValue(val)


class Singleton(type):
    """Singleton implementation"""

    _instances = {}

    def __call__(cls, *args, **kwargs):  # @NoSelf
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(
                *args, **kwargs
            )  # parameters Singleton, cls are mandatory
        return cls._instances[cls]


@lru_cache(maxsize=128)
def tr(text: str, context: str = None) -> str:
    """Translate a text using the installed translator.

    :param text: text to translate, defaults to None.
    :type text: str
    :param context: where the text is located. In Python code, it's the class name. \
    If None, it tries to automatically retrieve class name. Defaults to None.
    :type context: str, optional

    :return: translated text.
    :rtype: str
    """
    if not context:
        frame = currentframe().f_back
        if "self" in frame.f_locals:
            context = type(frame.f_locals["self"]).__name__

    if not context:
        context = "CtrlCraft"

    return QApplication.translate(context, text)


def stripAccents(text):
    """
    Strip accents from input String.

    :param text: The input string.
    :type text: String.

    :returns: The processed String.
    :rtype: String.
    """
    text = unicodedata.normalize("NFD", text)
    text = text.encode("ascii", "ignore")
    text = text.decode("utf-8")
    return text


def textToId(text):
    """
    Convert input text to id.

    :param text: The input string.
    :type text: String.

    :returns: The processed String.
    :rtype: String.
    """
    text = stripAccents(text.lower())
    text = re.sub("[ ]+", "_", text)
    text = re.sub("[^0-9a-zA-Z_-]", "", text)
    return text


def teneur_coeffs(conf):
    """
    Convert formula from gamma.ini file into coefficients

    :param text: the value of the config file
    :type text: string

    :returns: coefficients a and b
    :rtype: tuple of float
    """
    try:
        a = float(conf.split(" * ")[0])
        b = float(conf.split(" ^ ")[1])
    except Exception:
        return None, None

    return a, b
