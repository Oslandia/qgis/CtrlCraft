create extension IF NOT EXISTS postgis
;

create extension IF NOT EXISTS pgcrypto
;

drop schema if exists ctrlcraft cascade
;

create schema ctrlcraft
;

-- the uuid package is not easilly available on windows, so we create our own
create sequence ctrlcraft.unique_name_seq
;

create or replace function ctrlcraft.unique_id()
returns varchar
language plpgsql volatile
as
$$
  begin
    return gen_random_uuid();
  end
$$
;

-- enum types
DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'lithology') THEN
        CREATE TYPE lithology AS ENUM ('Argile', 'Grès', 'Argile Oxydé', 'Grès Oxydé', 'ND');
    END IF;
END$$
;

-- table storing some settings
create table ctrlcraft.settings
(
  name varchar,
  value varchar
)
;

insert into ctrlcraft.settings(name, value) values('probe_interval', '20');

-- table storing data from the ore smu file
create table ctrlcraft.smus
(
  id varchar primary key default ctrlcraft.unique_id(),
  smu_id varchar,
  deposit varchar,
  panel varchar,
  blast varchar,
  radiometry float,  -- u_krigee / quality value
  lithology lithology,
  percentage float,  -- recognition percentage for lithology
  unit varchar,
  trans_x double precision,
  trans_y double precision,
  trans_z double precision,
  geom geometry(PointZ, 32632)
)
;

create table ctrlcraft.truck_loads
(
  id varchar primary key default ctrlcraft.unique_id(),
  truck varchar,
  load_time timestamp,
  unload_time timestamp
)
;

-- table for all slabs
create table ctrlcraft.slabs
(
  id varchar primary key default ctrlcraft.unique_id(),
  smu varchar references ctrlcraft.smus(id),
  load varchar references ctrlcraft.truck_loads(id),
  deposit varchar,
  panel varchar,
  blast varchar,
  radiometry float,  -- u_krigee / quality value; this value is not supposed to change during CtrlCraft use
  radiometry_probe float,  -- to update when a new fixed measure is added
  lithology lithology,
  unit varchar,
  geom geometry(MultiPolygonZ, 32632),
  centroid_z float, -- too long to retrieve from a MultiPolygonZ
  affectedradio varchar default NULL -- when radiometry is updated by the CAN OP
  )
;

create table ctrlcraft.trucks
(
  id varchar primary key,
  loads varchar[4]
)
;

-- table for co3 separation
CREATE TABLE ctrlcraft.co3_line (
    id varchar primary key default ctrlcraft.unique_id(),
    comments text NOT NULL,
    geom geometry(LineString,32632)
)
;

CREATE INDEX sidx_co3_line_geom ON ctrlcraft.co3_line USING gist (geom)
;

-- table for radiometry class thresholds
CREATE TABLE ctrlcraft.radiometry_class_thresholds (
  id int primary key,
  min numeric NOT NULL,
  max numeric NOT NULL,
  checked bool NOT NULL,
  color varchar(7) NOT NULL
)
;

INSERT INTO ctrlcraft.radiometry_class_thresholds values
  (0, -999, -998.99, true, '#FFFFFF'),
  (1, 0, 0.7, true, '#B2B2B2'),
  (2, 0.7, 1.3, true, '#00FF00'),
  (3, 1.3, 1.4, true, '#FFE600'),
  (4, 1.4, 1.8, true, '#FF0000'),
  (5, 1.8, 2.5, true, '#0073FF'),
  (6, 2.5, 1000, true, '#A800FF')
;

create view ctrlcraft.slabs_available as
select slabs.*, rc.id as radiometry_class
from ctrlcraft.slabs
left join ctrlcraft.radiometry_class_thresholds rc on
  (COALESCE(radiometry_probe, radiometry) >= min and COALESCE(radiometry_probe, radiometry) < max)
where radiometry is not null and load is null order by centroid_z
;

create view ctrlcraft.truck_loads_status as
  with t as (
    SELECT count(*) AS count,
      array_agg(s.id) AS slabs,
      array_agg(s.lithology) AS lithologies,
      l.truck,
      s.deposit,
      s.blast,
      s.panel,
      avg(s.radiometry) AS radiometry,
      avg(s.radiometry_probe) AS radiometry_probe,
    COALESCE(to_char(l.unload_time, 'YYYY-MM-DD HH24:MI:SS'::text), ''::text) AS unload_time
    FROM (ctrlcraft.slabs s
    JOIN ctrlcraft.truck_loads l ON (((s.load)::text = (l.id)::text)))
    WHERE (s.load IS NOT NULL)
    GROUP BY l.truck, s.deposit, s.panel, s.blast, l.unload_time
    ORDER BY COALESCE(to_char(l.unload_time, 'YYYY-MM-DD HH24:MI:SS'::text), ''::text)
  )
select count::text, slabs, lithologies, truck, deposit, blast, panel, radiometry::text, radiometry_probe::text, unload_time,
    'M' || rc.id::text as quality
  from t
  LEFT JOIN ctrlcraft.radiometry_class_thresholds rc on (radiometry_probe >= rc.min and radiometry_probe < rc.max)
;

create table ctrlcraft.probe
(
  id varchar primary key default ctrlcraft.unique_id(),
  radiometry float,
  alt float,
  inclination float,
  gammaup float,
  gammadown float,
  teneurup float,
  teneurdown float,
  time timestamp,
  serial_number_gps varchar,
  serial_number_gamma varchar,
  status int,
  coeff_a_up float,
  coeff_a_low float,
  coeff_b_up float,
  coeff_b_low float,
  coeff_n_up float,
  coeff_n_low float,
  geom geometry(Point, 32632)
)
;
CREATE INDEX IF NOT EXISTS probe_geom_idx ON ctrlcraft.probe USING gist (geom);

CREATE OR REPLACE VIEW ctrlcraft.probe_last_positions AS
(
	SELECT probe.*, rc.id as radiometry_class
	FROM ctrlcraft.probe
  LEFT JOIN ctrlcraft.radiometry_class_thresholds rc on (radiometry >= rc.min and radiometry < rc.max)
	WHERE "time" >=
		(
			SELECT max("time") -
				(
					SELECT "value"
					FROM ctrlcraft.settings
					WHERE "name" = 'probe_interval'
				)::interval
			FROM ctrlcraft.probe
		)
	ORDER BY "time" DESC
);

CREATE
OR REPLACE VIEW ctrlcraft.levels AS (
  SELECT
    DISTINCT centroid_z,
    ROW_NUMBER() OVER(
      ORDER BY
        centroid_z DESC
    ) id,
    Dense_rank() OVER(
      PARTITION BY
        deposit,
        panel,
        blast
      ORDER BY
        centroid_z DESC
    ) lvl,
    deposit,
    panel,
    blast,
    ST_Envelope(st_collect(geom))::geometry('POLYGON', 32632) as geom
  FROM
    ctrlcraft.slabs
  GROUP BY
    centroid_z,
    deposit,
    panel,
    blast
  ORDER BY
    centroid_z DESC
);

CREATE
OR REPLACE VIEW ctrlcraft.topo_control as (
  WITH lp as (
    SELECT
      *
    FROM
      ctrlcraft.probe_last_positions
    ORDER BY
      time DESC
    LIMIT
      1
  )
  SELECT
    s.centroid_z,
    l.lvl,
    l.maxlvl,
    (
      SELECT
        alt
      FROM
        lp
    ),
    (
      SELECT
        geom
      FROM
        lp
    ),
    (
      (
        SELECT
          alt
        FROM
          lp
      ) - s.centroid_z
    ) as delta
  FROM
    ctrlcraft.slabs_available s
    LEFT JOIN (
      SELECT
        centroid_z,
        lvl,
        m.maxlvl
      from
        ctrlcraft.levels CROSS
        JOIN (
          SELECT
            max(lvl) maxlvl
          FROM
            ctrlcraft.levels
        ) m
    ) l ON s.centroid_z = l.centroid_z
  WHERE
    st_intersects(
      geom,
      (
        SELECT
          geom
        FROM
          lp
      )
    )
  ORDER BY
    centroid_z DESC
  LIMIT
    1
);

CREATE TABLE ctrlcraft.fixed_measure(
id varchar primary KEY default ctrlcraft.unique_id(),
slabid varchar,
radioup double precision,
radiolow double precision,
inclination float,
modedeg bool default false,  -- mode degrade
geom geometry(POINT, 32632),
probe_ids varchar[]  -- liste des mesures utilisees pour cette fixed_measure
);
CREATE INDEX IF NOT EXISTS fixed_measure_geom_idx ON ctrlcraft.fixed_measure USING gist (geom);

-- table storing co3 concentration from the Co3 file
create table ctrlcraft.co3
(
  deposit varchar,
  panel varchar,
  blast varchar,
  depth float,
  co3 float,
  geom geometry(PointZ, 32632)
)
;

--function to check that a fix measures has been taken before loading a slab
create function ctrlcraft.check_fixed_meaures_on_slabs(wkt varchar)
RETURNS int
as
$$
DECLARE
  slab_id varchar;
  nb int;
BEGIN
  -- search the more above available slab at the given point (in wkt)
  slab_id := (select id
             from ctrlcraft.slabs_available
             where st_intersects(geom, st_geomfromtext(wkt, 32632))
             order by centroid_z DESC limit 1);

  IF slab_id is not null
  THEN
      nb := (select COUNT(*) from ctrlcraft.fixed_measure f where f.slabid = slab_id);
  ELSE
      nb = 0;
  END IF;
  return nb;
END;
$$ language plpgsql
;


-- function to load a truck with a slab
create function ctrlcraft.truck_load_slab(truckid varchar, wkt varchar)
RETURNS void
as
$$
DECLARE
  slabid varchar;
  loadid varchar;
  z_value float;
  tolerance float;
BEGIN
  tolerance := 0.00001;

  -- search the more above available slab at the given point (in wkt)
  z_value := (select ST_Z(st_geomfromtext(wkt,32632)));

  if z_value is not null
  then
  slabid := (select id
             from ctrlcraft.slabs_available
             where st_intersects(geom, st_geomfromtext(wkt, 32632))
             and abs(centroid_z - z_value) < tolerance
             order by centroid_z DESC limit 1);
  else
  slabid := (select id
             from ctrlcraft.slabs_available
             where st_intersects(geom, st_geomfromtext(wkt, 32632))
             order by centroid_z DESC limit 1);
  end if;

  if slabid is not null
  then
    -- new load
    insert into ctrlcraft.truck_loads(truck, load_time) values(truckid, now())
    returning id into loadid;

    -- update truck with loads
    update ctrlcraft.trucks set loads = loads || loadid where id = truckid;

    -- update slab with load
    update ctrlcraft.slabs set load = loadid where id = slabid;
  else
    RAISE info '%', format('No available slabs at %s.', wkt);
  end if;
END;
$$ language plpgsql
;

-- function to unload the slabs from the truck
create function ctrlcraft.truck_unload_slabs(truckid varchar)
RETURNS void
as
$$
DECLARE
BEGIN
  -- update departure time for loads
  update ctrlcraft.truck_loads set unload_time = now()
  where id in (select unnest(loads) from ctrlcraft.trucks where id = truckid);

  -- empty the trucks
  update ctrlcraft.trucks set loads = null where id = truckid;
END;
$$ language plpgsql
;

-- function to unload the last slab (whatever the truck)
create function ctrlcraft.truck_unload_last_slab()
RETURNS varchar
as
$$
DECLARE
  loadid varchar;
  truckid varchar;
BEGIN
  -- search the more recent inserted slab in trucks
  loadid := (select id from ctrlcraft.truck_loads
             where id in (select unnest(loads) from ctrlcraft.trucks)
             order by load_time DESC limit 1);

  if loadid is not null
  then
    -- update slab with load
    update ctrlcraft.slabs set load = null where load = loadid;

    -- remove load from trucks
    truckid := (select id from ctrlcraft.trucks where loadid = ANY(loads));
    update ctrlcraft.trucks set loads = array_remove(loads, loadid)
    where id = truckid;

    -- remove load
    delete from ctrlcraft.truck_loads where id = loadid;
  end if;

  return truckid;
END;
$$ language plpgsql
;

create function ctrlcraft.lithology_update(_lithology lithology, wkt varchar)
RETURNS void
as
$$
DECLARE
  slabid varchar;
BEGIN
  -- search the more above available slab at the given point (in wkt)
  slabid := (select id
             from ctrlcraft.slabs_available
             where st_intersects(geom, st_geomfromtext(wkt, 32632))
             order by centroid_z DESC limit 1);

  if slabid is not null
  then
    update ctrlcraft.slabs set lithology = _lithology where id = slabid;
  end if;
end;
$$ language plpgsql
;

CREATE FUNCTION ctrlcraft.add_fix_measure(_sondeUp int, _sondeLow int)
RETURNS void
  AS
$$
DECLARE
    avgcps double precision;
    coeff_a double precision;
    coeff_b double precision;
    coeff_n double precision;
    radiometry double precision;
    inc float;
    pt geometry(POINT, 32632);
    slabid varchar;
    probe_ids varchar[];
BEGIN
    -- get coefficients and position from last 4 seconds
    -- we must be sure that the coefficients used for every measurement is the same
    -- otherwise it will be null and we will not save a fixed_measure
    SELECT
    CASE
      WHEN _sondeUp = 1 THEN
        CASE WHEN array_length(array_agg(DISTINCT coeff_a_up), 1) = 1 THEN max(coeff_a_up) END
      WHEN _sondeLow = 1 THEN
        CASE WHEN array_length(array_agg(DISTINCT coeff_a_low), 1) = 1 THEN max(coeff_a_low) END
    END,
    CASE
      WHEN _sondeUp = 1 THEN
        CASE WHEN array_length(array_agg(DISTINCT coeff_b_up), 1) = 1 THEN max(coeff_b_up) END
      WHEN _sondeLow = 1 THEN
        CASE WHEN array_length(array_agg(DISTINCT coeff_b_low), 1) = 1 THEN max(coeff_b_low) END
    END,
    CASE
      WHEN _sondeUp = 1 THEN
        CASE WHEN array_length(array_agg(DISTINCT coeff_n_up), 1) = 1 THEN max(coeff_n_up) END
      WHEN _sondeLow = 1 THEN
        CASE WHEN array_length(array_agg(DISTINCT coeff_n_low), 1) = 1 THEN max(coeff_n_low) END
    END,
    CASE WHEN _sondeUp = 1 THEN AVG(gammaup) WHEN _sondeLow = 1 THEN AVG(gammadown) END,
    AVG(inclination),
    ST_Centroid (St_Collect (geom)),
    array_agg(id)
		INTO coeff_a, coeff_b, coeff_n, avgcps, inc, pt, probe_ids
    FROM
        ctrlcraft.probe
    WHERE
        "time" >= (
            SELECT
                max("time") - interval '4' second
            FROM
                ctrlcraft.probe);
    -- search the more above available slab at the given point
    slabid := (
        SELECT
            id
        FROM
            ctrlcraft.slabs_available
        WHERE
            st_intersects (geom, pt)
        ORDER BY
            centroid_z DESC
        LIMIT 1);

    -- compute radiometry
    radiometry := coeff_a * POWER(avgcps * coeff_n, coeff_b);

    IF slabid IS NOT NULL and radiometry IS NOT NULL THEN
      -- insert radiometry for the fixed_measure
      INSERT INTO ctrlcraft.fixed_measure(slabid, radioup, radiolow, inclination, geom, probe_ids)
      VALUES (slabid, radiometry * _sondeUp, radiometry * _sondeLow, inc, pt, probe_ids);

      -- update radiometry for the slab
      PERFORM ctrlcraft.affect_radiometry_slab(slabid);
    END IF;
END;
$$
LANGUAGE plpgsql;

CREATE FUNCTION ctrlcraft.add_fix_measure_degraded(_teneurUp double precision, _teneurLow double precision, _inclination double precision, _wkt varchar )
RETURNS void
AS
$$
DECLARE
  slabid varchar;
BEGIN
  slabid := (
    SELECT
      id
    FROM
      ctrlcraft.slabs_available
    WHERE
      st_intersects(geom, st_geomfromtext(_wkt, 32632))
    ORDER BY
      centroid_z DESC
    LIMIT 1);

    IF slabid is NOT NULL THEN
      -- insert radiometry for the sonde
      INSERT INTO ctrlcraft.fixed_measure(slabid, radioup, radiolow, inclination, modedeg, geom) VALUES (slabid, _teneurUp, _teneurLow, _inclination, true, st_geomfromtext(_wkt, 32632));
      -- update radiometry for the slab
      PERFORM ctrlcraft.affect_radiometry_slab(slabid);
    END IF;
END;
$$
LANGUAGE plpgsql;

CREATE FUNCTION ctrlcraft.affect_radiometry_slab(_slabid varchar)
    RETURNS void
    AS
$$
DECLARE
  nbRadioUp int;
  nbRadioLow int;
  sumRadioUp double precision;
  sumRadioLow double precision;
BEGIN
  SELECT SUM(radioup), sum(radiolow), count(*) FILTER (WHERE radioup > 0), count(*) FILTER (WHERE radiolow > 0)
  INTO sumRadioUp, sumRadioLow, nbRadioUp, nbRadioLow
  FROM ctrlcraft.fixed_measure
  WHERE
    slabid = _slabid;

  -- ignore gamma measurements values of 0
  IF nbRadioUp + nbRadioLow > 0 THEN

    -- update slabs with mean radiometry
    UPDATE
      ctrlcraft.slabs
    SET
      radiometry_probe = (sumRadioUp + sumRadioLow ) / (nbRadioUp + nbRadioLow)
    , affectedradio = nbRadioUp::varchar || '-' || nbRadioLow::varchar
    WHERE
      id = _slabid;

  END IF;

END;
$$
LANGUAGE plpgsql;

-- view with distinct deposits
create view ctrlcraft.deposits as
(
  select distinct deposit as id from ctrlcraft.smus order by id
)
;

-- view with distinct panels
create view ctrlcraft.panels as
(
  select distinct panel as id, deposit from ctrlcraft.smus order by id
)
;

-- view with distinct blast
create view ctrlcraft.blasts as
(
  select distinct blast as id, deposit, panel from ctrlcraft.smus
)
;

create view ctrlcraft.measurements_slab as
(
 SELECT slabs.id AS slab,
    slabs.deposit,
    slabs.panel,
    slabs.blast,
    slabs.lithology,
    tl.truck,
    tl.load_time,
    tl.unload_time,
    slabs.radiometry AS u_krigee,
    co3.co3,
    slabs.unit,
    slabs.radiometry_probe,
    smus.trans_x AS delta_x,
    smus.trans_y AS delta_y,
    smus.trans_z AS delta_z,
    round((public.st_x(public.st_centroid(slabs.geom)))::numeric, 3) AS x_wgs84,
    round((public.st_y(public.st_centroid(slabs.geom)))::numeric, 3) AS y_wgs84,
    round((slabs.centroid_z)::numeric, 3) AS z_wgs84,
    round((public.st_x(public.st_centroid(public.st_collect(probe.geom))))::numeric, 3) AS x_meas,
    round((public.st_y(public.st_centroid(public.st_collect(probe.geom))))::numeric, 3) AS y_meas,
    round((avg(probe.alt))::numeric, 3) AS z_meas,
    round((public.st_x(public.st_transform(public.st_centroid(slabs.geom), 4326)))::numeric, 7) AS lon_wgs84,
    round((public.st_y(public.st_transform(public.st_centroid(slabs.geom), 4326)))::numeric, 7) AS lat_wgs84,
    round((public.st_x(public.st_transform(public.st_centroid(public.st_collect(probe.geom)), 4326)))::numeric, 7) AS lon_meas,
    round((public.st_y(public.st_transform(public.st_centroid(public.st_collect(probe.geom)), 4326)))::numeric, 7) AS lat_meas,
    count(probe.*) AS nb_meas,
    max(probe."time") AS date_last,
    min(probe."time") AS date_first,
    avg(probe.gammadown) AS cps_low,
    avg(probe.gammaup) AS cps_up,
    slabs.affectedradio,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_a_low), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_low)
        END AS a_low,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_a_up), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_up)
        END AS a_up,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_b_low), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_low)
        END AS b_low,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_b_up), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_up)
        END AS b_up,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_n_low), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_low)
        END AS n_low,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_n_up), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_up)
        END AS n_up,
    string_agg(DISTINCT (probe.serial_number_gamma)::text, ' | '::text) AS serial_number_probe,
    string_agg(DISTINCT (probe.serial_number_gps)::text, ' | '::text) AS serial_number_cube
   FROM (((((ctrlcraft.probe
     LEFT JOIN ctrlcraft.fixed_measure f ON (((probe.id)::text = ANY ((f.probe_ids)::text[]))))
     RIGHT JOIN ctrlcraft.slabs ON (((slabs.id)::text = (f.slabid)::text)))
     JOIN ctrlcraft.truck_loads tl ON (((tl.id)::text = (slabs.load)::text)))
     JOIN ctrlcraft.smus ON (((smus.id)::text = (slabs.smu)::text)))
     LEFT JOIN ctrlcraft.co3 ON (public.st_intersects(public.st_buffer(co3.geom, (0.1)::double precision), slabs.geom)))
  WHERE (((slabs.deposit)::text <> 'Etalonnage'::text) AND (NOT (((tl.truck)::text = '-1'::text) AND (slabs.radiometry_probe IS NULL))))
  GROUP BY slabs.id, tl.id, co3.co3, smus.id
)
;

CREATE VIEW ctrlcraft.measurements_slab_not_loaded AS
(
 SELECT slabs.id AS slab,
    slabs.deposit,
    slabs.panel,
    slabs.blast,
    slabs.lithology,
    slabs.radiometry AS u_krigee,
    co3.co3,
    slabs.unit,
    slabs.radiometry_probe,
    smus.trans_x AS delta_x,
    smus.trans_y AS delta_y,
    smus.trans_z AS delta_z,
    round((public.st_x(public.st_centroid(slabs.geom)))::numeric, 3) AS x_wgs84,
    round((public.st_y(public.st_centroid(slabs.geom)))::numeric, 3) AS y_wgs84,
    round((slabs.centroid_z)::numeric, 3) AS z_wgs84,
    round((public.st_x(public.st_centroid(public.st_collect(probe.geom))))::numeric, 3) AS x_meas,
    round((public.st_y(public.st_centroid(public.st_collect(probe.geom))))::numeric, 3) AS y_meas,
    round((avg(probe.alt))::numeric, 3) AS z_meas,
    count(probe.*) AS nb_meas,
    max(probe."time") AS date_last,
    min(probe."time") AS date_first,
    avg(probe.gammadown) AS cps_low,
    avg(probe.gammaup) AS cps_up,
    slabs.affectedradio,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_a_low), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_low)
        END AS a_low,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_a_up), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_up)
        END AS a_up,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_b_low), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_low)
        END AS b_low,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_b_up), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_up)
        END AS b_up,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_n_low), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_low)
        END AS n_low,
        CASE
            WHEN (array_length(array_agg(DISTINCT probe.coeff_n_up), 1) > 1) THEN ('-1'::integer)::double precision
            ELSE max(probe.coeff_a_up)
        END AS n_up,
    string_agg(DISTINCT (probe.serial_number_gamma)::text, ' | '::text) AS serial_number_probe,
    string_agg(DISTINCT (probe.serial_number_gps)::text, ' | '::text) AS serial_number_cube
   FROM ((((ctrlcraft.probe
     JOIN ctrlcraft.fixed_measure f ON (((probe.id)::text = ANY ((f.probe_ids)::text[]))))
     JOIN ctrlcraft.slabs ON (((slabs.id)::text = (f.slabid)::text)))
     JOIN ctrlcraft.smus ON (((smus.id)::text = (slabs.smu)::text)))
     LEFT JOIN ctrlcraft.co3 ON (public.st_intersects(public.st_buffer(co3.geom, (0.1)::double precision), slabs.geom)))
  WHERE (((slabs.deposit)::text <> 'Etalonnage'::text) AND (slabs.load IS NULL))
  GROUP BY slabs.id, co3.co3, smus.id
)
;

-- ---------------------------------------------------------------------------
--
-- Utility functions
--
-- ---------------------------------------------------------------------------
-- function to update probe radiometry for a slab
create function ctrlcraft.slab_update_probe_radiometry(
  _x double precision,
  _y double precision,
  _radiometry float
)
RETURNS setof void
as
$$
DECLARE
  point geometry;  -- Point
  mean_radiometry float;
  slab_id varchar;
BEGIN
  point := st_setsrid(st_point(_x, _y), 32632);
  slab_id := (select id from ctrlcraft.slabs_available
              where st_intersects(geom, point)
              order by centroid_z DESC limit 1);

  IF slab_id IS NOT NULL
  THEN
    mean_radiometry := (select radiometry_probe from ctrlcraft.slabs where id = slab_id);
    IF mean_radiometry IS NOT NULL
    THEN
      mean_radiometry := (mean_radiometry + _radiometry) / 2;
    ELSE
      mean_radiometry = _radiometry;
    END IF;
    update ctrlcraft.slabs set radiometry_probe = mean_radiometry where id = slab_id;
  END IF;
END;
$$ language plpgsql volatile
;

-- function to insert a smu
-- SMU dimension: 5 meters x 5 meters x 0.5 meters
-- slab dimension: 2.5 meters x 2.5 meters x 0.5 meters
create function ctrlcraft.insert_smu(
  _smuid text,
  _deposit text,
  _panel text,
  _blast text,
  _quality float,
  _lithology text,
  _percentage float,
  _unit text,
  _x double precision,
  _y double precision,
  _z float,
  _trans_x double precision,
  _trans_y double precision,
  _trans_z double precision)
RETURNS setof void
as
$$
DECLARE
  smu_thick constant float := 0.5;  -- meters
  smu_side constant float := 5;  -- meters
  slab_side constant float := smu_side/2;
  slab0_centroid geometry;  -- PointZ
  slab0 geometry;  -- MultiPolygonZ
  slab1_centroid geometry;  -- PointZ
  slab1 geometry;  -- MultiPolygonZ
  slab2_centroid geometry;  -- PointZ
  slab2 geometry;  -- MultiPolygonZ
  slab3_centroid geometry;  -- PointZ
  slab3 geometry;  -- MultiPolygonZ
  lithology lithology;
  smu_id varchar;
BEGIN
  -- clean lithology text
  IF _lithology = 'argiles'
  THEN
    lithology = 'Argile';
  ELSIF _lithology = 'gres'
  THEN
    lithology = 'Grès';
  ELSIF _lithology = 'gres oxyde'
  THEN
    lithology = 'Grès Oxydé';
  ELSIF _lithology = 'argiles oxyde'
  THEN
    lithology = 'Argile Oxydé';
  ELSE
    lithology = 'ND';
  END IF;

  -- insert new SMU
  insert into ctrlcraft.smus(
    smu_id,
    deposit,
    panel,
    blast,
    radiometry,
    lithology,
    percentage,
    unit,
    trans_x,
    trans_y,
    trans_z,
    geom
  ) values(
    _smuid,
    _deposit,
    _panel,
    _blast,
    _quality,
    lithology,
    _percentage,
    _unit,
    _trans_x,
    _trans_y,
    _trans_z,
    ST_SetSrid( ST_MakePoint( _x, _y, _z ), 32632 )
  )
  returning id into smu_id;

  -- build 3D slabs from SMU centroid
  slab0_centroid := ST_SetSrid( ST_MakePoint( _x - slab_side/2, _y + slab_side/2, _z ), 32632 );
  slab0 := ctrlcraft.create_3d_cube( slab0_centroid, slab_side, smu_thick );
  insert into ctrlcraft.slabs(smu, deposit, panel, blast, radiometry, lithology, unit, geom, centroid_z) values(smu_id, _deposit, _panel, _blast, _quality, lithology, _unit, slab0, _z);

  slab1_centroid := ST_SetSrid( ST_MakePoint( _x + slab_side/2, _y + slab_side/2, _z ), 32632 );
  slab1 := ctrlcraft.create_3d_cube( slab1_centroid, slab_side, smu_thick );
  insert into ctrlcraft.slabs(smu, deposit, panel, blast, radiometry, lithology, unit, geom, centroid_z) values(smu_id, _deposit, _panel, _blast, _quality, lithology, _unit, slab1, _z);

  slab2_centroid := ST_SetSrid( ST_MakePoint( _x + slab_side/2, _y - slab_side/2, _z ), 32632 );
  slab2 := ctrlcraft.create_3d_cube( slab2_centroid, slab_side, smu_thick );
  insert into ctrlcraft.slabs(smu, deposit, panel, blast, radiometry, lithology, unit, geom, centroid_z) values(smu_id, _deposit, _panel, _blast, _quality, lithology, _unit, slab2, _z);

  slab3_centroid := ST_SetSrid( ST_MakePoint( _x - slab_side/2, _y - slab_side/2, _z ), 32632 );
  slab3 := ctrlcraft.create_3d_cube( slab3_centroid, slab_side, smu_thick );
  insert into ctrlcraft.slabs(smu, deposit, panel, blast, radiometry, lithology, unit, geom, centroid_z) values(smu_id, _deposit, _panel, _blast, _quality, lithology, _unit, slab3, _z);
END;
$$ language plpgsql volatile
;

-- returns a centroid from a cube (z is not exact, but it's enough for our use
-- case)
create function ctrlcraft.cube_centroid(cube geometry)
RETURNS geometry
as
$$
DECLARE
  z double precision;
  centroid geometry;
BEGIN
  select AVG(zz) into z as avgz from ( select st_z(st_astext((st_dumppoints(cube)).geom)) as zz ) as zs;
  centroid := ST_MakePoint(ST_X(ST_Centroid(cube)), ST_Y(ST_Centroid(cube)), z);
  RETURN centroid;
END;
$$ language plpgsql volatile
;

-- function to build a 3D cube from its centroid
/*
       b _____________  c
        /|           /|
       / |          / |
      /  |         /  |
   a /___|________/ d |
     | F |________|___| g
     |   /        |   /
     |  /         |  /
     | /          | /
   e |/___________|/ h
*/
create function ctrlcraft.create_3d_cube(_centroid geometry, _side float, _thick float)
RETURNS geometry
as
$$
DECLARE
  a_x double precision;
  a_y double precision;
  a_z double precision;
  b_x double precision;
  b_y double precision;
  b_z double precision;
  c_x double precision;
  c_y double precision;
  c_z double precision;
  d_x double precision;
  d_y double precision;
  d_z double precision;
  e_x double precision;
  e_y double precision;
  e_z double precision;
  f_x double precision;
  f_y double precision;
  f_z double precision;
  g_x double precision;
  g_y double precision;
  g_z double precision;
  h_x double precision;
  h_y double precision;
  h_z double precision;
  geom_string text;
BEGIN

  a_x := ST_X(_centroid) - _side / 2;
  a_y := ST_Y(_centroid) - _side / 2;
  a_z := ST_Z(_centroid) + _thick / 2;

  b_x := ST_X(_centroid) - _side / 2;
  b_y := ST_Y(_centroid) + _side / 2;
  b_z := ST_Z(_centroid) + _thick / 2;

  c_x := ST_X(_centroid) + _side / 2;
  c_y := ST_Y(_centroid) + _side / 2;
  c_z := ST_Z(_centroid) + _thick / 2;

  d_x := ST_X(_centroid) + _side / 2;
  d_y := ST_Y(_centroid) - _side / 2;
  d_z := ST_Z(_centroid) + _thick / 2;

  e_x := a_x;
  e_y := a_y;
  e_z := ST_Z(_centroid) - _thick / 2;

  f_x := b_x;
  f_y := b_y;
  f_z := ST_Z(_centroid) - _thick / 2;

  g_x := c_x;
  g_y := c_y;
  g_z := ST_Z(_centroid) - _thick / 2;

  h_x := d_x;
  h_y := d_y;
  h_z := ST_Z(_centroid) - _thick / 2;

  /*
      a, b, c, d, a
      f, e, h, g, f
      e, a, d, h, e
      g, c, b, f, g
      f, b, a, e, f
      h, d, c, g, h
  */
  geom_string := FORMAT('MULTIPOLYGONZ(
                         ((%s %s %s, %s %s %s, %s %s %s, %s %s %s, %s %s %s)),
                         ((%s %s %s, %s %s %s, %s %s %s, %s %s %s, %s %s %s)),
                         ((%s %s %s, %s %s %s, %s %s %s, %s %s %s, %s %s %s)),
                         ((%s %s %s, %s %s %s, %s %s %s, %s %s %s, %s %s %s)),
                         ((%s %s %s, %s %s %s, %s %s %s, %s %s %s, %s %s %s)),
                         ((%s %s %s, %s %s %s, %s %s %s, %s %s %s, %s %s %s)) )',
                        a_x, a_y, a_z, b_x, b_y, b_z, c_x, c_y, c_z, d_x, d_y, d_z, a_x, a_y, a_z,
                        f_x, f_y, f_z, e_x, e_y, e_z, h_x, h_y, h_z, g_x, g_y, g_z, f_x, f_y, f_z,
                        e_x, e_y, e_z, a_x, a_y, a_z, d_x, d_y, d_z, h_x, h_y, h_z, e_x, e_y, e_z,
                        g_x, g_y, g_z, c_x, c_y, c_z, b_x, b_y, b_z, f_x, f_y, f_z, g_x, g_y, g_z,
                        f_x, f_y, f_z, b_x, b_y, b_z, a_x, a_y, a_z, e_x, e_y, e_z, f_x, f_y, f_z,
                        h_x, h_y, h_z, d_x, d_y, d_z, c_x, c_y, c_z, g_x, g_y, g_z, h_x, h_y, h_z);

  RETURN ST_GeomFromText(geom_string, 32632);
END;
$$ language plpgsql volatile
;

create function ctrlcraft.probe_new_data(
  _date varchar,
  _serial_number_gps varchar,
  _serial_number_gamma varchar,
  _x double precision,
  _y double precision,
  _z double precision,
  _radiometry float,
  _gammaup float,
  _gammadown float,
  _teneurup float,
  _teneurdown float,
  _inclination float,
  _status int,
  _coeff_a_up float,
  _coeff_a_low float,
  _coeff_b_up float,
  _coeff_b_low float,
  _coeff_n_up float,
  _coeff_n_low float)
RETURNS setof void
as
$$
DECLARE
  geom geometry;
  t timestamp;
BEGIN
  geom := ST_SetSrid( ST_MakePoint( _x, _y ), 32632 );
  t := to_timestamp(_date, 'DD/MM/YYYY HH24:MI:SS') at time zone 'Etc/UTC';
  insert into ctrlcraft.probe(radiometry, time, geom, gammaup, gammadown, teneurup, teneurdown, alt, inclination, serial_number_gps, serial_number_gamma, status, coeff_a_up, coeff_a_low, coeff_b_up, coeff_b_low, coeff_n_up, coeff_n_low)
  values( _radiometry, t, geom, _gammaup, _gammadown, _teneurup, _teneurdown, _z, _inclination, _serial_number_gps, _serial_number_gamma, _status, _coeff_a_up, _coeff_a_low, _coeff_b_up, _coeff_b_low, _coeff_n_up, _coeff_n_low );
END;
$$ language plpgsql volatile
;

create function ctrlcraft.probe_to_csv(
  _filename varchar
)
RETURNS setof void
as
$$
DECLARE
BEGIN
  execute 'copy (select radiometry, time, st_astext(geom) from ctrlcraft.probe) to ''' || _filename || ''' with csv;';
END;
$$ language plpgsql volatile
;

create function ctrlcraft.trucks_loads_status_to_csv(
  _filename varchar
)
RETURNS setof void
as
$$
DECLARE
BEGIN
  execute 'copy (select * from ctrlcraft.truck_loads_status) to ''' || _filename || ''' with csv;';
END;
$$ language plpgsql volatile
;
