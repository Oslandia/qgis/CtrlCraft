# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Main CtrlCraft class for orchestration
"""

import platform
from functools import partial
from pathlib import Path

from qgis.core import (
    Qgis,
    QgsFeature,
    QgsGeometry,
    QgsPointXY,
    QgsProject,
    QgsSettings,
)
from qgis.PyQt.QtCore import QCoreApplication, Qt, QTranslator
from qgis.PyQt.QtGui import QIcon, QPixmap
from qgis.PyQt.QtWidgets import (
    QAction,
    QComboBox,
    QDialog,
    QDockWidget,
    QHBoxLayout,
    QLabel,
    QMenu,
    QMessageBox,
    QProgressBar,
    QPushButton,
    QSizePolicy,
    QToolBar,
    qApp,
)
from qgis.utils import OverrideCursor, active_plugins

from CtrlCraft import __about__
from CtrlCraft.data import CtrlCraftData
from CtrlCraft.lithology import CtrlCraftLithologyUpdateMapTool
from CtrlCraft.logger import CtrlCraftLogger
from CtrlCraft.project import CtrlCraftProject
from CtrlCraft.rtkube.constantes import DURATION
from CtrlCraft.rtkube.rtkube import RTKube
from CtrlCraft.rtkube.rtkube_serial import RtkubeSingleMeas
from CtrlCraft.truck import CtrlCraftTruck, CtrlCraftTruckLoadMapTool
from CtrlCraft.utils import misc
from CtrlCraft.widgets.affect_radiometry_slab import CtrlCraftAffectRadiometrySlab
from CtrlCraft.widgets.database_export import CtrlCraftDatabaseExport
from CtrlCraft.widgets.database_import import CtrlCraftDatabaseImport
from CtrlCraft.widgets.export_atlas import ExportAtlas
from CtrlCraft.widgets.level_filter import CtrlCraftLevelFilter
from CtrlCraft.widgets.main_parameters_dock import CtrlCraftMainParametersDock
from CtrlCraft.widgets.office_co3_import import CtrlCraftOfficeCO3Import
from CtrlCraft.widgets.office_shipments_chart import CtrlCraftOfficeShipmentsChart
from CtrlCraft.widgets.office_shipments_table import CtrlCraftOfficeShipmentsTable
from CtrlCraft.widgets.office_slabs_import import CtrlCraftOfficeSlabsImport
from CtrlCraft.widgets.probe_radiometry_dock import CtrlCraftProbeRadiometryDock
from CtrlCraft.widgets.slab_threshold import CtrlCraftSlabThreshold
from CtrlCraft.widgets.trucks_loads_status_export import CtrlCraftTrucksLoadsStatusExport


class CtrlCraft:  # pylint: disable=too-many-instance-attributes
    """QGIS Plugin Implementation."""

    def __init__(self, iface):  # pylint: disable=too-many-statements
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """

        # Declare instance attributes
        self.actions = []
        self.toolbar = None
        self.toolbarOffice = None
        self.rtkubeActionExport = None
        self.depositCombo = None
        self.panelCombo = None
        self.blastCombo = None
        self.undoAction = None
        self.affectRadio = None
        self.addLandmarkAction = None
        self.removeLandmarkAction = None
        self.clearLandmarksAction = None
        self.lithologyMapTools = {}
        self.trucks = []
        self.menu = None
        self.actionReset = None
        self.actionClearDb = None
        self.actionOpen = None
        self.statusLabel = None
        self.statusBar = None
        self.name = "CtrlCraft"
        self.translator = None  # To keep translations from being garbage collected
        self.timer = None
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        self.data = None
        self.rtkube = None
        self.truck = None
        self.mainParamtersWidget = None
        self.radiometryWidget = None

        # initialize locale
        self.initLocale()

        # Init logger
        self.logger = CtrlCraftLogger(self.name, iface)

        version = self.tr("office")
        if CtrlCraft.isMobileVersion():
            version = self.tr("mobile")
        msg = self.tr("Initialize CtrlCraft plugin ") + "{}".format(version)
        self.logger.info(msg)

        # Custom projection
        self.projIniFile = str(Path(__file__).parent / "proj.ini")

        # Custom gamma correction
        self.gammaIniFile = str(Path(__file__).parent / "gamma.ini")

        # Custom gamma normalization
        self.gammaNIniFile = str(Path(__file__).parent / "probe_normalization.ini")

        # init QgsSettings parameters
        self.initQgsSettingsParameters()

        # RTKube low-level for landmark points
        self.rtkubeLandmark = RtkubeSingleMeas()
        self.rtkubeLandmark.sendSingleGNSS.connect(self.addLandmark)
        self.rtkubeLandmark.sendError.connect(
            lambda msg: self.logger.logBar(msg, level=Qgis.MessageLevel.Critical, duration=DURATION)
        )

        # Check which project is loaded to activate some plugin functionnalities accordingly
        QgsProject.instance().readProject.connect(self.projectLoaded)
        iface.newProjectCreated.connect(self.projectClosed)

        # Init database
        self.initData()

        # Create underlying project if necessary
        self.project = CtrlCraftProject()
        if not self.project.exists():
            self.project.create()

    def initLocale(self):
        """init locale if translation file found"""

        locale = QgsSettings().value("/locale/userLocale")
        if locale is not None:
            locale = locale[0:2]
        localePath = Path(__file__).parent / "i18n" / f"CtrlCraft_{locale}.qm"
        if localePath.exists():
            self.translator = QTranslator()
            self.translator.load(str(localePath))
            QCoreApplication.installTranslator(self.translator)
        self.tr = misc.tr

    def initData(self):
        """Init data and try to connect to the underlying database"""

        self.data = CtrlCraftData()
        self.data.open()
        if not self.data.isValid():
            self.data.create()

    @property
    def currentDeposit(self):
        return self.depositCombo.itemData(self.depositCombo.currentIndex())

    @property
    def currentPanel(self):
        return self.panelCombo.itemData(self.panelCombo.currentIndex())

    @property
    def currentBlast(self):
        return self.blastCombo.itemData(self.blastCombo.currentIndex())

    def refresh(self):
        """Refresh the slabs layer"""
        layer = self.project.slabsLayer()
        if layer:
            layer.triggerRepaint()
            self.canvas.refresh()

    def updateUndo(self):
        """The undo button is enabled if at least one truck has a load"""
        self.undoAction.setEnabled(any(not truck.isEmpty() for truck in self.trucks))

    def addAction(  # pylint: disable=too-many-arguments, too-many-locals
        self,
        iconPath,
        text,
        callback=None,
        params=None,
        enabledFlag=True,
        addToMenu=False,
        addToToolbar=True,
        statusTip=None,
        whatsThis=None,
        parent=None,
        checkable=False,
        toolbar=None,
    ):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.

        :param callback: Function to be called when the action is triggered.

        :param enabled_flag: A flag indicating if the action should be enabled by default. Defaults to True.

        :param add_to_menu: Flag indicating whether the action should also be added to the menu. Defaults to True.

        :param add_to_toolbar: Flag indicating whether the action should also be added to the toolbar. Defaults to True.

        :param status_tip: Optional text to show in a popup when mouse pointer hovers over the action.

        :param parent: Parent widget for the new action. Defaults None.

        :param whats_this: Optional text to show in the status bar when the mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also added to self.actions list.
        """

        # Create the dialog (after translation) and keep reference
        pix = QPixmap(str(Path(__file__).parent / "img" / iconPath))

        icon = QIcon(pix)
        action = QAction(icon, text, parent)

        if callback:
            if params:
                action.triggered.connect(partial(callback, params))
            else:
                action.triggered.connect(callback)

        action.setEnabled(enabledFlag)
        action.setCheckable(checkable)

        action.setData(params)

        if statusTip is not None:
            action.setStatusTip(statusTip)

        if whatsThis is not None:
            action.setWhatsThis(whatsThis)

        if addToToolbar:
            if not toolbar:
                toolbar = self.toolbar
            toolbar.addAction(action)

        if addToMenu:
            self.iface.addPluginToMenu(self.name, action)

        self.actions.append(action)

        return action

    def addMenuEntry(self, name, callback, enabled=True, helpStr=""):
        act = self.menu.addAction(name)
        if callback is not None:
            act.triggered.connect(callback)
            act.setEnabled(enabled)
            act.setToolTip(helpStr)
        else:
            act.setEnabled(False)
            act.setToolTip("NOT INMPLEMENTED " + helpStr)
        return act

    def initGui(self):  # called by QGIS plugin initialization
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        self.initToolBarMobile()
        self.initMenu()
        self.iface.layerTreeView().parent().findChild(QToolBar).setVisible(False)

        if not self.isMobileVersion():
            self.initToolBarOffice()
        else:
            iconSize = QgsSettings().value("/qgis/iconSize")
            styleSheet = f"QTabBar::tab {{ height: {iconSize}px; }}"
            self.iface.mainWindow().setStyleSheet(styleSheet)

        # Not ready by default. We're waiting to load a valid project
        self.setReady(False)

    def initToolBarMobile(self):
        if self.toolbar:
            self.toolbar.close()
            self.toolbar = None

        self.toolbar = self.iface.addToolBar(self.name)
        self.toolbar.setObjectName(self.name)

        # Deposit selector
        self.depositCombo = QComboBox()
        self.depositCombo.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.depositCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.depositCombo.setToolTip(self.tr("Deposits"))
        self.depositCombo.currentIndexChanged.connect(self.depositSelected)
        self.toolbar.addWidget(self.depositCombo)

        # Panels selector
        self.panelCombo = QComboBox()
        self.panelCombo.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.panelCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.panelCombo.setToolTip(self.tr("Panels"))
        self.panelCombo.currentIndexChanged.connect(self.panelSelected)
        self.toolbar.addWidget(self.panelCombo)

        # Blast selector
        self.blastCombo = QComboBox()
        self.blastCombo.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.blastCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.blastCombo.setToolTip(self.tr("Blasts"))
        self.blastCombo.currentIndexChanged.connect(self.blastSelected)
        self.toolbar.addWidget(self.blastCombo)

        # Add some actions
        if CtrlCraft.isMobileVersion():
            self.toolbar.addAction(self.iface.actionPan())
            self.toolbar.addAction(self.iface.actionZoomIn())
            self.toolbar.addAction(self.iface.actionZoomOut())
            self.toolbar.addAction(self.iface.actionZoomFullExtent())
        self.undoAction = self.addAction("iconUndoSlab.png", text=self.tr("Undo"), callback=self.unloadLastSlab)

        # Radiometry dock widget
        if self.radiometryWidget is None:
            self.radiometryWidget = CtrlCraftProbeRadiometryDock(self.iface)
        if self.mainParamtersWidget is None:
            self.mainParamtersWidget = CtrlCraftMainParametersDock(self.iface, self.data)
            self.mainParamtersWidget.configModified.connect(self.resetToolBarAndProject)

        if not self.rtkube:
            self.rtkube = RTKube(
                self.iface,
                self.data,
                self.radiometryWidget,
                self.projIniFile,
                self.gammaIniFile,
                self.gammaNIniFile,
            )
            self.rtkube.init()

        self.affectRadio = self.addAction(
            "iconAffectRadioSlab.png", text=self.tr("Affect the grade on a slab"), callback=self.affectRadiometrySlab
        )

        # Landmarking
        self.addLandmarkAction = self.addAction(
            "iconAddLandmark.png",
            text=self.tr("Add a landmark point"),
            callback=self.rtkubeLandmark.getSingleGNSS,
        )
        self.removeLandmarkAction = self.addAction(
            "iconRemoveLandmark.png",
            text=self.tr("Remove last landmark point"),
            callback=self.removeLandmark,
        )
        self.clearLandmarksAction = self.addAction(
            "iconClearLandmarks.png",
            text=self.tr("Clear all landmark points"),
            callback=self.clearLandmarks,
        )

        # Add lithology buttons
        self.buildLithoButtons()

        # Trucks objects
        self.populateTruckList()

        # Build Select Truck Action
        truckSelectAction = self.addAction(
            "icon_select_truck.png", text=self.tr("Select a truck"), callback=self.selectCurrentTruck
        )
        truckSelectAction.setCheckable(True)
        # Select the first truck by default
        if len(self.trucks) > 0:
            self.truckSelected(self.trucks[0])

    def initMenu(self):
        self.menu = QMenu(self.name)

        self.actionOpen = self.addMenuEntry(self.tr("Open Project"), self.openProject)
        self.actionReset = self.addMenuEntry(self.tr("Reset Project"), self.resetProject)
        self.actionClearDb = self.addMenuEntry(self.tr("Clear Database"), self.clearDatabase)
        self.addMenuEntry(self.tr("Define slab threshold symbology"), self.defineRadiometryThreshold)

        self.menu.addSeparator()

        self.addMenuEntry(self.tr("Import Slabs"), self.importSlabs)
        self.addMenuEntry(self.tr("Import CO3"), self.importCO3)

        self.menu.addSeparator()
        self.addMenuEntry(self.tr("Filter levels"), self.filterLevels)
        self.addMenuEntry(self.tr("Remove levels"), self.removeLevel)
        self.addMenuEntry(self.tr("Revert removed levels"), self.revertRemovedLevel)

        self.menu.addSeparator()
        self.addMenuEntry(self.tr("Import Database"), self.importDatabase)
        self.addMenuEntry(self.tr("Export Database"), self.exportDatabase)

        self.menu.addSeparator()

        self.addMenuEntry(self.tr("Export Trucks Loads Status"), self.exportTrucksLoadsStatus)

        self.rtkubeActionExport = self.addMenuEntry(self.tr("Export K2 data to CSV"), self.rtkube.export)

        self.iface.mainWindow().menuBar().addMenu(self.menu)

        self.menu.addSeparator()
        self.addMenuEntry(self.tr("Export Atlas PDF"), self.exportAtlasPDF)

        self.menu.addSeparator()
        self.addMenuEntry(self.tr("Exit QGIS"), self.iface.actionExit().trigger)

    def initToolBarOffice(self):
        if self.toolbarOffice is None:
            self.toolbarOffice = self.iface.addToolBar(f"{self.name} Office")
            self.toolbarOffice.setObjectName(f"{self.name} Office")
        self.addAction(
            "iconOffice.png",
            text=self.tr("CtrCraft Office Shipments"),
            callback=self.showOfficeShipmentsTable,
            toolbar=self.toolbarOffice,
        )
        self.addAction(
            "iconOfficeChart.png",
            text=self.tr("CtrCraft Office Shipments Graph"),
            callback=self.showOfficeShipmentsChart,
            toolbar=self.toolbarOffice,
        )

    def initQgsSettingsParameters(self):
        settings = QgsSettings()
        for i in range(1, 5):
            if settings.value("/CtrlCraft/Truck1/id") is None:
                settings.setValue("/CtrlCraft/Truck{}/id".format(str(i)), str(i).zfill(3))
            if settings.value("/CtrlCraft/Truck1/sc") is None:
                settings.setValue("/CtrlCraft/Truck{}/sc".format(str(i)), 8)
        if settings.value("/GeoCube/GPSmode") is None:
            settings.setValue("/GeoCube/GPSmode", "Fixed")
        if settings.value("/GeoCube/angle") is not None:
            settings.setValue("/GeoCube/angle", 90.0)

    def setReady(self, ready):
        for action in self.actions:
            action.setEnabled(ready)

        self.depositCombo.setEnabled(ready)
        self.panelCombo.setEnabled(ready)
        self.blastCombo.setEnabled(ready)
        self.depositSelected()
        self.blastSelected()

        if self.rtkube.ready:
            self.rtkubeActionExport.setEnabled(ready)

        for truck in list(self.trucks):
            truck.updateIconLoad()
            truck.updateIconUnload()

        if ready:
            self.fillDepositCombo()
            self.fillPanelCombo()
            self.fillBlastCombo()

            self.rtkube.layer = self.project.probeLayer()
            self.rtkube.topoLayer = self.project.topoLayer()

            if self.actionOpen:
                self.actionOpen.setEnabled(False)
                self.actionReset.setEnabled(True)
                self.actionClearDb.setEnabled(True)

            # Delete the tile scale dock widget because we can't remove it in the customization file
            # see https://github.com/qgis/QGIS/issues/58617
            if (dock := self.iface.mainWindow().findChild(QDockWidget, "theTileScaleDock")) is not None:
                dock.deleteLater()

            QgsProject.instance().setTitle("CtrlCraft version " + __about__.__version__)

        else:
            self.rtkube.layer = None
            self.depositCombo.clear()
            self.panelCombo.clear()
            self.blastCombo.clear()

            if self.actionOpen:
                self.actionOpen.setEnabled(True)
                self.actionReset.setEnabled(False)
                self.actionClearDb.setEnabled(False)

    def unloadLastSlab(self):
        if not self.data.isValid():
            return

        tid = self.data.truckUnloadLastSlab()
        for truck in self.trucks:
            if truck.id == tid:
                truck.updateIconLoad()
                truck.updateIconUnload()
                truck.updateIconState()
                self.refresh()

    def importSlabs(self):
        """
        Imports Slabs from a CSV file with SMU definition
        """

        dlg = CtrlCraftOfficeSlabsImport(self.data, self.projIniFile, self.currentDeposit)
        dlg.show()
        dlg.exec()

        if dlg.updated:
            layer = self.project.slabsLayer()
            if layer is not None:
                layer.dataProvider().reloadData()
                self.iface.mapCanvas().refreshAllLayers()
                self.iface.mapCanvas().setExtent(layer.extent())

            self.fillDepositCombo()
            self.fillPanelCombo()
            self.fillBlastCombo()

    def importCO3(self):
        """
        Import CO3 data.
        """

        dlg = CtrlCraftOfficeCO3Import(self.data, self.projIniFile, self.currentDeposit)
        dlg.show()
        dlg.exec()

        if dlg.updated:
            layer = self.project.co3Layer()
            layer.dataProvider().reloadData()
            self.iface.mapCanvas().refreshAllLayers()
            self.iface.mapCanvas().setExtent(layer.extent())

    def resetProject(self):
        """
        Resets the current project to its initial stage.
        """

        self.project.reset()
        if self.project.exists():
            self.project.load()

    def openProject(self):
        """
        Opens the ctrlcraft project if not yet loaded
        """

        if not self.project.isLoaded():
            self.project.load()
            self.updateUndo()

    def clearDatabase(self):
        title = self.tr("Clear Database")
        msg = self.tr(
            "Are you sure you want to delete the database? Underlying data "
            "will be lost and K2 connection will be closed."
        )
        reply = QMessageBox.question(None, title, msg, QMessageBox.Yes | QMessageBox.No)

        if reply == QMessageBox.Yes:
            self.rtkube.layer = None
            self.rtkube.save = False
            self.project.close()

            self.updateStatus(self.tr("Clearing database..."))
            self.data.clear()
            self.updateStatus()

            self.project.load()
            self.iface.mapCanvas().refreshAllLayers()

            self.rtkube.layer = self.project.probeLayer()
            self.rtkube.topoLayer = self.project.topoLayer()
            self.rtkube.save = True

    def exportDatabase(self):
        dlg = CtrlCraftDatabaseExport(self.data)
        dlg.show()
        dlg.exec()

    def importDatabase(self):
        dlg = CtrlCraftDatabaseImport(self.data, self.rtkube, self.project, self.currentDeposit)
        dlg.show()
        dlg.exec()

        if dlg.updated:
            self.iface.mapCanvas().refreshAllLayers()

    def exportAtlasPDF(self):
        dlg = ExportAtlas()
        dlg.show()
        dlg.exec()

    def exportTrucksLoadsStatus(self):
        CtrlCraftTrucksLoadsStatusExport(self.data).export()

    def affectRadiometrySlab(self):
        if self.rtkube is None or self.rtkube.widget is None:
            return

        if self.rtkube.widget.isConnected:
            CtrlCraftAffectRadiometrySlab(self.rtkube.widget, self.canvas, self.data).exec()
        else:
            QMessageBox.warning(None, self.tr("Affect the grade on a slab"), self.tr("K2 must be connected"))

    def projectClosed(self):
        if self.name in active_plugins or not CtrlCraft.isOnWindows():
            self.setReady(False)

    def projectLoaded(self):
        """
        Slot called when a new project has been loaded from desktop.
        """

        if CtrlCraft.isOnWindows() and self.name not in active_plugins:
            return

        if not self.project.isLoaded():
            self.setReady(False)
            self.data.close()
            return

        self.data.open()
        if not self.data.isValid():
            return

        self.setReady(True)

    def addLandmark(self, gnssData):
        if self.project.landmarkLayer is None:
            self.project.resetLandmarkLayer()
        lon = float(gnssData[2])
        lat = float(gnssData[3])
        self.project.landmarkLayer.startEditing()
        feature = QgsFeature()
        feature.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(lon, lat)))
        self.project.landmarkLayer.addFeature(feature)
        self.project.landmarkLayer.commitChanges()
        self.logger.logBar(self.tr("Landmark point added"))

    def removeLandmark(self):
        if self.project.landmarkLayer is None:
            return
        features = list(self.project.landmarkLayer.getFeatures())
        if len(features) > 0:
            self.project.landmarkLayer.startEditing()
            self.project.landmarkLayer.deleteFeature(features[-1].id())
            self.project.landmarkLayer.commitChanges()
            self.logger.logBar(self.tr("Last landmark point removed"))
        else:
            self.logger.logBar(self.tr("No landmark point to remove"))

    def clearLandmarks(self):
        if self.project.landmarkLayer is None:
            return
        features = list(self.project.landmarkLayer.getFeatures())
        if len(features) > 0:
            self.project.landmarkLayer.startEditing()
            self.project.landmarkLayer.deleteFeatures([f.id() for f in features])
            self.project.landmarkLayer.commitChanges()
            self.logger.logBar(self.tr("Landmark points cleared"))
        else:
            self.logger.logBar(self.tr("No landmark point to clear"))

    def filterLevels(self):
        layer = self.project.slabsLayer()
        if not layer:
            return

        originSubstring = layer.subsetString()
        idx = originSubstring.find(' and "centroid_z"')
        actualLevel = -9999
        if idx == -1:
            originSubstring += " "
            idx = len(originSubstring)
        else:
            lvlIdx = originSubstring.find(" <=")
            actualLevel = float(originSubstring[lvlIdx + 4 :])

        dlg = CtrlCraftLevelFilter(
            self.data,
            actualLevel,
            self.currentDeposit,
            self.currentPanel,
            self.currentBlast,
        )
        dlg.show()

        res = dlg.exec()
        if res:
            maxZ = dlg.mLevels.currentData()
            layer.setSubsetString(originSubstring[:idx] + ' and "centroid_z" <= ' + str(maxZ))

    def removeLevel(self):
        """
        Remove an entire level
        """
        layer = self.project.slabsLayer()
        if not layer:
            return

        dlg = CtrlCraftLevelFilter(self.data, 0, self.currentDeposit, self.currentPanel, self.currentBlast)
        dlg.setWindowTitle(self.tr("Remove levels"))
        dlg.label.setText(self.tr("Remove levels:"))
        dlg.show()

        res = dlg.exec()
        if res:
            self.data.removeLevel(
                dlg.mLevels.currentData(),
                self.currentDeposit,
                self.currentPanel,
                self.currentBlast,
            )
            self.iface.mapCanvas().refreshAllLayers()

    def revertRemovedLevel(self):
        """
        Revert a removed level
        """
        layer = self.project.slabsLayer()
        if not layer:
            return

        dlg = CtrlCraftLevelFilter(self.data, -1, self.currentDeposit, self.currentPanel, self.currentBlast)
        dlg.setWindowTitle(self.tr("Revert removed levels"))
        dlg.label.setText(self.tr("Revert removed levels:"))
        dlg.show()

        res = dlg.exec()
        if res:
            self.data.revertRemovedLevel(
                dlg.mLevels.currentData(),
                self.currentDeposit,
                self.currentPanel,
                self.currentBlast,
            )
            self.iface.mapCanvas().refreshAllLayers()

        dlg = CtrlCraftLevelFilter(self.data, -1, self.currentDeposit, self.currentPanel, self.currentBlast)

    def fillDepositCombo(self):
        """Fill the deposit combo"""
        self.depositCombo.clear()
        for deposit in self.data.deposits():
            self.depositCombo.addItem(deposit, deposit)

    def fillPanelCombo(self):
        """Fill the panel combo"""
        self.panelCombo.clear()
        for panel in self.data.panels(self.currentDeposit):
            self.panelCombo.addItem(panel, panel)

    def fillBlastCombo(self):
        """Fill the blast combo"""
        self.blastCombo.clear()
        for blast in self.data.blasts(self.currentPanel):
            self.blastCombo.addItem(blast, blast)

    def populateTruckList(self):
        """Create the list of all truck objects"""
        for truckid in self.data.trucksNoOrder():
            # Create a new truck
            truck = CtrlCraftTruck(truckid, self.data, self.canvas)
            truck.updated.connect(self.refresh)
            self.trucks.append(truck)

    def selectCurrentTruck(self):
        """
        Open Dialog to select a Truck
        """
        dlg = QDialog()
        vLayout = QHBoxLayout(dlg)
        for truck in self.trucks:
            truckButton = QPushButton(truck.id)
            truckButton.clicked.connect(lambda _, x=truck: self.truckSelected(x))
            truckButton.clicked.connect(dlg.close)
            vLayout.addWidget(truckButton)

        dlg.exec()

    def truckSelected(self, truck):
        """Change truck, triggered using truck combobox"""
        if self.truck:
            self.truck.updated.disconnect(self.updateUndo)
            self.toolbar.removeAction(self.truck.actionLoad)
            self.toolbar.removeAction(self.truck.actionUnload)
            self.toolbar.removeAction(self.truck.actionState)

        truck.actionLoad = self.addAction(
            truck.loadIcon(),
            text=self.tr("Load the truck ") + "{}".format(truck.id),
            callback=truck.activateMapTool,
        )
        truck.actionLoad.setCheckable(True)

        truck.actionUnload = self.addAction(
            truck.unloadIcon(),
            text=self.tr("Unload the truck ") + "{}".format(truck.id),
            callback=truck.removeLoads,
            params=[self.currentDeposit, self.currentPanel, self.currentBlast],
        )

        truck.actionState = self.addAction(
            truck.stateIcon(),
            text=self.tr("Probe grade for truck ") + "{}".format(truck.id),
            callback=None,
        )

        # Store truck
        self.truck = truck
        self.truck.updated.connect(self.updateUndo)

        # Re-activate load maptool
        if isinstance(self.iface.mapCanvas().mapTool(), CtrlCraftTruckLoadMapTool):
            self.truck.activateMapTool()

    def buildLithoButtons(self):
        """Build the litho list and buttons to apply to slab"""

        for name in self.data.lithologies():
            mapTool = CtrlCraftLithologyUpdateMapTool(name, self.canvas, self.data)
            action = self.addAction(
                f"iconLithoSlab_{misc.textToId(name)}.png",
                text=self.tr("Set lithology ") + "{}".format(name),
                callback=partial(self.canvas.setMapTool, mapTool),
            )
            action.setCheckable(True)
            mapTool.setAction(action)
            mapTool.lithologyUpdated.connect(self.refresh)
            self.lithologyMapTools[name] = mapTool  # To keep the tools from the garbage collector

    def depositSelected(self):
        self.fillPanelCombo()
        self.fillBlastCombo()

        if self.currentDeposit:
            self.project.setDepositGroupName(self.currentDeposit)
            self.rtkube.setDeposit(self.currentDeposit)
            self.logger.info(f"Set deposit {self.currentDeposit}")

        else:
            self.project.setDepositGroupName("None")

    def resetToolBarAndProject(self):
        with OverrideCursor(Qt.CursorShape.WaitCursor):
            self.initToolBarMobile()
            self.resetProject()

    def panelSelected(self):
        """Update blast combo"""
        self.fillBlastCombo()

    def blastSelected(self):
        layer = self.project.slabsLayer()
        if layer:
            if self.currentBlast:
                layer.setName("{panel}-{blast}".format(panel=self.currentPanel, blast=self.currentBlast))
                self.filterLayer()
            else:
                layer.setName("None")
        levelsLayer = self.project.levelsLayer()
        if levelsLayer:
            if self.currentBlast:
                self.filterLevelByBlast()

    def filterLayer(self):
        """Filter the slabs layer according to current panel and blast"""
        layer = self.project.slabsLayer()

        if not layer:
            return

        layer.setSubsetString(
            "deposit='{deposit}' and panel='{panel}' and blast='{blast}'".format(
                panel=self.currentPanel,
                blast=self.currentBlast,
                deposit=self.currentDeposit,
            )
        )
        layer.triggerRepaint()
        extent = layer.extent()
        self.canvas.setExtent(extent)
        self.canvas.zoomOut()

    def filterLevelByBlast(self):
        layer = self.project.levelsLayer()

        if not layer:
            return

        layer.setSubsetString(
            "deposit='{deposit}' and panel='{panel}' and blast='{blast}'".format(
                panel=self.currentPanel,
                blast=self.currentBlast,
                deposit=self.currentDeposit,
            )
        )

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(self.name, action)
            self.iface.removeToolBarIcon(action)

        # remove the toolbar
        if self.toolbar:
            self.toolbar.setParent(None)
        if self.toolbarOffice:
            self.toolbarOffice.setParent(None)

        # remove the menu
        if self.menu:
            del self.menu

        if self.data:
            self.data.close()

        if self.timer:
            self.timer.stop()

        self.iface.layerTreeView().parent().findChild(QToolBar).setVisible(True)

    def showOfficeShipmentsTable(self):
        """Office management"""
        dlg = CtrlCraftOfficeShipmentsTable(self.data)
        dlg.init(self.currentDeposit, self.currentPanel, self.currentBlast)
        dlg.exec()

    def showOfficeShipmentsChart(self):
        """Office chart management"""
        maxSlab = 0
        for truck in self.trucks:
            maxSlab = max(maxSlab, truck.maxSlab)
        maxSlab = max(maxSlab, self.data.getMaxSlabsShipment(self.currentDeposit, self.currentPanel, self.currentBlast))
        dlg = CtrlCraftOfficeShipmentsChart(self.data, self.canvas, maxSlab)
        dlg.init(self.currentDeposit, self.currentPanel, self.currentBlast)
        dlg.exec()

    def updateStatus(self, msg=""):
        statusBar = self.iface.mainWindow().statusBar()

        if msg:
            self.statusLabel = QLabel()
            self.statusBar = QProgressBar()
            self.statusBar.setMinimum(0)
            self.statusBar.setMaximum(0)

            self.statusLabel.setText(msg)
            statusBar.insertWidget(0, self.statusLabel)
            statusBar.insertWidget(1, self.statusBar)
        else:
            statusBar.removeWidget(self.statusLabel)
            statusBar.removeWidget(self.statusBar)

        qApp.processEvents()

    def defineRadiometryThreshold(self):
        if not self.project.isLoaded():
            return

        if self.data.isPre310():
            QMessageBox.critical(
                None,
                self.tr("Old database"),
                self.tr("The database is from an old dump\nand is not compatible with CtrlCraft >= 3.1.0."),
            )
            return

        dlg = CtrlCraftSlabThreshold(self.project, self.data)
        dlg.accepted.connect(self.refresh)
        if dlg.exec() == CtrlCraftSlabThreshold.Accepted:
            self.project.load()

    @staticmethod
    def isMobileVersion():
        return QgsSettings().value("/CtrlCraft/mobile", "").lower() == "true"

    @staticmethod
    def isOnWindows():
        return platform.system() == "Windows"
