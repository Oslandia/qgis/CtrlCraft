# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Logging module
"""

import inspect
import logging
from logging.handlers import TimedRotatingFileHandler

from qgis.core import Qgis, QgsMessageLog

from CtrlCraft.settings import LOGFILE, LOGLEVEL
from CtrlCraft.utils.misc import Singleton


def CtrlCraftLogger(name="root", iface=None, levelStr=LOGLEVEL):
    """Just wraps getLogger function"""
    return CtrlCraftLoggers().getLogger(name, iface, levelStr)


class CtrlCraftLoggers(metaclass=Singleton):  # pylint: disable=too-few-public-methods
    """Singleton class to hold all named loggers"""

    loggers = {}

    def __init__(self):
        pass

    def getLogger(self, name="root", iface=None, levelStr=LOGLEVEL):
        if name not in self.loggers:
            self.loggers[name] = CtrlCraftNamedLogger(name, iface, levelStr)
        return self.loggers[name]


class CtrlCraftNamedLogger:
    """
    Logs messages to 2 loggers: file and qgis.
    The minimun log level is taken by default from CtrlCraft.settings.LOGLEVEL

    As Qgis has not as many log levels than logging:
    * DEBUG log level is mapped to Qgis.Info minus 1
    * ERROR log level is mapped to Qgis.Critical
    """

    def __init__(self, name="root", iface=None, levelStr=LOGLEVEL):
        self.name = name
        self.iface = iface

        self.minLevel = logging.getLevelName(levelStr.upper())

        self.fileLogger = logging.getLogger(name)
        self.fileLogger.setLevel(self.minLevel)

        fh = TimedRotatingFileHandler(LOGFILE, when="D", interval=1, backupCount=21)
        fh.setLevel(self.minLevel)
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        fh.setFormatter(formatter)

        # Don't propagate to parent logger because all loggers including root have a file handler
        if name != "root":
            self.fileLogger.propagate = False

        self.fileLogger.addHandler(fh)
        self.debug(f"Logfile is located here: {LOGFILE}")

    def qgisToLoggingLevel(self, level):
        """Converts Qgis log levels to logging"""
        if level == Qgis.Info - 1:
            return logging.DEBUG
        if level == Qgis.Info:
            return logging.INFO
        if level == Qgis.Warning:
            return logging.WARNING
        if level == Qgis.Critical:
            return logging.ERROR

        return logging.INFO

    def log(self, msg="", level=Qgis.Info, loggingLevel=None):
        """Logs a message at specified level to qgis and file loggers"""
        if self.minLevel <= self.qgisToLoggingLevel(level):
            stack = inspect.stack()
            className = "<module>"
            if "self" in stack[2][0].f_locals:
                className = stack[2][0].f_locals["self"].__class__.__name__

            methodName = stack[2][3]

            msg = "{}::{} - {}".format(className, methodName, msg)
            QgsMessageLog.logMessage(msg, self.name, level)

            # by default use the log level matching Qgis
            if loggingLevel is None:
                self.fileLogger.log(self.qgisToLoggingLevel(level), msg)
            else:
                self.fileLogger.log(loggingLevel, msg)

    def logBar(self, msg, level=Qgis.Info, duration=0):
        """Logs a message at specified level to qgis log bar and file logger"""
        if self.iface and self.minLevel <= self.qgisToLoggingLevel(level):
            if duration:
                self.iface.messageBar().pushMessage(self.name, msg, level=level, duration=duration)
            else:
                self.iface.messageBar().pushMessage(self.name, msg, level=level)

            self.log(msg, level)

    def debug(self, msg, *args, **kwargs):
        """Logs a debug message to file and qgis loggers"""
        self.log(msg.format(*args, *kwargs), Qgis.Info - 1, logging.DEBUG)

    def info(self, msg, *args, **kwargs):
        """Logs an info message to file and qgis loggers"""
        self.log(msg.format(*args, *kwargs), Qgis.Info, logging.INFO)

    def warning(self, msg, *args, **kwargs):
        """Logs a warning message to file and qgis loggers"""
        self.log(msg.format(*args, *kwargs), Qgis.Warning, logging.WARNING)

    def error(self, msg, *args, **kwargs):
        """Logs an error message to file and qgis loggers"""
        self.log(msg.format(*args, *kwargs), Qgis.Critical, logging.ERROR)

    def critical(self, msg, *args, **kwargs):
        """Logs a critical message to file and qgis loggers"""
        self.log(msg.format(*args, *kwargs), Qgis.Critical, logging.CRITICAL)
