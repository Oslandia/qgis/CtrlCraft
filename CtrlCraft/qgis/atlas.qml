<qgis version="3.34.10-Prizren" simplifyDrawingHints="1" simplifyMaxScale="1" styleCategories="AllStyleCategories" maxScale="-4.6566128730773926e-10" readOnly="0" simplifyAlgorithm="0" labelsEnabled="1" simplifyLocal="1" symbologyReferenceScale="-1" hasScaleBasedVisibilityFlag="0" minScale="100000000" simplifyDrawingTol="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="" durationUnit="min" accumulate="0" limitMode="0" endExpression="" fixedDuration="0" startExpression="" endField="" enabled="0" mode="0" durationField="radiometry">
    <fixedRange>
      <start />
      <end />
    </fixedRange>
  </temporal>
  <elevation binding="Centroid" respectLayerSymbol="1" zscale="1" extrusionEnabled="0" clamping="Terrain" zoffset="0" showMarkerSymbolInSurfacePlots="0" symbology="Line" extrusion="0" type="IndividualFeatures">
    <data-defined-properties>
      <Option type="Map">
        <Option name="name" value="" type="QString" />
        <Option name="properties" />
        <Option name="type" value="collection" type="QString" />
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" id="{7a90af83-446f-43ce-b5be-8376674920f4}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="align_dash_pattern" value="0" type="QString" />
            <Option name="capstyle" value="square" type="QString" />
            <Option name="customdash" value="5;2" type="QString" />
            <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="customdash_unit" value="MM" type="QString" />
            <Option name="dash_pattern_offset" value="0" type="QString" />
            <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
            <Option name="draw_inside_polygon" value="0" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="line_color" value="114,155,111,255" type="QString" />
            <Option name="line_style" value="solid" type="QString" />
            <Option name="line_width" value="0.6" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="ring_filter" value="0" type="QString" />
            <Option name="trim_distance_end" value="0" type="QString" />
            <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="trim_distance_end_unit" value="MM" type="QString" />
            <Option name="trim_distance_start" value="0" type="QString" />
            <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="trim_distance_start_unit" value="MM" type="QString" />
            <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
            <Option name="use_custom_dash" value="0" type="QString" />
            <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{b0338771-5890-413f-88fe-cad14915b4e4}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="114,155,111,255" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="81,111,79,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.2" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{a57de10a-c634-4b7f-b38f-a7c5c85d5480}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="114,155,111,255" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="diamond" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="81,111,79,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.2" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="diameter" type="QString" />
            <Option name="size" value="3" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 symbollevels="0" referencescale="-1" forceraster="0" enableorderby="0" type="RuleRenderer">
    <rules key="{1050e838-30f5-45f1-aa71-74b925095554}">
      <rule key="{57ab379e-5588-48ef-a54f-f9cc8215962a}" symbol="0" filter="to_string(round(&quot;centroid_z&quot;, 10)) = @atlas_pagename&#10;and &quot;radiometry_class&quot; = 0" label="-999.0" checkstate="1">
        <rule key="{77f0cb08-ff67-4dc1-9ef5-9d27fb2fe5dd}" label="&quot;lithology&quot; = 'Argile'" symbol="1" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{5560e510-291e-4306-9ee7-9e07fb51951a}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="2" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{5d3fc2e2-7302-4fad-832a-c2e1e1c18a8d}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="3" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{6d1039ca-ba19-4ac8-b959-3a612507891d}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="4" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{8ebe86e5-403f-4b2a-9596-3fb6a9d114cf}" label="&quot;lithology&quot; = 'ND'" symbol="5" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{c793b67f-ad19-4c6b-a604-ae1f1bafbb54}" symbol="6" filter="to_string(round(&quot;centroid_z&quot;, 10)) = @atlas_pagename&#10;and &quot;radiometry_class&quot; = 1" label="0.0 - 0.7" checkstate="1">
        <rule key="{5e4db4d1-9c6a-4307-a550-51b3fb84f4a4}" label="&quot;lithology&quot; = 'Argile'" symbol="7" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{874a8c16-1e24-4260-a8ab-f85cd7a763b4}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="8" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{3f0910f3-e7a7-4218-8503-8606750319f6}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="9" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{dee133e5-b7f8-4947-9a93-8087adbd7984}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="10" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{d7d32f01-8c5b-4e6d-82f4-9b155d8766dd}" label="&quot;lithology&quot; = 'ND'" symbol="11" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{d7f97b10-07aa-4f39-9ddc-e2020bfd0e83}" symbol="12" filter="to_string(round(&quot;centroid_z&quot;, 10)) = @atlas_pagename&#10;and &quot;radiometry_class&quot; = 2" label="0.7 - 1.3" checkstate="1">
        <rule key="{96df7a9c-352f-4d69-9a48-bc11552be8da}" label="&quot;lithology&quot; = 'Argile'" symbol="13" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{c0f02776-3e51-4d45-a56c-5d636fef94b8}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="14" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{a2fcf81b-4a10-4a22-897f-7e92c96c9a92}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="15" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{48691f1d-2d59-450d-91e6-192bf97b3d33}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="16" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{d89d967a-6b89-4904-9bbd-12a7216df68a}" label="&quot;lithology&quot; = 'ND'" symbol="17" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{7c827356-a09e-432d-ad2e-a18bb5893aaf}" symbol="18" filter="to_string(round(&quot;centroid_z&quot;, 10)) = @atlas_pagename&#10;and &quot;radiometry_class&quot; = 3" label="1.3 - 1.4" checkstate="1">
        <rule key="{7fb71bfc-7ffb-439e-acac-7a5d50ee691c}" label="&quot;lithology&quot; = 'Argile'" symbol="19" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{49ced5e3-7e73-439b-8308-bc2bdb3093d3}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="20" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{57c5a210-48ed-40e8-bb9d-e32717d72195}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="21" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{65e029c2-38cc-4817-89e2-c2c99a38c923}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="22" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{515d30ab-529d-4002-8caf-b9317b6ac936}" label="&quot;lithology&quot; = 'ND'" symbol="23" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{c1f48dc9-91cb-40bf-ac96-6b5eddf71793}" symbol="24" filter="to_string(round(&quot;centroid_z&quot;, 10)) = @atlas_pagename&#10;and &quot;radiometry_class&quot; = 4" label="1.4 - 1.8" checkstate="1">
        <rule key="{9cc6d36f-e01e-4075-bb2a-31c50463553f}" label="&quot;lithology&quot; = 'Argile'" symbol="25" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{c3265fec-1bcf-4d50-8b84-49e35b71cb9e}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="26" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{e89657b8-d153-44a2-b735-fe110bdf6784}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="27" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{c02bd718-ac2e-4611-9ef9-7c0889393306}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="28" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{ace5e880-fe1f-486b-9b95-bde83ab6e1b4}" label="&quot;lithology&quot; = 'ND'" symbol="29" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{fa511a8b-ed1f-4f25-9e42-bfd53c512dbb}" symbol="30" filter="to_string(round(&quot;centroid_z&quot;, 10)) = @atlas_pagename&#10;and &quot;radiometry_class&quot; = 5" label="1.8 - 2.5" checkstate="1">
        <rule key="{22381f3e-cbb6-46dd-8af3-2c314324b239}" label="&quot;lithology&quot; = 'Argile'" symbol="31" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{9e7a85b8-e03a-43b4-b231-bddee0ee4c07}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="32" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{1a5a747b-68dc-4371-bda5-3b5489645f61}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="33" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{f15cdd81-3ef5-48a9-8819-113ab381f641}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="34" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{ddfe4c84-2d91-4380-b225-4ca7c54f2ad0}" label="&quot;lithology&quot; = 'ND'" symbol="35" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{af35a396-0a46-4043-8dac-672a75d65386}" symbol="36" filter="to_string(round(&quot;centroid_z&quot;, 10)) = @atlas_pagename&#10;and &quot;radiometry_class&quot; = 6" label="+2.5" checkstate="1">
        <rule key="{83eb5e6b-c1f1-44d7-87f3-2ba05b75a399}" label="&quot;lithology&quot; = 'Argile'" symbol="37" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{014c107d-b049-42a8-87f3-15c7feab8778}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="38" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{f700b890-4104-4811-b68d-f5ff99aec3ea}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="39" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{007c1b8b-cfb3-4cc9-99ad-9359e1449eab}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="40" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{b050c8e8-f69f-473f-9ca6-58a548cea36d}" label="&quot;lithology&quot; = 'ND'" symbol="41" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
    </rules>
    <symbols>
      <symbol clip_to_extent="1" name="0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{d5483f4e-a22e-4271-944b-ea1c221a1e02}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#FFFFFF" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="114,114,114,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="1" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{ada4d764-1800-4be0-b44e-1a0267731575}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@1@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{a93a603a-5320-4f1d-aca8-25a590afd5f3}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="10" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{4d845302-b356-498f-bc23-9f47489b734a}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="277676565" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@10@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{8ae8afc5-1fc8-44d0-adc6-1b6f5304ecfc}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="11" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{54d12729-07c9-4059-b3e5-cad448e66081}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@11@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{23e861cd-9109-41fd-a642-f05c7e917c13}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{421d83be-cc0f-40f8-94cd-bddac18c06d5}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="12" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{06b8d6d9-c061-4aa4-b312-51e29ddfedb1}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#00FF00" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="13" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{20ccdd5b-d3ed-40be-a513-165e5fd521c2}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@13@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{8d455420-3f57-4acb-ab62-cd9e62c11707}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="14" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{9857aea4-33eb-4940-aa3c-6cb23389cb4f}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@14@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{006211fe-c1e8-4654-8d48-58559fc77dc3}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="15" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{bd94d1b3-5cdc-4861-91a5-d3f9a52fe98d}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="600726863" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@15@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{a6412de4-c72c-4330-9099-a1128b1598ac}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="16" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{a5e6b940-e4f6-4021-bde9-f2f9dae2c6dd}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="506315836" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@16@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{3f7942ad-5238-4496-ad71-4c8a871fa6c4}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="17" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{b589a4c9-409a-4e72-9f7c-6a3eb0306133}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@17@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{cf543d5c-dac9-45f2-a05f-0f56ef844953}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{f232ffd4-2a21-408c-af6d-5e08e763ee38}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="18" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{f6b4d89f-00d4-45ca-8732-b817ba59f132}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#FFE600" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="19" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{c1f18527-ba8f-407e-85a3-8e6dfc3643b3}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@19@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{77ffc817-33b0-49f9-9337-c61dcb844742}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="2" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{9fac9297-1863-46af-8778-b318fb446ed4}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@2@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{9587aaa3-1d2e-42ec-9ede-7ab044bb5f59}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="20" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{89f1ea1a-787f-441a-9df4-05530f230fc7}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@20@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{ec0e80d7-ff86-4325-a002-81fcdae48712}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="21" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{6fc984ab-5579-4ffb-b58e-1c599d0c5c7e}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="935045882" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@21@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{e9786dd9-adc1-458b-a2f4-ec1317360d3e}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="22" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{60d86b76-33cd-4a43-b627-86ffeaad5b24}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="630480019" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@22@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{def0a98d-fe63-491b-9ff7-b0c8be0ac3bf}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="23" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{b401a3e9-e303-42d2-924a-4fefa76da00e}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@23@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{7fd2e282-e7c6-48e4-9bd8-b8bbc4bd3665}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{3ec349ba-fd4c-4ade-84e2-a9edbd6a104e}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="24" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{7004b3c6-b6db-4a29-8527-cd06eb545d0b}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#FF0000" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="25" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{2bb5fe80-ec6d-4f18-9970-48cde73b98b1}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@25@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{5dea0b48-6210-4bc4-a3b2-efe48faa08b8}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="26" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{95886658-e441-49fa-aad7-a747dbbc5b0c}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@26@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{8465e106-d6c5-46ff-b302-ec35005b2664}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="27" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{759d4e24-8f1e-4fe7-ac41-cd8b1a654a32}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="157970429" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@27@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{b18186c3-6bd6-440c-8e52-43f539acec4e}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="28" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{ed688f7d-005b-4b87-8e15-01c4ff188027}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="727645427" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@28@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{052f74ea-715c-4832-8914-0f5885535496}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="29" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{a5001f0a-1aa3-4e02-8fed-6161cfa2187e}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@29@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{69cdeeb5-46f3-4130-8401-3c35674a7c60}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{57520672-b933-4764-9a20-c23d3f5446e6}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="3" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{a7e84750-efcf-4d8f-a139-a6537f956ff6}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="297620758" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@3@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{c61e6fc1-b21a-43c2-89d4-4f63a825faf3}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="30" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{352f7b20-09cd-42bc-9bbb-6f4449521d60}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#0073FF" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="31" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{23e8996a-39df-44ff-bb3d-5a52e2789e7c}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@31@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{9a940502-13dc-4f91-98f2-4804319f1d9f}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="32" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{268662e4-d142-471f-b4e8-d4ecc44322b6}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@32@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{3283dafe-c536-4f92-b2d1-67ff98e42160}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="33" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{a161e9ec-a824-4246-9f63-293c1555c5af}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="522602639" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@33@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{18c122cf-db7f-4d27-a509-1872f5267400}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="34" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{14c821bc-7929-4b9c-8a05-4e3f6e93f16e}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="788876849" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@34@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{1c2f49f7-f691-47c8-bb29-e71f6ee719b9}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="35" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{4bd8956e-d446-4690-8580-1779d17c6db3}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@35@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{6c0f460e-e111-4d4c-a919-75cdc62c8958}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{44b9ce00-750f-4a84-9bd1-18d3056ae6b6}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="36" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{99340eb4-1f87-4664-83c8-c07907630cbb}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#A800FF" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="37" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{40043385-92db-4d61-a3c8-041e952bd19a}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@37@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{fda86274-5df6-4fbc-8122-e80a0fcebded}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="38" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{9c9ff649-4df6-4399-80d7-370b1717c72d}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@38@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{8fc84e57-1387-4935-b05a-b613378329b7}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="39" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{2bd14b9d-fc3f-4936-98fe-950a371c25a7}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="288173135" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@39@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{feb929b4-58a7-44e9-bdd1-72a38c6a324a}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="4" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{5762adfd-2379-40ee-8dd9-97a74f0b1431}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="277676565" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@4@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{86f06f58-a0b5-408c-81d1-3a1833ba07b9}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="40" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{da433ccc-45c0-4d3c-930e-bf1d3c1b6fe0}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="537380890" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@40@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{cbce3e48-ac19-4344-b6ad-91b120dd8cfc}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="41" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{c64d63b4-3510-4d41-bd16-82026df21734}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@41@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{82e393d5-2e70-4d55-a71e-c21db306d49f}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{f5d98df1-1e31-4269-8d8f-31ab9afea527}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="5" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{881cdfff-068d-4401-be09-be3e0b41ca60}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@5@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{768caaf1-678a-418f-a3d3-aec24bcd02db}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{e7ba5633-f4ca-46a2-9ada-2dfa97e58074}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="6" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{23ed8825-c973-48af-804d-3ad5338693a6}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#B2B2B2" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="114,114,114,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="7" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{d4cb980c-03e7-4b0c-87d4-2bf88eee595e}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@7@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{1d8aa43f-4fd1-475e-98f0-84f143455d5f}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="8" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{c1f93dc3-b936-4bfd-b089-285ea2c9db01}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@8@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{3c0db9e7-1d8e-4eb9-9b38-7660ed654107}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="9" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{169beaf9-46c3-4472-bd0e-4a1207f344a4}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="297620758" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@9@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{1f0c5d5e-641f-46ea-b2ff-4b1a3fb265bc}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <selection mode="Default">
    <selectionColor invalid="1" />
    <selectionSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{f6b09726-91e8-44fe-9dbd-ecd85ed78a27}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="0,0,255,255" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </selectionSymbol>
  </selection>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style allowHtml="0" textColor="0,0,0,255" fontFamily="DejaVu Sans" fontWeight="50" multilineHeightUnit="Percentage" fontSizeMapUnitScale="3x:0,0,0,0,0,0" forcedItalic="0" previewBkgrdColor="255,255,255,255" blendMode="0" fieldName="affectedradio" fontLetterSpacing="0" fontItalic="0" fontSizeUnit="Point" fontKerning="1" legendString="Aa" fontStrikeout="0" textOpacity="1" multilineHeight="1" fontUnderline="0" useSubstitutions="0" fontWordSpacing="0" capitalization="0" forcedBold="0" textOrientation="horizontal" namedStyle="Book" fontSize="10" isExpression="0">
        <families />
        <text-buffer bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferBlendMode="0" bufferNoFill="1" bufferOpacity="1" bufferSize="1" bufferJoinStyle="128" bufferColor="255,255,255,255" bufferDraw="1" bufferSizeUnits="MM" />
        <text-mask maskOpacity="1" maskedSymbolLayers="" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskType="0" maskSize="1.5" maskJoinStyle="128" maskEnabled="0" maskSizeUnits="MM" maskSize2="1.5" />
        <background shapeBorderWidthUnit="MM" shapeSizeX="0" shapeBorderWidth="0" shapeSizeType="0" shapeSizeUnit="MM" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeOffsetUnit="MM" shapeJoinStyle="64" shapeRadiiX="0" shapeRadiiY="0" shapeRotation="0" shapeBlendMode="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeOpacity="1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderColor="128,128,128,255" shapeType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSVGFile="" shapeRotationType="0" shapeOffsetX="0" shapeDraw="0">
          <symbol clip_to_extent="1" name="markerSymbol" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="243,166,178,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="35,35,35,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol clip_to_extent="1" name="fillSymbol" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleFill" pass="0" id="" locked="0" enabled="1">
              <Option type="Map">
                <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="128,128,128,255" type="QString" />
                <Option name="outline_style" value="no" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="style" value="solid" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowBlendMode="6" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowScale="100" shadowOffsetGlobal="1" shadowRadiusAlphaOnly="0" shadowRadiusUnit="MM" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOpacity="0.69999999999999996" shadowDraw="0" shadowColor="0,0,0,255" shadowUnder="0" shadowOffsetUnit="MM" shadowOffsetDist="1" shadowOffsetAngle="135" shadowRadius="1.5" />
        <dd_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </dd_properties>
        <substitutions />
      </text-style>
      <text-format wrapChar="-" multilineAlign="0" reverseDirectionSymbol="0" decimals="3" placeDirectionSymbol="0" autoWrapLength="0" rightDirectionSymbol="&gt;" useMaxLineLengthForAutoWrap="1" plussign="0" leftDirectionSymbol="&lt;" formatNumbers="0" addDirectionSymbol="0" />
      <placement centroidWhole="1" offsetType="0" dist="0" maxCurvedCharAngleIn="25" lineAnchorType="0" overlapHandling="PreventOverlap" geometryGeneratorType="PointGeometry" distMapUnitScale="3x:0,0,0,0,0,0" quadOffset="4" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" offsetUnits="MM" polygonPlacementFlags="2" centroidInside="0" maxCurvedCharAngleOut="-25" preserveRotation="1" priority="5" geometryGeneratorEnabled="0" placement="1" xOffset="0" lineAnchorTextPoint="CenterOfText" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0" lineAnchorClipping="0" fitInPolygonOnly="0" distUnits="MM" overrunDistance="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" yOffset="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" repeatDistanceUnits="MM" layerType="PolygonGeometry" rotationUnit="AngleDegrees" allowDegraded="0" geometryGenerator="" rotationAngle="0" lineAnchorPercent="0.5" placementFlags="10" overrunDistanceUnit="MM" />
      <rendering obstacleFactor="1" fontMaxPixelSize="10000" obstacle="1" drawLabels="1" unplacedVisibility="0" limitNumLabels="0" maxNumLabels="2000" labelPerPart="0" scaleMin="0" fontMinPixelSize="3" scaleVisibility="0" upsidedownLabels="0" mergeLines="0" zIndex="0" minFeatureSize="0" obstacleType="0" scaleMax="0" fontLimitPixelSize="0" />
      <dd_properties>
        <Option type="Map">
          <Option name="name" value="" type="QString" />
          <Option name="properties" />
          <Option name="type" value="collection" type="QString" />
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option name="anchorPoint" value="pole_of_inaccessibility" type="QString" />
          <Option name="blendMode" value="0" type="int" />
          <Option name="ddProperties" type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
          <Option name="drawToAllParts" value="false" type="bool" />
          <Option name="enabled" value="0" type="QString" />
          <Option name="labelAnchorPoint" value="point_on_exterior" type="QString" />
          <Option name="lineSymbol" value="&lt;symbol clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; force_rhr=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;line&quot;&gt;&lt;data_defined_properties&gt;&lt;Option type=&quot;Map&quot;&gt;&lt;Option name=&quot;name&quot; value=&quot;&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;properties&quot;/&gt;&lt;Option name=&quot;type&quot; value=&quot;collection&quot; type=&quot;QString&quot;/&gt;&lt;/Option&gt;&lt;/data_defined_properties&gt;&lt;layer class=&quot;SimpleLine&quot; pass=&quot;0&quot; id=&quot;{66879105-b5c6-4fcc-acfd-ec6556e61c50}&quot; locked=&quot;0&quot; enabled=&quot;1&quot;&gt;&lt;Option type=&quot;Map&quot;&gt;&lt;Option name=&quot;align_dash_pattern&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;capstyle&quot; value=&quot;square&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;customdash&quot; value=&quot;5;2&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;customdash_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;customdash_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;dash_pattern_offset&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;dash_pattern_offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;dash_pattern_offset_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;draw_inside_polygon&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;joinstyle&quot; value=&quot;bevel&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;line_color&quot; value=&quot;60,60,60,255&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;line_style&quot; value=&quot;solid&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;line_width&quot; value=&quot;0.3&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;line_width_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;offset&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;offset_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;ring_filter&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_end&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_end_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_end_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_start&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_start_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_start_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;tweak_dash_pattern_on_corners&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;use_custom_dash&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;/Option&gt;&lt;data_defined_properties&gt;&lt;Option type=&quot;Map&quot;&gt;&lt;Option name=&quot;name&quot; value=&quot;&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;properties&quot;/&gt;&lt;Option name=&quot;type&quot; value=&quot;collection&quot; type=&quot;QString&quot;/&gt;&lt;/Option&gt;&lt;/data_defined_properties&gt;&lt;/layer&gt;&lt;/symbol&gt;" type="QString" />
          <Option name="minLength" value="0" type="double" />
          <Option name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0" type="QString" />
          <Option name="minLengthUnit" value="MM" type="QString" />
          <Option name="offsetFromAnchor" value="0" type="double" />
          <Option name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0" type="QString" />
          <Option name="offsetFromAnchorUnit" value="MM" type="QString" />
          <Option name="offsetFromLabel" value="0" type="double" />
          <Option name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0" type="QString" />
          <Option name="offsetFromLabelUnit" value="MM" type="QString" />
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option name="embeddedWidgets/count" value="0" type="QString" />
      <Option name="variableNames" />
      <Option name="variableValues" />
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <LinearlyInterpolatedDiagramRenderer lowerValue="0" lowerHeight="0" attributeLegend="1" classificationAttributeExpression="" lowerWidth="0" upperValue="0" diagramType="Histogram" upperWidth="50" upperHeight="50">
    <DiagramCategory barWidth="5" labelPlacementMethod="XHeight" width="15" lineSizeType="MM" minimumSize="0" penWidth="0" sizeScale="3x:0,0,0,0,0,0" backgroundColor="#ffffff" scaleBasedVisibility="0" direction="1" minScaleDenominator="-4.65661e-10" sizeType="MM" penAlpha="255" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" scaleDependency="Area" penColor="#000000" opacity="1" spacingUnit="MM" spacingUnitScale="3x:0,0,0,0,0,0" rotationOffset="270" showAxis="0" diagramOrientation="Up" height="15" maxScaleDenominator="1e+08" enabled="0" spacing="0">
      <fontProperties italic="0" style="" bold="0" underline="0" description="Ubuntu,9,-1,5,50,0,0,0,0,0" strikethrough="0" />
      <attribute colorOpacity="1" field="" label="" color="#000000" />
      <axisSymbol>
        <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" pass="0" id="{dec8f34f-5da3-4d88-a650-3d16ff8fe675}" locked="0" enabled="1">
            <Option type="Map">
              <Option name="align_dash_pattern" value="0" type="QString" />
              <Option name="capstyle" value="square" type="QString" />
              <Option name="customdash" value="5;2" type="QString" />
              <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="customdash_unit" value="MM" type="QString" />
              <Option name="dash_pattern_offset" value="0" type="QString" />
              <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
              <Option name="draw_inside_polygon" value="0" type="QString" />
              <Option name="joinstyle" value="bevel" type="QString" />
              <Option name="line_color" value="35,35,35,255" type="QString" />
              <Option name="line_style" value="solid" type="QString" />
              <Option name="line_width" value="0.26" type="QString" />
              <Option name="line_width_unit" value="MM" type="QString" />
              <Option name="offset" value="0" type="QString" />
              <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="offset_unit" value="MM" type="QString" />
              <Option name="ring_filter" value="0" type="QString" />
              <Option name="trim_distance_end" value="0" type="QString" />
              <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="trim_distance_end_unit" value="MM" type="QString" />
              <Option name="trim_distance_start" value="0" type="QString" />
              <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="trim_distance_start_unit" value="MM" type="QString" />
              <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
              <Option name="use_custom_dash" value="0" type="QString" />
              <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </LinearlyInterpolatedDiagramRenderer>
  <DiagramLayerSettings dist="0" showAll="1" obstacle="0" priority="0" zIndex="0" placement="0" linePlacementFlags="2">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString" />
        <Option name="properties" />
        <Option name="type" value="collection" type="QString" />
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks />
    <checkConfiguration type="Map">
      <Option name="QgsGeometryGapCheck" type="Map">
        <Option name="allowedGapsBuffer" value="0" type="double" />
        <Option name="allowedGapsEnabled" value="false" type="bool" />
        <Option name="allowedGapsLayer" value="" type="QString" />
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector" />
  <referencedLayers />
  <fieldConfiguration>
    <field name="id" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="smu" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="load" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="deposit" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="panel" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="blast" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="radiometry" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="radiometry_probe" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lithology" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="unit" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="centroid_z" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="affectedradio" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="radiometry_class" configurationFlags="NoFlag">
      <editWidget type="Range">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="id" index="0" />
    <alias name="" field="smu" index="1" />
    <alias name="" field="load" index="2" />
    <alias name="" field="deposit" index="3" />
    <alias name="" field="panel" index="4" />
    <alias name="" field="blast" index="5" />
    <alias name="" field="radiometry" index="6" />
    <alias name="" field="radiometry_probe" index="7" />
    <alias name="" field="lithology" index="8" />
    <alias name="" field="unit" index="9" />
    <alias name="" field="centroid_z" index="10" />
    <alias name="" field="affectedradio" index="11" />
    <alias name="" field="radiometry_class" index="12" />
  </aliases>
  <splitPolicies>
    <policy policy="Duplicate" field="id" />
    <policy policy="Duplicate" field="smu" />
    <policy policy="Duplicate" field="load" />
    <policy policy="Duplicate" field="deposit" />
    <policy policy="Duplicate" field="panel" />
    <policy policy="Duplicate" field="blast" />
    <policy policy="Duplicate" field="radiometry" />
    <policy policy="Duplicate" field="radiometry_probe" />
    <policy policy="Duplicate" field="lithology" />
    <policy policy="Duplicate" field="unit" />
    <policy policy="Duplicate" field="centroid_z" />
    <policy policy="Duplicate" field="affectedradio" />
    <policy policy="Duplicate" field="radiometry_class" />
  </splitPolicies>
  <defaults>
    <default applyOnUpdate="0" field="id" expression="" />
    <default applyOnUpdate="0" field="smu" expression="" />
    <default applyOnUpdate="0" field="load" expression="" />
    <default applyOnUpdate="0" field="deposit" expression="" />
    <default applyOnUpdate="0" field="panel" expression="" />
    <default applyOnUpdate="0" field="blast" expression="" />
    <default applyOnUpdate="0" field="radiometry" expression="" />
    <default applyOnUpdate="0" field="radiometry_probe" expression="" />
    <default applyOnUpdate="0" field="lithology" expression="" />
    <default applyOnUpdate="0" field="unit" expression="" />
    <default applyOnUpdate="0" field="centroid_z" expression="" />
    <default applyOnUpdate="0" field="affectedradio" expression="" />
    <default applyOnUpdate="0" field="radiometry_class" expression="" />
  </defaults>
  <constraints>
    <constraint notnull_strength="1" exp_strength="0" field="id" unique_strength="1" constraints="3" />
    <constraint notnull_strength="0" exp_strength="0" field="smu" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="load" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="deposit" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="panel" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="blast" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="radiometry" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="radiometry_probe" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="lithology" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="unit" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="centroid_z" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="affectedradio" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="radiometry_class" unique_strength="0" constraints="0" />
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp="" />
    <constraint desc="" field="smu" exp="" />
    <constraint desc="" field="load" exp="" />
    <constraint desc="" field="deposit" exp="" />
    <constraint desc="" field="panel" exp="" />
    <constraint desc="" field="blast" exp="" />
    <constraint desc="" field="radiometry" exp="" />
    <constraint desc="" field="radiometry_probe" exp="" />
    <constraint desc="" field="lithology" exp="" />
    <constraint desc="" field="unit" exp="" />
    <constraint desc="" field="centroid_z" exp="" />
    <constraint desc="" field="affectedradio" exp="" />
    <constraint desc="" field="radiometry_class" exp="" />
  </constraintExpressions>
  <expressionfields />
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}" />
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="" sortOrder="0">
    <columns>
      <column name="id" width="-1" hidden="0" type="field" />
      <column name="smu" width="-1" hidden="0" type="field" />
      <column name="load" width="-1" hidden="0" type="field" />
      <column name="deposit" width="-1" hidden="0" type="field" />
      <column name="panel" width="-1" hidden="0" type="field" />
      <column name="blast" width="-1" hidden="0" type="field" />
      <column name="radiometry" width="-1" hidden="0" type="field" />
      <column name="radiometry_probe" width="-1" hidden="0" type="field" />
      <column name="lithology" width="-1" hidden="0" type="field" />
      <column name="unit" width="-1" hidden="0" type="field" />
      <column name="centroid_z" width="-1" hidden="0" type="field" />
      <column width="-1" hidden="1" type="actions" />
      <column name="radiometry_class" width="-1" hidden="0" type="field" />
      <column name="affectedradio" width="-1" hidden="0" type="field" />
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles />
    <fieldstyles />
  </conditionalstyles>
  <storedexpressions />
  <editform tolerant="1" />
  <editforminit />
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>/../.qgis2/python/docs/affaires/areva</editforminitfilepath>
  <editforminitcode># -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appel&#233;e &#224; l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalit&#233;s &#224; vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple &#224; suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

</editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="affectedradio" editable="1" />
    <field name="blast" editable="1" />
    <field name="centroid_z" editable="1" />
    <field name="deposit" editable="1" />
    <field name="id" editable="1" />
    <field name="lithology" editable="1" />
    <field name="load" editable="1" />
    <field name="panel" editable="1" />
    <field name="radiometry" editable="1" />
    <field name="radiometry_class" editable="1" />
    <field name="radiometry_probe" editable="1" />
    <field name="smu" editable="1" />
    <field name="unit" editable="1" />
  </editable>
  <labelOnTop>
    <field name="affectedradio" labelOnTop="0" />
    <field name="blast" labelOnTop="0" />
    <field name="centroid_z" labelOnTop="0" />
    <field name="deposit" labelOnTop="0" />
    <field name="id" labelOnTop="0" />
    <field name="lithology" labelOnTop="0" />
    <field name="load" labelOnTop="0" />
    <field name="panel" labelOnTop="0" />
    <field name="radiometry" labelOnTop="0" />
    <field name="radiometry_class" labelOnTop="0" />
    <field name="radiometry_probe" labelOnTop="0" />
    <field name="smu" labelOnTop="0" />
    <field name="unit" labelOnTop="0" />
  </labelOnTop>
  <reuseLastValue>
    <field name="affectedradio" reuseLastValue="0" />
    <field name="blast" reuseLastValue="0" />
    <field name="centroid_z" reuseLastValue="0" />
    <field name="deposit" reuseLastValue="0" />
    <field name="id" reuseLastValue="0" />
    <field name="lithology" reuseLastValue="0" />
    <field name="load" reuseLastValue="0" />
    <field name="panel" reuseLastValue="0" />
    <field name="radiometry" reuseLastValue="0" />
    <field name="radiometry_class" reuseLastValue="0" />
    <field name="radiometry_probe" reuseLastValue="0" />
    <field name="smu" reuseLastValue="0" />
    <field name="unit" reuseLastValue="0" />
  </reuseLastValue>
  <dataDefinedFieldProperties />
  <widgets />
  <previewExpression>COALESCE( "id", '&lt;NULL&gt;' )</previewExpression>
  <mapTip enabled="1" />
  <layerGeometryType>2</layerGeometryType>
</qgis>