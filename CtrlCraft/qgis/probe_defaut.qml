<qgis version="3.34.10-Prizren" simplifyDrawingHints="0" simplifyMaxScale="1" styleCategories="AllStyleCategories" maxScale="0" readOnly="0" simplifyAlgorithm="0" labelsEnabled="0" simplifyLocal="1" symbologyReferenceScale="-1" hasScaleBasedVisibilityFlag="0" minScale="100000000" simplifyDrawingTol="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="time" durationUnit="min" accumulate="0" limitMode="0" endExpression="" fixedDuration="0" startExpression="" endField="" enabled="0" mode="0" durationField="radiometry">
    <fixedRange>
      <start />
      <end />
    </fixedRange>
  </temporal>
  <elevation binding="Centroid" respectLayerSymbol="1" zscale="1" extrusionEnabled="0" clamping="Terrain" zoffset="0" showMarkerSymbolInSurfacePlots="0" symbology="Line" extrusion="0" type="IndividualFeatures">
    <data-defined-properties>
      <Option type="Map">
        <Option name="name" value="" type="QString" />
        <Option name="properties" />
        <Option name="type" value="collection" type="QString" />
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" id="{b9eab4ee-3340-4097-948b-6a8db2f02271}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="align_dash_pattern" value="0" type="QString" />
            <Option name="capstyle" value="square" type="QString" />
            <Option name="customdash" value="5;2" type="QString" />
            <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="customdash_unit" value="MM" type="QString" />
            <Option name="dash_pattern_offset" value="0" type="QString" />
            <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
            <Option name="draw_inside_polygon" value="0" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="line_color" value="196,60,57,255" type="QString" />
            <Option name="line_style" value="solid" type="QString" />
            <Option name="line_width" value="0.6" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="ring_filter" value="0" type="QString" />
            <Option name="trim_distance_end" value="0" type="QString" />
            <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="trim_distance_end_unit" value="MM" type="QString" />
            <Option name="trim_distance_start" value="0" type="QString" />
            <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="trim_distance_start_unit" value="MM" type="QString" />
            <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
            <Option name="use_custom_dash" value="0" type="QString" />
            <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{b16dc787-9ca2-46ec-9e1c-2049d9e69d7a}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="196,60,57,255" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="140,43,41,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.2" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{fa1e93b5-02d5-48be-92a2-186ecca03d67}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="196,60,57,255" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="diamond" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="140,43,41,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.2" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="diameter" type="QString" />
            <Option name="size" value="3" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 symbollevels="0" referencescale="-1" forceraster="0" enableorderby="0" type="RuleRenderer">
    <rules key="{a81a0ce3-63f2-4aff-ae47-9dd7be108c70}">
      <rule key="{a3adcb55-1fd1-44a9-bc3b-cd501c00359e}" symbol="0" filter="&quot;radiometry_class&quot; = 0" label="-999.0" checkstate="1" />
      <rule key="{c748dba8-ed71-4665-ad61-330331b9a700}" symbol="1" filter="&quot;radiometry_class&quot; = 1" label="0.0 - 0.7" checkstate="1" />
      <rule key="{b44737fb-8da6-4c12-a1b8-c632ca56663e}" symbol="2" filter="&quot;radiometry_class&quot; = 2" label="0.7 - 1.3" checkstate="1" />
      <rule key="{cd3a9273-34e7-4044-a830-e7a6495c9f84}" symbol="3" filter="&quot;radiometry_class&quot; = 3" label="1.3 - 1.4" checkstate="1" />
      <rule key="{90640a84-dcc8-4953-8f36-3b9e1ed18c90}" symbol="4" filter="&quot;radiometry_class&quot; = 4" label="1.4 - 1.8" checkstate="1" />
      <rule key="{d8d54c6f-cb45-4b2f-b1cc-7c03249effa4}" symbol="5" filter="&quot;radiometry_class&quot; = 5" label="1.8 - 2.5" checkstate="1" />
      <rule key="{a1273450-a2da-4dca-87c8-1d3e3d6bc8c3}" symbol="6" filter="&quot;radiometry_class&quot; = 6" label="+2.5" checkstate="1" />
      <rule key="{c422a11f-6461-4bf8-bfc6-5a7e107f1d6d}" symbol="7" filter="ELSE" />
    </rules>
    <symbols>
      <symbol clip_to_extent="1" name="0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{05549570-a83b-4665-9776-dd8e0f875b74}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="#FFFFFF" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="circle" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="0,0,0,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="area" type="QString" />
            <Option name="size" value="2" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="1" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{fb882e3f-20c9-4e57-8d7f-fd778b8e79e1}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="#B2B2B2" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="circle" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="0,0,0,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="area" type="QString" />
            <Option name="size" value="2" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="2" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{6e1d60c5-de5d-4eac-bb49-bad0a5b365b6}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="#00FF00" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="circle" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="0,0,0,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="area" type="QString" />
            <Option name="size" value="2" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="3" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{0bcf2787-d9c8-47e6-9c52-1c812ccddc59}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="#FFE600" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="circle" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="0,0,0,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="diameter" type="QString" />
            <Option name="size" value="2" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="4" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{f1cf737b-7330-4ccd-a463-e668727261ed}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="#FF0000" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="circle" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="0,0,0,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="diameter" type="QString" />
            <Option name="size" value="2" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="5" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{39aaea49-175e-4076-b1a5-11a2b95b4733}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="#0073FF" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="circle" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="0,0,0,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="diameter" type="QString" />
            <Option name="size" value="2" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="6" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{bbb4e2e3-83d3-482d-bf21-2c67d3f9d085}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="#A800FF" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="circle" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="0,0,0,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="diameter" type="QString" />
            <Option name="size" value="2" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="7" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{55289f99-dd67-496a-8601-e6c317124d2c}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="231,113,72,255" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="triangle" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="diameter" type="QString" />
            <Option name="size" value="2" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <selection mode="Default">
    <selectionColor invalid="1" />
    <selectionSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{33b6f4af-7c5d-49af-b305-1bc5e2bf6b70}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="255,0,0,255" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="circle" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="diameter" type="QString" />
            <Option name="size" value="2" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </selectionSymbol>
  </selection>
  <customproperties>
    <Option type="Map">
      <Option name="embeddedWidgets/count" value="0" type="QString" />
      <Option name="variableNames" />
      <Option name="variableValues" />
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <LinearlyInterpolatedDiagramRenderer lowerValue="0" lowerHeight="0" attributeLegend="1" classificationAttributeExpression="" lowerWidth="0" upperValue="0" diagramType="Histogram" upperWidth="50" upperHeight="50">
    <DiagramCategory barWidth="5" labelPlacementMethod="XHeight" width="15" lineSizeType="MM" minimumSize="0" penWidth="0" sizeScale="3x:0,0,0,0,0,0" backgroundColor="#ffffff" scaleBasedVisibility="0" direction="1" minScaleDenominator="0" sizeType="MM" penAlpha="255" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" scaleDependency="Area" penColor="#000000" opacity="1" spacingUnit="MM" spacingUnitScale="3x:0,0,0,0,0,0" rotationOffset="270" showAxis="0" diagramOrientation="Up" height="15" maxScaleDenominator="1e+08" enabled="0" spacing="0">
      <fontProperties italic="0" style="" bold="0" underline="0" description="Sans Serif,9,-1,5,50,0,0,0,0,0" strikethrough="0" />
      <attribute colorOpacity="1" field="" label="" color="#000000" />
      <axisSymbol>
        <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" pass="0" id="{1d7f0cb6-8c0d-4983-85bb-3a34bab2571b}" locked="0" enabled="1">
            <Option type="Map">
              <Option name="align_dash_pattern" value="0" type="QString" />
              <Option name="capstyle" value="square" type="QString" />
              <Option name="customdash" value="5;2" type="QString" />
              <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="customdash_unit" value="MM" type="QString" />
              <Option name="dash_pattern_offset" value="0" type="QString" />
              <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
              <Option name="draw_inside_polygon" value="0" type="QString" />
              <Option name="joinstyle" value="bevel" type="QString" />
              <Option name="line_color" value="35,35,35,255" type="QString" />
              <Option name="line_style" value="solid" type="QString" />
              <Option name="line_width" value="0.26" type="QString" />
              <Option name="line_width_unit" value="MM" type="QString" />
              <Option name="offset" value="0" type="QString" />
              <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="offset_unit" value="MM" type="QString" />
              <Option name="ring_filter" value="0" type="QString" />
              <Option name="trim_distance_end" value="0" type="QString" />
              <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="trim_distance_end_unit" value="MM" type="QString" />
              <Option name="trim_distance_start" value="0" type="QString" />
              <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="trim_distance_start_unit" value="MM" type="QString" />
              <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
              <Option name="use_custom_dash" value="0" type="QString" />
              <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </LinearlyInterpolatedDiagramRenderer>
  <DiagramLayerSettings dist="0" showAll="1" obstacle="0" priority="0" zIndex="0" placement="0" linePlacementFlags="2">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString" />
        <Option name="properties" />
        <Option name="type" value="collection" type="QString" />
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks />
    <checkConfiguration />
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector" />
  <referencedLayers />
  <fieldConfiguration>
    <field name="id" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="radiometry" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="alt" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="inclination" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="gammaup" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="gammadown" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="teneurup" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="teneurdown" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="time" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="serial_number_gps" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="serial_number_gamma" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="status" configurationFlags="NoFlag">
      <editWidget type="Range">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="coeff_a_up" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="coeff_a_low" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="coeff_b_up" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="coeff_b_low" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="coeff_n_up" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="coeff_n_low" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="id" index="0" />
    <alias name="" field="radiometry" index="1" />
    <alias name="" field="alt" index="2" />
    <alias name="" field="inclination" index="3" />
    <alias name="" field="gammaup" index="4" />
    <alias name="" field="gammadown" index="5" />
    <alias name="" field="teneurup" index="6" />
    <alias name="" field="teneurdown" index="7" />
    <alias name="" field="time" index="8" />
    <alias name="" field="serial_number_gps" index="9" />
    <alias name="" field="serial_number_gamma" index="10" />
    <alias name="" field="status" index="11" />
    <alias name="" field="coeff_a_up" index="12" />
    <alias name="" field="coeff_a_low" index="13" />
    <alias name="" field="coeff_b_up" index="14" />
    <alias name="" field="coeff_b_low" index="15" />
    <alias name="" field="coeff_n_up" index="16" />
    <alias name="" field="coeff_n_low" index="17" />
  </aliases>
  <splitPolicies>
    <policy policy="Duplicate" field="id" />
    <policy policy="Duplicate" field="radiometry" />
    <policy policy="Duplicate" field="alt" />
    <policy policy="Duplicate" field="inclination" />
    <policy policy="Duplicate" field="gammaup" />
    <policy policy="Duplicate" field="gammadown" />
    <policy policy="Duplicate" field="teneurup" />
    <policy policy="Duplicate" field="teneurdown" />
    <policy policy="Duplicate" field="time" />
    <policy policy="Duplicate" field="serial_number_gps" />
    <policy policy="Duplicate" field="serial_number_gamma" />
    <policy policy="Duplicate" field="status" />
    <policy policy="Duplicate" field="coeff_a_up" />
    <policy policy="Duplicate" field="coeff_a_low" />
    <policy policy="Duplicate" field="coeff_b_up" />
    <policy policy="Duplicate" field="coeff_b_low" />
    <policy policy="Duplicate" field="coeff_n_up" />
    <policy policy="Duplicate" field="coeff_n_low" />
  </splitPolicies>
  <defaults>
    <default applyOnUpdate="0" field="id" expression="" />
    <default applyOnUpdate="0" field="radiometry" expression="" />
    <default applyOnUpdate="0" field="alt" expression="" />
    <default applyOnUpdate="0" field="inclination" expression="" />
    <default applyOnUpdate="0" field="gammaup" expression="" />
    <default applyOnUpdate="0" field="gammadown" expression="" />
    <default applyOnUpdate="0" field="teneurup" expression="" />
    <default applyOnUpdate="0" field="teneurdown" expression="" />
    <default applyOnUpdate="0" field="time" expression="" />
    <default applyOnUpdate="0" field="serial_number_gps" expression="" />
    <default applyOnUpdate="0" field="serial_number_gamma" expression="" />
    <default applyOnUpdate="0" field="status" expression="" />
    <default applyOnUpdate="0" field="coeff_a_up" expression="" />
    <default applyOnUpdate="0" field="coeff_a_low" expression="" />
    <default applyOnUpdate="0" field="coeff_b_up" expression="" />
    <default applyOnUpdate="0" field="coeff_b_low" expression="" />
    <default applyOnUpdate="0" field="coeff_n_up" expression="" />
    <default applyOnUpdate="0" field="coeff_n_low" expression="" />
  </defaults>
  <constraints>
    <constraint notnull_strength="1" exp_strength="0" field="id" unique_strength="1" constraints="3" />
    <constraint notnull_strength="0" exp_strength="0" field="radiometry" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="alt" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="inclination" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="gammaup" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="gammadown" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="teneurup" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="teneurdown" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="time" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="serial_number_gps" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="serial_number_gamma" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="status" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="coeff_a_up" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="coeff_a_low" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="coeff_b_up" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="coeff_b_low" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="coeff_n_up" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="coeff_n_low" unique_strength="0" constraints="0" />
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp="" />
    <constraint desc="" field="radiometry" exp="" />
    <constraint desc="" field="alt" exp="" />
    <constraint desc="" field="inclination" exp="" />
    <constraint desc="" field="gammaup" exp="" />
    <constraint desc="" field="gammadown" exp="" />
    <constraint desc="" field="teneurup" exp="" />
    <constraint desc="" field="teneurdown" exp="" />
    <constraint desc="" field="time" exp="" />
    <constraint desc="" field="serial_number_gps" exp="" />
    <constraint desc="" field="serial_number_gamma" exp="" />
    <constraint desc="" field="status" exp="" />
    <constraint desc="" field="coeff_a_up" exp="" />
    <constraint desc="" field="coeff_a_low" exp="" />
    <constraint desc="" field="coeff_b_up" exp="" />
    <constraint desc="" field="coeff_b_low" exp="" />
    <constraint desc="" field="coeff_n_up" exp="" />
    <constraint desc="" field="coeff_n_low" exp="" />
  </constraintExpressions>
  <expressionfields />
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}" />
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="" sortOrder="0">
    <columns>
      <column name="id" width="-1" hidden="0" type="field" />
      <column name="radiometry" width="-1" hidden="0" type="field" />
      <column name="alt" width="-1" hidden="0" type="field" />
      <column name="gammaup" width="-1" hidden="0" type="field" />
      <column name="gammadown" width="-1" hidden="0" type="field" />
      <column name="teneurup" width="-1" hidden="0" type="field" />
      <column name="teneurdown" width="-1" hidden="0" type="field" />
      <column name="time" width="-1" hidden="0" type="field" />
      <column width="-1" hidden="1" type="actions" />
      <column name="serial_number_gps" width="-1" hidden="0" type="field" />
      <column name="serial_number_gamma" width="-1" hidden="0" type="field" />
      <column name="status" width="-1" hidden="0" type="field" />
      <column name="coeff_a_up" width="-1" hidden="0" type="field" />
      <column name="coeff_a_low" width="-1" hidden="0" type="field" />
      <column name="coeff_b_up" width="-1" hidden="0" type="field" />
      <column name="coeff_b_low" width="-1" hidden="0" type="field" />
      <column name="coeff_n_up" width="-1" hidden="0" type="field" />
      <column name="coeff_n_low" width="-1" hidden="0" type="field" />
      <column name="inclination" width="-1" hidden="0" type="field" />
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles />
    <fieldstyles />
  </conditionalstyles>
  <storedexpressions />
  <editform tolerant="1">/../../../../../lbartoletti/prog/.local/share/QGIS/QGIS3/profiles/default/python/plugins/CtrlCraft/qgis</editform>
  <editforminit />
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>/../../../../../lbartoletti/prog/.local/share/QGIS/QGIS3/profiles/default/python/plugins/CtrlCraft/qgis</editforminitfilepath>
  <editforminitcode># -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
  geom = feature.geometry()
  control = dialog.findChild(QWidget, "MyLineEdit")
</editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="alt" editable="1" />
    <field name="coeff_a_low" editable="1" />
    <field name="coeff_a_up" editable="1" />
    <field name="coeff_b_low" editable="1" />
    <field name="coeff_b_up" editable="1" />
    <field name="coeff_n_low" editable="1" />
    <field name="coeff_n_up" editable="1" />
    <field name="gammadown" editable="1" />
    <field name="gammaup" editable="1" />
    <field name="id" editable="1" />
    <field name="inclination" editable="1" />
    <field name="radiometry" editable="1" />
    <field name="serial_number_gamma" editable="1" />
    <field name="serial_number_gps" editable="1" />
    <field name="status" editable="1" />
    <field name="teneurdown" editable="1" />
    <field name="teneurup" editable="1" />
    <field name="time" editable="1" />
  </editable>
  <labelOnTop>
    <field name="alt" labelOnTop="0" />
    <field name="coeff_a_low" labelOnTop="0" />
    <field name="coeff_a_up" labelOnTop="0" />
    <field name="coeff_b_low" labelOnTop="0" />
    <field name="coeff_b_up" labelOnTop="0" />
    <field name="coeff_n_low" labelOnTop="0" />
    <field name="coeff_n_up" labelOnTop="0" />
    <field name="gammadown" labelOnTop="0" />
    <field name="gammaup" labelOnTop="0" />
    <field name="id" labelOnTop="0" />
    <field name="inclination" labelOnTop="0" />
    <field name="radiometry" labelOnTop="0" />
    <field name="serial_number_gamma" labelOnTop="0" />
    <field name="serial_number_gps" labelOnTop="0" />
    <field name="status" labelOnTop="0" />
    <field name="teneurdown" labelOnTop="0" />
    <field name="teneurup" labelOnTop="0" />
    <field name="time" labelOnTop="0" />
  </labelOnTop>
  <reuseLastValue>
    <field name="alt" reuseLastValue="0" />
    <field name="coeff_a_low" reuseLastValue="0" />
    <field name="coeff_a_up" reuseLastValue="0" />
    <field name="coeff_b_low" reuseLastValue="0" />
    <field name="coeff_b_up" reuseLastValue="0" />
    <field name="coeff_n_low" reuseLastValue="0" />
    <field name="coeff_n_up" reuseLastValue="0" />
    <field name="gammadown" reuseLastValue="0" />
    <field name="gammaup" reuseLastValue="0" />
    <field name="id" reuseLastValue="0" />
    <field name="inclination" reuseLastValue="0" />
    <field name="radiometry" reuseLastValue="0" />
    <field name="serial_number_gamma" reuseLastValue="0" />
    <field name="serial_number_gps" reuseLastValue="0" />
    <field name="status" reuseLastValue="0" />
    <field name="teneurdown" reuseLastValue="0" />
    <field name="teneurup" reuseLastValue="0" />
    <field name="time" reuseLastValue="0" />
  </reuseLastValue>
  <dataDefinedFieldProperties />
  <widgets />
  <previewExpression>COALESCE( "id", '&lt;NULL&gt;' )</previewExpression>
  <mapTip enabled="1" />
  <layerGeometryType>0</layerGeometryType>
</qgis>