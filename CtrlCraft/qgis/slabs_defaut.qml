<qgis version="3.34.10-Prizren" simplifyDrawingHints="1" simplifyMaxScale="1" styleCategories="AllStyleCategories" maxScale="-4.6566128730773926e-10" readOnly="0" simplifyAlgorithm="0" labelsEnabled="1" simplifyLocal="1" symbologyReferenceScale="-1" hasScaleBasedVisibilityFlag="0" minScale="100000000" simplifyDrawingTol="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="" durationUnit="min" accumulate="0" limitMode="0" endExpression="" fixedDuration="0" startExpression="" endField="" enabled="0" mode="0" durationField="radiometry">
    <fixedRange>
      <start />
      <end />
    </fixedRange>
  </temporal>
  <elevation binding="Centroid" respectLayerSymbol="1" zscale="1" extrusionEnabled="0" clamping="Terrain" zoffset="0" showMarkerSymbolInSurfacePlots="0" symbology="Line" extrusion="0" type="IndividualFeatures">
    <data-defined-properties>
      <Option type="Map">
        <Option name="name" value="" type="QString" />
        <Option name="properties" />
        <Option name="type" value="collection" type="QString" />
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" id="{d49e14b4-c035-4145-afe3-81e04ad42d18}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="align_dash_pattern" value="0" type="QString" />
            <Option name="capstyle" value="square" type="QString" />
            <Option name="customdash" value="5;2" type="QString" />
            <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="customdash_unit" value="MM" type="QString" />
            <Option name="dash_pattern_offset" value="0" type="QString" />
            <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
            <Option name="draw_inside_polygon" value="0" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="line_color" value="114,155,111,255" type="QString" />
            <Option name="line_style" value="solid" type="QString" />
            <Option name="line_width" value="0.6" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="ring_filter" value="0" type="QString" />
            <Option name="trim_distance_end" value="0" type="QString" />
            <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="trim_distance_end_unit" value="MM" type="QString" />
            <Option name="trim_distance_start" value="0" type="QString" />
            <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="trim_distance_start_unit" value="MM" type="QString" />
            <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
            <Option name="use_custom_dash" value="0" type="QString" />
            <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{3171aa77-ed2e-4feb-b0f3-5d3434c370b9}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="114,155,111,255" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="81,111,79,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.2" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{346c5900-f23c-44f5-9b39-87b88cf2b9b1}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="QString" />
            <Option name="cap_style" value="square" type="QString" />
            <Option name="color" value="114,155,111,255" type="QString" />
            <Option name="horizontal_anchor_point" value="1" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="name" value="diamond" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="81,111,79,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.2" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="scale_method" value="diameter" type="QString" />
            <Option name="size" value="3" type="QString" />
            <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="size_unit" value="MM" type="QString" />
            <Option name="vertical_anchor_point" value="1" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 symbollevels="0" referencescale="-1" forceraster="0" enableorderby="0" type="RuleRenderer">
    <rules key="{1050e838-30f5-45f1-aa71-74b925095554}">
      <rule key="{57ab379e-5588-48ef-a54f-f9cc8215962a}" symbol="0" filter="&quot;radiometry_class&quot; = 0" label="-999.0" checkstate="1">
        <rule key="{77f0cb08-ff67-4dc1-9ef5-9d27fb2fe5dd}" label="&quot;lithology&quot; = 'Argile'" symbol="1" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{5560e510-291e-4306-9ee7-9e07fb51951a}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="2" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{5d3fc2e2-7302-4fad-832a-c2e1e1c18a8d}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="3" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{6d1039ca-ba19-4ac8-b959-3a612507891d}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="4" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{8ebe86e5-403f-4b2a-9596-3fb6a9d114cf}" label="&quot;lithology&quot; = 'ND'" symbol="5" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{c793b67f-ad19-4c6b-a604-ae1f1bafbb54}" symbol="6" filter="&quot;radiometry_class&quot; = 1" label="0.0 - 0.7" checkstate="1">
        <rule key="{5e4db4d1-9c6a-4307-a550-51b3fb84f4a4}" label="&quot;lithology&quot; = 'Argile'" symbol="7" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{874a8c16-1e24-4260-a8ab-f85cd7a763b4}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="8" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{3f0910f3-e7a7-4218-8503-8606750319f6}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="9" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{dee133e5-b7f8-4947-9a93-8087adbd7984}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="10" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{d7d32f01-8c5b-4e6d-82f4-9b155d8766dd}" label="&quot;lithology&quot; = 'ND'" symbol="11" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{d7f97b10-07aa-4f39-9ddc-e2020bfd0e83}" symbol="12" filter="&quot;radiometry_class&quot; = 2" label="0.7 - 1.3" checkstate="1">
        <rule key="{96df7a9c-352f-4d69-9a48-bc11552be8da}" label="&quot;lithology&quot; = 'Argile'" symbol="13" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{c0f02776-3e51-4d45-a56c-5d636fef94b8}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="14" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{a2fcf81b-4a10-4a22-897f-7e92c96c9a92}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="15" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{48691f1d-2d59-450d-91e6-192bf97b3d33}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="16" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{d89d967a-6b89-4904-9bbd-12a7216df68a}" label="&quot;lithology&quot; = 'ND'" symbol="17" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{7c827356-a09e-432d-ad2e-a18bb5893aaf}" symbol="18" filter="&quot;radiometry_class&quot; = 3" label="1.3 - 1.4" checkstate="1">
        <rule key="{7fb71bfc-7ffb-439e-acac-7a5d50ee691c}" label="&quot;lithology&quot; = 'Argile'" symbol="19" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{49ced5e3-7e73-439b-8308-bc2bdb3093d3}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="20" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{57c5a210-48ed-40e8-bb9d-e32717d72195}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="21" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{65e029c2-38cc-4817-89e2-c2c99a38c923}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="22" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{515d30ab-529d-4002-8caf-b9317b6ac936}" label="&quot;lithology&quot; = 'ND'" symbol="23" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{c1f48dc9-91cb-40bf-ac96-6b5eddf71793}" symbol="24" filter="&quot;radiometry_class&quot; = 4" label="1.4 - 1.8" checkstate="1">
        <rule key="{9cc6d36f-e01e-4075-bb2a-31c50463553f}" label="&quot;lithology&quot; = 'Argile'" symbol="25" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{c3265fec-1bcf-4d50-8b84-49e35b71cb9e}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="26" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{e89657b8-d153-44a2-b735-fe110bdf6784}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="27" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{c02bd718-ac2e-4611-9ef9-7c0889393306}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="28" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{ace5e880-fe1f-486b-9b95-bde83ab6e1b4}" label="&quot;lithology&quot; = 'ND'" symbol="29" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{fa511a8b-ed1f-4f25-9e42-bfd53c512dbb}" symbol="30" filter="&quot;radiometry_class&quot; = 5" label="1.8 - 2.5" checkstate="1">
        <rule key="{22381f3e-cbb6-46dd-8af3-2c314324b239}" label="&quot;lithology&quot; = 'Argile'" symbol="31" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{9e7a85b8-e03a-43b4-b231-bddee0ee4c07}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="32" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{1a5a747b-68dc-4371-bda5-3b5489645f61}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="33" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{f15cdd81-3ef5-48a9-8819-113ab381f641}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="34" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{ddfe4c84-2d91-4380-b225-4ca7c54f2ad0}" label="&quot;lithology&quot; = 'ND'" symbol="35" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
      <rule key="{af35a396-0a46-4043-8dac-672a75d65386}" symbol="36" filter="&quot;radiometry_class&quot; = 6" label="+2.5" checkstate="1">
        <rule key="{83eb5e6b-c1f1-44d7-87f3-2ba05b75a399}" label="&quot;lithology&quot; = 'Argile'" symbol="37" filter="&quot;lithology&quot; = 'Argile'" />
        <rule key="{014c107d-b049-42a8-87f3-15c7feab8778}" label="&quot;lithology&quot; = 'Argile Oxyd&#233;'" symbol="38" filter="&quot;lithology&quot; = 'Argile Oxyd&#233;'" />
        <rule key="{f700b890-4104-4811-b68d-f5ff99aec3ea}" label="&quot;lithology&quot; = 'Gr&#232;s'" symbol="39" filter="&quot;lithology&quot; = 'Gr&#232;s'" />
        <rule key="{007c1b8b-cfb3-4cc9-99ad-9359e1449eab}" label="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" symbol="40" filter="&quot;lithology&quot; = 'Gr&#232;s Oxyd&#233;'" />
        <rule key="{b050c8e8-f69f-473f-9ca6-58a548cea36d}" label="&quot;lithology&quot; = 'ND'" symbol="41" filter="&quot;lithology&quot; = 'ND'" />
      </rule>
    </rules>
    <symbols>
      <symbol clip_to_extent="1" name="0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{5e16c15c-7dc0-4eb9-8d8e-44230f153295}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#FFFFFF" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="114,114,114,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="1" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{ab010a0b-c5b7-4203-912e-2c2ee8d7650a}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@1@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{f7fead46-7e89-4b1a-bc74-017d91e5bde9}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="10" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{f328a4f4-fa68-4ae3-9096-ff62e98472c8}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="277676565" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@10@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{82f95a23-62d0-4f4e-8fe5-74a859dd201f}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="11" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{2b6b4d85-627e-4056-9e4c-46c1fd924e77}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@11@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{6a75823a-af71-47c2-a82b-2f86c69fb5ca}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{c1ec0148-f3c3-4527-a6c7-271bb19605b8}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="12" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{b486247e-29c2-403a-9a8d-e6e87119b668}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#00FF00" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="13" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{6b78f464-5cac-4a4d-8e74-77f9e501f370}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@13@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{4bdeb5a4-4da5-488d-9ece-e81b0f290282}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="14" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{f445b2aa-5cec-480b-80ff-1f4ff0671446}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@14@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{12e1b5aa-4126-4393-8d0a-e5eba85991f7}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="15" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{584d0f6e-efa4-4450-99ad-20f560fad577}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="600726863" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@15@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{896ea170-b11c-4db1-84d7-5e7c0b373313}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="16" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{280da159-e33b-41e4-86e3-1ed2446c16bf}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="506315836" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@16@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{0c056f90-bf97-46d0-99e8-31f0be754488}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="17" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{18745417-f5da-4c26-8920-6c648eaa870c}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@17@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{b30a1d0d-1292-498b-bb9c-49d42839c861}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{9bbcc7f6-8a13-4f3c-bf56-fa653b507b59}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="18" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{e775ef3c-6720-4130-88ec-c95b548be945}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#FFE600" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="19" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{64649027-8c85-4523-abaf-10e3b30ea525}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@19@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{a23df61e-8ae1-42f9-a747-6d61d5358e28}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="2" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{d9eab02e-fe72-45ff-ad15-393ed5931020}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@2@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{7c5f5c24-4540-4218-a181-d081ddee1800}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="20" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{c331ceec-e298-4a5e-b1e0-c94e31d22548}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@20@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{fd7342d1-aec2-4c9b-ae8b-372e1c2059ec}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="21" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{9374e44f-9093-463f-93cc-6d828c7dacd0}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="935045882" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@21@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{47eef07a-5c1e-4cf9-8821-9443c7aec60e}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="22" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{f59d274a-8d97-410d-b59f-3f088993bc7c}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="630480019" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@22@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{90694a58-9740-40ec-a657-99cc4f4dfab1}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="23" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{bf8e7e6d-1865-49dd-b214-49b3c0b5cf68}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@23@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{13c40e0a-4dde-4fa6-a04d-6861634ebdfc}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{8f56d22c-2455-407b-8770-8aca5301def8}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="24" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{5845871b-f336-480f-a5e9-7f209c270daf}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#FF0000" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="25" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{627bf7b7-34ff-4150-aec8-c920f2239c52}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@25@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{aa668825-9dbe-45d8-a50c-de89bc6618d3}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="26" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{0c32926c-0819-4ccb-a2cb-0edc30fbdb38}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@26@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{5dccf91e-5103-4a3b-900e-f2179a2f5ad5}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="27" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{53559190-972a-4a5b-8716-122c90bc0d8a}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="157970429" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@27@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{d1de4f41-93ee-4370-b1b0-5f7c0f547bf1}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="28" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{d8a396a0-871d-4abe-83ef-64f853d567f5}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="727645427" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@28@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{6cdb5c29-58b0-4170-b14a-f193c11a2bfd}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="29" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{b40a7a2f-899b-4976-b2a1-599603c8b3b9}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@29@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{e1e07ac7-b894-4adf-b80b-eba9ef3354f8}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{444f0582-5c89-490c-bed8-8a6fd9689940}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="3" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{fa4c6a6d-688d-4be5-a350-fe3790add3ce}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="297620758" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@3@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{d40b3470-b164-4794-a4fd-35c7a241b38c}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="30" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{536ab1e2-f05d-40e1-a72f-620bcda9dbaf}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#0073FF" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="31" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{70480edb-b374-4bf2-ba82-b4b6a7ccc486}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@31@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{2ff77681-e291-407a-bd99-7ff7ea84905f}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="32" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{a613d3bc-8f29-4be7-ad1f-05791ef21de8}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@32@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{a676bd31-4070-461b-b300-2f28ad24c18c}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="33" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{aa5dd75d-91a3-420b-9fec-7149492772ba}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="522602639" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@33@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{1f9519f1-0a9f-4f47-bc43-63e78b2a6c82}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="34" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{83c1f6df-000a-43f9-90c2-07e86e668645}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="788876849" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@34@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{ef3c1eb9-47cc-4219-be47-02007d42d160}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="35" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{5bb771da-a6ea-41dc-ae68-e349939275b8}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@35@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{a79be96b-abc6-47b7-8bc8-2046818b8d30}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{6130e642-99af-4bb5-8e8c-ff65036984c4}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="36" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{cf520439-f0d3-4a31-8e17-eb0c2e2e8e09}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#A800FF" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="37" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{58993299-ff2a-41a5-b517-1025289c3bab}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@37@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{9f0f2040-14b6-43b0-8b2e-a3f39fb9235b}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="38" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{ba38a32d-f011-4cab-924c-ceb789e2164a}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@38@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{a175db79-dc4d-4edf-8b15-0b70a9f8f37d}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="39" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{c41a530c-1edb-453f-83b4-0d84bd70eae8}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="288173135" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@39@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{01ab8f97-6869-46e9-8961-21a86d5d38d0}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="4" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{7aa92bfb-1488-4c7b-b906-d1c1df9a24f4}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="277676565" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@4@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{62c348e1-d8b7-477e-8798-8fe0d767faa2}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="40" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{53f43be8-c04d-402d-8878-1ca28b6d1d28}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="537380890" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@40@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{9976230d-052f-4b73-ac38-0ebe29886f75}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="137,137,137,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="226,137,75,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="41" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{512bea67-1b70-4916-a983-825f532de0f5}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@41@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{0afeb02d-cb69-45ae-964e-fef8746158a1}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{2fa03a90-bf1f-496e-9ac8-359f96260b5b}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="5" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="CentroidFill" pass="0" id="{8369cb78-104b-4a00-a93c-167f26f7d69d}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="clip_on_current_part_only" value="0" type="QString" />
            <Option name="clip_points" value="0" type="QString" />
            <Option name="point_on_all_parts" value="1" type="QString" />
            <Option name="point_on_surface" value="0" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@5@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{ac9469fa-a18f-4960-be6c-6c61ababfac3}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.2" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="3.2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
            <layer class="SimpleMarker" pass="0" id="{f8cebf25-62bb-48b7-bf5a-eec128837d82}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="0,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="0,0,0,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0.4" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="0.7" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="6" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{b3fd4448-ebb0-4894-911e-de36ff6de882}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="#B2B2B2" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="114,114,114,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="7" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{322753ec-cd21-4dac-8393-2fa38b2ea348}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="137,137,137,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@7@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{e2650c0e-5e2f-4ce9-ba9d-43a4d1411cab}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="137,137,137,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="8" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="LinePatternFill" pass="0" id="{e742210d-b3b0-4601-83d0-486838c8c408}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="45" type="QString" />
            <Option name="clip_mode" value="during_render" type="QString" />
            <Option name="color" value="226,137,75,255" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="distance" value="5" type="QString" />
            <Option name="distance_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_unit" value="MM" type="QString" />
            <Option name="line_width" value="0.5" type="QString" />
            <Option name="line_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="line_width_unit" value="MM" type="QString" />
            <Option name="offset" value="0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@8@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleLine" pass="0" id="{ebda017f-6a37-47fe-8499-59f6c3acc19d}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="align_dash_pattern" value="0" type="QString" />
                <Option name="capstyle" value="square" type="QString" />
                <Option name="customdash" value="5;2" type="QString" />
                <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="customdash_unit" value="MM" type="QString" />
                <Option name="dash_pattern_offset" value="0" type="QString" />
                <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
                <Option name="draw_inside_polygon" value="0" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="line_color" value="226,137,75,255" type="QString" />
                <Option name="line_style" value="solid" type="QString" />
                <Option name="line_width" value="0.5" type="QString" />
                <Option name="line_width_unit" value="MM" type="QString" />
                <Option name="offset" value="0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="ring_filter" value="0" type="QString" />
                <Option name="trim_distance_end" value="0" type="QString" />
                <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_end_unit" value="MM" type="QString" />
                <Option name="trim_distance_start" value="0" type="QString" />
                <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="trim_distance_start_unit" value="MM" type="QString" />
                <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
                <Option name="use_custom_dash" value="0" type="QString" />
                <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="9" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="PointPatternFill" pass="0" id="{782f6eca-fa62-4bbe-98fa-cee602448093}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="angle" value="0" type="double" />
            <Option name="clip_mode" value="shape" type="QString" />
            <Option name="coordinate_reference" value="feature" type="QString" />
            <Option name="displacement_x" value="1" type="QString" />
            <Option name="displacement_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_x_unit" value="MM" type="QString" />
            <Option name="displacement_y" value="0" type="QString" />
            <Option name="displacement_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="displacement_y_unit" value="MM" type="QString" />
            <Option name="distance_x" value="2" type="QString" />
            <Option name="distance_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_x_unit" value="MM" type="QString" />
            <Option name="distance_y" value="2" type="QString" />
            <Option name="distance_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="distance_y_unit" value="MM" type="QString" />
            <Option name="offset_x" value="0" type="QString" />
            <Option name="offset_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_x_unit" value="MM" type="QString" />
            <Option name="offset_y" value="0" type="QString" />
            <Option name="offset_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_y_unit" value="MM" type="QString" />
            <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="random_deviation_x" value="0" type="QString" />
            <Option name="random_deviation_x_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_x_unit" value="MM" type="QString" />
            <Option name="random_deviation_y" value="0" type="QString" />
            <Option name="random_deviation_y_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="random_deviation_y_unit" value="MM" type="QString" />
            <Option name="seed" value="297620758" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" name="@9@0" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="{b2bfef06-b54d-4788-a9f6-2dd2df5c87d9}" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="255,0,0,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="line" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="137,137,137,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="area" type="QString" />
                <Option name="size" value="1" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <selection mode="Default">
    <selectionColor invalid="1" />
    <selectionSymbol>
      <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{aa188575-a712-430f-a05c-3ecdc5417c12}" locked="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="color" value="0,0,255,255" type="QString" />
            <Option name="joinstyle" value="bevel" type="QString" />
            <Option name="offset" value="0,0" type="QString" />
            <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            <Option name="offset_unit" value="MM" type="QString" />
            <Option name="outline_color" value="35,35,35,255" type="QString" />
            <Option name="outline_style" value="solid" type="QString" />
            <Option name="outline_width" value="0.26" type="QString" />
            <Option name="outline_width_unit" value="MM" type="QString" />
            <Option name="style" value="solid" type="QString" />
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </selectionSymbol>
  </selection>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style allowHtml="0" textColor="0,0,0,255" fontFamily="DejaVu Sans" fontWeight="50" multilineHeightUnit="Percentage" fontSizeMapUnitScale="3x:0,0,0,0,0,0" forcedItalic="0" previewBkgrdColor="255,255,255,255" blendMode="0" fieldName="affectedradio" fontLetterSpacing="0" fontItalic="0" fontSizeUnit="Point" fontKerning="1" legendString="Aa" fontStrikeout="0" textOpacity="1" multilineHeight="1" fontUnderline="0" useSubstitutions="0" fontWordSpacing="0" capitalization="0" forcedBold="0" textOrientation="horizontal" namedStyle="Book" fontSize="10" isExpression="0">
        <families />
        <text-buffer bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferBlendMode="0" bufferNoFill="1" bufferOpacity="1" bufferSize="1" bufferJoinStyle="128" bufferColor="255,255,255,255" bufferDraw="1" bufferSizeUnits="MM" />
        <text-mask maskOpacity="1" maskedSymbolLayers="" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskType="0" maskSize="1.5" maskJoinStyle="128" maskEnabled="0" maskSizeUnits="MM" maskSize2="1.5" />
        <background shapeBorderWidthUnit="MM" shapeSizeX="0" shapeBorderWidth="0" shapeSizeType="0" shapeSizeUnit="MM" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeOffsetUnit="MM" shapeJoinStyle="64" shapeRadiiX="0" shapeRadiiY="0" shapeRotation="0" shapeBlendMode="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeOpacity="1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderColor="128,128,128,255" shapeType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSVGFile="" shapeRotationType="0" shapeOffsetX="0" shapeDraw="0">
          <symbol clip_to_extent="1" name="markerSymbol" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" pass="0" id="" locked="0" enabled="1">
              <Option type="Map">
                <Option name="angle" value="0" type="QString" />
                <Option name="cap_style" value="square" type="QString" />
                <Option name="color" value="243,166,178,255" type="QString" />
                <Option name="horizontal_anchor_point" value="1" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="name" value="circle" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="35,35,35,255" type="QString" />
                <Option name="outline_style" value="solid" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="scale_method" value="diameter" type="QString" />
                <Option name="size" value="2" type="QString" />
                <Option name="size_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="size_unit" value="MM" type="QString" />
                <Option name="vertical_anchor_point" value="1" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol clip_to_extent="1" name="fillSymbol" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="fill">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
            <layer class="SimpleFill" pass="0" id="" locked="0" enabled="1">
              <Option type="Map">
                <Option name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="color" value="255,255,255,255" type="QString" />
                <Option name="joinstyle" value="bevel" type="QString" />
                <Option name="offset" value="0,0" type="QString" />
                <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
                <Option name="offset_unit" value="MM" type="QString" />
                <Option name="outline_color" value="128,128,128,255" type="QString" />
                <Option name="outline_style" value="no" type="QString" />
                <Option name="outline_width" value="0" type="QString" />
                <Option name="outline_width_unit" value="MM" type="QString" />
                <Option name="style" value="solid" type="QString" />
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString" />
                  <Option name="properties" />
                  <Option name="type" value="collection" type="QString" />
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowBlendMode="6" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowScale="100" shadowOffsetGlobal="1" shadowRadiusAlphaOnly="0" shadowRadiusUnit="MM" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOpacity="0.69999999999999996" shadowDraw="0" shadowColor="0,0,0,255" shadowUnder="0" shadowOffsetUnit="MM" shadowOffsetDist="1" shadowOffsetAngle="135" shadowRadius="1.5" />
        <dd_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
        </dd_properties>
        <substitutions />
      </text-style>
      <text-format wrapChar="-" multilineAlign="0" reverseDirectionSymbol="0" decimals="3" placeDirectionSymbol="0" autoWrapLength="0" rightDirectionSymbol="&gt;" useMaxLineLengthForAutoWrap="1" plussign="0" leftDirectionSymbol="&lt;" formatNumbers="0" addDirectionSymbol="0" />
      <placement centroidWhole="1" offsetType="0" dist="0" maxCurvedCharAngleIn="25" lineAnchorType="0" overlapHandling="PreventOverlap" geometryGeneratorType="PointGeometry" distMapUnitScale="3x:0,0,0,0,0,0" quadOffset="4" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" offsetUnits="MM" polygonPlacementFlags="2" centroidInside="0" maxCurvedCharAngleOut="-25" preserveRotation="1" priority="5" geometryGeneratorEnabled="0" placement="1" xOffset="0" lineAnchorTextPoint="CenterOfText" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0" lineAnchorClipping="0" fitInPolygonOnly="0" distUnits="MM" overrunDistance="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" yOffset="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" repeatDistanceUnits="MM" layerType="PolygonGeometry" rotationUnit="AngleDegrees" allowDegraded="0" geometryGenerator="" rotationAngle="0" lineAnchorPercent="0.5" placementFlags="10" overrunDistanceUnit="MM" />
      <rendering obstacleFactor="1" fontMaxPixelSize="10000" obstacle="1" drawLabels="1" unplacedVisibility="0" limitNumLabels="0" maxNumLabels="2000" labelPerPart="0" scaleMin="0" fontMinPixelSize="3" scaleVisibility="0" upsidedownLabels="0" mergeLines="0" zIndex="0" minFeatureSize="0" obstacleType="0" scaleMax="0" fontLimitPixelSize="0" />
      <dd_properties>
        <Option type="Map">
          <Option name="name" value="" type="QString" />
          <Option name="properties" />
          <Option name="type" value="collection" type="QString" />
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option name="anchorPoint" value="pole_of_inaccessibility" type="QString" />
          <Option name="blendMode" value="0" type="int" />
          <Option name="ddProperties" type="Map">
            <Option name="name" value="" type="QString" />
            <Option name="properties" />
            <Option name="type" value="collection" type="QString" />
          </Option>
          <Option name="drawToAllParts" value="false" type="bool" />
          <Option name="enabled" value="0" type="QString" />
          <Option name="labelAnchorPoint" value="point_on_exterior" type="QString" />
          <Option name="lineSymbol" value="&lt;symbol clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; force_rhr=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;line&quot;&gt;&lt;data_defined_properties&gt;&lt;Option type=&quot;Map&quot;&gt;&lt;Option name=&quot;name&quot; value=&quot;&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;properties&quot;/&gt;&lt;Option name=&quot;type&quot; value=&quot;collection&quot; type=&quot;QString&quot;/&gt;&lt;/Option&gt;&lt;/data_defined_properties&gt;&lt;layer class=&quot;SimpleLine&quot; pass=&quot;0&quot; id=&quot;{0a157963-3acf-437c-adcf-46f5686c4b69}&quot; locked=&quot;0&quot; enabled=&quot;1&quot;&gt;&lt;Option type=&quot;Map&quot;&gt;&lt;Option name=&quot;align_dash_pattern&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;capstyle&quot; value=&quot;square&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;customdash&quot; value=&quot;5;2&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;customdash_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;customdash_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;dash_pattern_offset&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;dash_pattern_offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;dash_pattern_offset_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;draw_inside_polygon&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;joinstyle&quot; value=&quot;bevel&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;line_color&quot; value=&quot;60,60,60,255&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;line_style&quot; value=&quot;solid&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;line_width&quot; value=&quot;0.3&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;line_width_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;offset&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;offset_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;ring_filter&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_end&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_end_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_end_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_start&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_start_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;trim_distance_start_unit&quot; value=&quot;MM&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;tweak_dash_pattern_on_corners&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;use_custom_dash&quot; value=&quot;0&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot;/&gt;&lt;/Option&gt;&lt;data_defined_properties&gt;&lt;Option type=&quot;Map&quot;&gt;&lt;Option name=&quot;name&quot; value=&quot;&quot; type=&quot;QString&quot;/&gt;&lt;Option name=&quot;properties&quot;/&gt;&lt;Option name=&quot;type&quot; value=&quot;collection&quot; type=&quot;QString&quot;/&gt;&lt;/Option&gt;&lt;/data_defined_properties&gt;&lt;/layer&gt;&lt;/symbol&gt;" type="QString" />
          <Option name="minLength" value="0" type="double" />
          <Option name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0" type="QString" />
          <Option name="minLengthUnit" value="MM" type="QString" />
          <Option name="offsetFromAnchor" value="0" type="double" />
          <Option name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0" type="QString" />
          <Option name="offsetFromAnchorUnit" value="MM" type="QString" />
          <Option name="offsetFromLabel" value="0" type="double" />
          <Option name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0" type="QString" />
          <Option name="offsetFromLabelUnit" value="MM" type="QString" />
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option name="embeddedWidgets/count" value="0" type="QString" />
      <Option name="variableNames" />
      <Option name="variableValues" />
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <LinearlyInterpolatedDiagramRenderer lowerValue="0" lowerHeight="0" attributeLegend="1" classificationAttributeExpression="" lowerWidth="0" upperValue="0" diagramType="Histogram" upperWidth="50" upperHeight="50">
    <DiagramCategory barWidth="5" labelPlacementMethod="XHeight" width="15" lineSizeType="MM" minimumSize="0" penWidth="0" sizeScale="3x:0,0,0,0,0,0" backgroundColor="#ffffff" scaleBasedVisibility="0" direction="1" minScaleDenominator="-4.65661e-10" sizeType="MM" penAlpha="255" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" scaleDependency="Area" penColor="#000000" opacity="1" spacingUnit="MM" spacingUnitScale="3x:0,0,0,0,0,0" rotationOffset="270" showAxis="0" diagramOrientation="Up" height="15" maxScaleDenominator="1e+08" enabled="0" spacing="0">
      <fontProperties italic="0" style="" bold="0" underline="0" description="Ubuntu,9,-1,5,50,0,0,0,0,0" strikethrough="0" />
      <attribute colorOpacity="1" field="" label="" color="#000000" />
      <axisSymbol>
        <symbol clip_to_extent="1" name="" is_animated="0" alpha="1" force_rhr="0" frame_rate="10" type="line">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString" />
              <Option name="properties" />
              <Option name="type" value="collection" type="QString" />
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" pass="0" id="{54e3fe37-3e71-41f1-991c-c239732fb100}" locked="0" enabled="1">
            <Option type="Map">
              <Option name="align_dash_pattern" value="0" type="QString" />
              <Option name="capstyle" value="square" type="QString" />
              <Option name="customdash" value="5;2" type="QString" />
              <Option name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="customdash_unit" value="MM" type="QString" />
              <Option name="dash_pattern_offset" value="0" type="QString" />
              <Option name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="dash_pattern_offset_unit" value="MM" type="QString" />
              <Option name="draw_inside_polygon" value="0" type="QString" />
              <Option name="joinstyle" value="bevel" type="QString" />
              <Option name="line_color" value="35,35,35,255" type="QString" />
              <Option name="line_style" value="solid" type="QString" />
              <Option name="line_width" value="0.26" type="QString" />
              <Option name="line_width_unit" value="MM" type="QString" />
              <Option name="offset" value="0" type="QString" />
              <Option name="offset_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="offset_unit" value="MM" type="QString" />
              <Option name="ring_filter" value="0" type="QString" />
              <Option name="trim_distance_end" value="0" type="QString" />
              <Option name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="trim_distance_end_unit" value="MM" type="QString" />
              <Option name="trim_distance_start" value="0" type="QString" />
              <Option name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
              <Option name="trim_distance_start_unit" value="MM" type="QString" />
              <Option name="tweak_dash_pattern_on_corners" value="0" type="QString" />
              <Option name="use_custom_dash" value="0" type="QString" />
              <Option name="width_map_unit_scale" value="3x:0,0,0,0,0,0" type="QString" />
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" value="" type="QString" />
                <Option name="properties" />
                <Option name="type" value="collection" type="QString" />
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </LinearlyInterpolatedDiagramRenderer>
  <DiagramLayerSettings dist="0" showAll="1" obstacle="0" priority="0" zIndex="0" placement="0" linePlacementFlags="2">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString" />
        <Option name="properties" />
        <Option name="type" value="collection" type="QString" />
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks />
    <checkConfiguration type="Map">
      <Option name="QgsGeometryGapCheck" type="Map">
        <Option name="allowedGapsBuffer" value="0" type="double" />
        <Option name="allowedGapsEnabled" value="false" type="bool" />
        <Option name="allowedGapsLayer" value="" type="QString" />
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector" />
  <referencedLayers />
  <fieldConfiguration>
    <field name="id" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="smu" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="load" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="deposit" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="panel" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="blast" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="radiometry" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="radiometry_probe" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lithology" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="unit" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="centroid_z" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="0" type="QString" />
            <Option name="UseHtml" value="0" type="QString" />
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="affectedradio" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
    <field name="radiometry_class" configurationFlags="NoFlag">
      <editWidget type="Range">
        <config>
          <Option />
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="id" index="0" />
    <alias name="" field="smu" index="1" />
    <alias name="" field="load" index="2" />
    <alias name="" field="deposit" index="3" />
    <alias name="" field="panel" index="4" />
    <alias name="" field="blast" index="5" />
    <alias name="" field="radiometry" index="6" />
    <alias name="" field="radiometry_probe" index="7" />
    <alias name="" field="lithology" index="8" />
    <alias name="" field="unit" index="9" />
    <alias name="" field="centroid_z" index="10" />
    <alias name="" field="affectedradio" index="11" />
    <alias name="" field="radiometry_class" index="12" />
  </aliases>
  <splitPolicies>
    <policy policy="Duplicate" field="id" />
    <policy policy="Duplicate" field="smu" />
    <policy policy="Duplicate" field="load" />
    <policy policy="Duplicate" field="deposit" />
    <policy policy="Duplicate" field="panel" />
    <policy policy="Duplicate" field="blast" />
    <policy policy="Duplicate" field="radiometry" />
    <policy policy="Duplicate" field="radiometry_probe" />
    <policy policy="Duplicate" field="lithology" />
    <policy policy="Duplicate" field="unit" />
    <policy policy="Duplicate" field="centroid_z" />
    <policy policy="Duplicate" field="affectedradio" />
    <policy policy="Duplicate" field="radiometry_class" />
  </splitPolicies>
  <defaults>
    <default applyOnUpdate="0" field="id" expression="" />
    <default applyOnUpdate="0" field="smu" expression="" />
    <default applyOnUpdate="0" field="load" expression="" />
    <default applyOnUpdate="0" field="deposit" expression="" />
    <default applyOnUpdate="0" field="panel" expression="" />
    <default applyOnUpdate="0" field="blast" expression="" />
    <default applyOnUpdate="0" field="radiometry" expression="" />
    <default applyOnUpdate="0" field="radiometry_probe" expression="" />
    <default applyOnUpdate="0" field="lithology" expression="" />
    <default applyOnUpdate="0" field="unit" expression="" />
    <default applyOnUpdate="0" field="centroid_z" expression="" />
    <default applyOnUpdate="0" field="affectedradio" expression="" />
    <default applyOnUpdate="0" field="radiometry_class" expression="" />
  </defaults>
  <constraints>
    <constraint notnull_strength="1" exp_strength="0" field="id" unique_strength="1" constraints="3" />
    <constraint notnull_strength="0" exp_strength="0" field="smu" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="load" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="deposit" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="panel" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="blast" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="radiometry" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="radiometry_probe" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="lithology" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="unit" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="centroid_z" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="affectedradio" unique_strength="0" constraints="0" />
    <constraint notnull_strength="0" exp_strength="0" field="radiometry_class" unique_strength="0" constraints="0" />
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp="" />
    <constraint desc="" field="smu" exp="" />
    <constraint desc="" field="load" exp="" />
    <constraint desc="" field="deposit" exp="" />
    <constraint desc="" field="panel" exp="" />
    <constraint desc="" field="blast" exp="" />
    <constraint desc="" field="radiometry" exp="" />
    <constraint desc="" field="radiometry_probe" exp="" />
    <constraint desc="" field="lithology" exp="" />
    <constraint desc="" field="unit" exp="" />
    <constraint desc="" field="centroid_z" exp="" />
    <constraint desc="" field="affectedradio" exp="" />
    <constraint desc="" field="radiometry_class" exp="" />
  </constraintExpressions>
  <expressionfields />
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}" />
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="" sortOrder="0">
    <columns>
      <column name="id" width="-1" hidden="0" type="field" />
      <column name="smu" width="-1" hidden="0" type="field" />
      <column name="load" width="-1" hidden="0" type="field" />
      <column name="deposit" width="-1" hidden="0" type="field" />
      <column name="panel" width="-1" hidden="0" type="field" />
      <column name="blast" width="-1" hidden="0" type="field" />
      <column name="radiometry" width="-1" hidden="0" type="field" />
      <column name="radiometry_probe" width="-1" hidden="0" type="field" />
      <column name="lithology" width="-1" hidden="0" type="field" />
      <column name="unit" width="-1" hidden="0" type="field" />
      <column name="centroid_z" width="-1" hidden="0" type="field" />
      <column width="-1" hidden="1" type="actions" />
      <column name="radiometry_class" width="-1" hidden="0" type="field" />
      <column name="affectedradio" width="-1" hidden="0" type="field" />
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles />
    <fieldstyles />
  </conditionalstyles>
  <storedexpressions />
  <editform tolerant="1" />
  <editforminit />
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>/../.qgis2/python/docs/affaires/areva</editforminitfilepath>
  <editforminitcode># -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appel&#233;e &#224; l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalit&#233;s &#224; vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple &#224; suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

</editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="affectedradio" editable="1" />
    <field name="blast" editable="1" />
    <field name="centroid_z" editable="1" />
    <field name="deposit" editable="1" />
    <field name="id" editable="1" />
    <field name="lithology" editable="1" />
    <field name="load" editable="1" />
    <field name="panel" editable="1" />
    <field name="radiometry" editable="1" />
    <field name="radiometry_class" editable="1" />
    <field name="radiometry_probe" editable="1" />
    <field name="smu" editable="1" />
    <field name="unit" editable="1" />
  </editable>
  <labelOnTop>
    <field name="affectedradio" labelOnTop="0" />
    <field name="blast" labelOnTop="0" />
    <field name="centroid_z" labelOnTop="0" />
    <field name="deposit" labelOnTop="0" />
    <field name="id" labelOnTop="0" />
    <field name="lithology" labelOnTop="0" />
    <field name="load" labelOnTop="0" />
    <field name="panel" labelOnTop="0" />
    <field name="radiometry" labelOnTop="0" />
    <field name="radiometry_class" labelOnTop="0" />
    <field name="radiometry_probe" labelOnTop="0" />
    <field name="smu" labelOnTop="0" />
    <field name="unit" labelOnTop="0" />
  </labelOnTop>
  <reuseLastValue>
    <field name="affectedradio" reuseLastValue="0" />
    <field name="blast" reuseLastValue="0" />
    <field name="centroid_z" reuseLastValue="0" />
    <field name="deposit" reuseLastValue="0" />
    <field name="id" reuseLastValue="0" />
    <field name="lithology" reuseLastValue="0" />
    <field name="load" reuseLastValue="0" />
    <field name="panel" reuseLastValue="0" />
    <field name="radiometry" reuseLastValue="0" />
    <field name="radiometry_class" reuseLastValue="0" />
    <field name="radiometry_probe" reuseLastValue="0" />
    <field name="smu" reuseLastValue="0" />
    <field name="unit" reuseLastValue="0" />
  </reuseLastValue>
  <dataDefinedFieldProperties />
  <widgets />
  <previewExpression>COALESCE( "id", '&lt;NULL&gt;' )</previewExpression>
  <mapTip enabled="1" />
  <layerGeometryType>2</layerGeometryType>
</qgis>