# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os
import time

from qgis.gui import QgsMapToolEmitPoint, QgsMapToolPan
from qgis.PyQt import QtWidgets, uic
from qgis.utils import iface

from CtrlCraft.logger import CtrlCraftLogger
from CtrlCraft.utils import misc

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), "ui", "affect_radiometry_slab.ui"))


class CtrlCraftAffectUpdateMapTool(QgsMapToolEmitPoint):  # pylint: disable=too-few-public-methods
    """Map Tool to clic on a slab and affect radiometry (for degraded mode)"""

    def __init__(self, affect, teneurUp, teneurLow, inclination):
        QgsMapToolEmitPoint.__init__(self, affect.mapCanvas)
        self.affect = affect
        self.teneurUp = teneurUp
        self.teneurLow = teneurLow
        self.inclination = inclination

    def canvasReleaseEvent(self, event):
        point = event.mapPoint()

        if self.affect.database.isValid():
            self.affect.database.affectRadiometrySlabDegraded(
                self.teneurUp, self.teneurLow, self.inclination, point.asWkt()
            )
            self.affect.mapCanvas.refreshAllLayers()
            self.affect.logger.logBar(
                self.affect.tr("Up probe: ")
                + "{}\n".format(str(round(self.teneurUp, 2)))
                + self.affect.tr("Down probe: ")
                + "{}".format(str(round(self.teneurLow, 2))),
                duration=5,
            )
        self.affect.mapCanvas.setMapTool(QgsMapToolPan(self.affect.mapCanvas))


class CtrlCraftAffectSonde(QtWidgets.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super().__init__(parent)
        self.setupUi(self)


class CtrlCraftAffectRadiometrySlab:  # pylint: disable=too-few-public-methods
    def __init__(self, rtkubeWidget, mapCanvas, database):
        self.rtkubeWidget = rtkubeWidget
        self.mapCanvas = mapCanvas
        self.database = database
        self.tr = misc.tr
        self.logger = CtrlCraftLogger("Affect Radiometry", iface)

    def exec(self):
        dlg = CtrlCraftAffectSonde()
        dlg.show()
        ret = dlg.exec()
        if not ret:
            return

        self.applyUserChanges(dlg.gammaSonde)

    def applyUserChanges(self, gammaSonde):
        teneurUp = 0
        teneurLow = 0
        inclination = 0
        degraded = all(  # pylint: disable=use-a-generator
            [
                x == "-1"
                for x in [
                    self.rtkubeWidget.GNSSLat.text(),
                    self.rtkubeWidget.GNSSLon.text(),
                    self.rtkubeWidget.GNSSAlt.text(),
                ]
            ]
        )
        before = time.monotonic()
        progress = 0
        self.logger.logBar(self.tr("Acquiring data"), duration=6)
        while progress < 4:
            QtWidgets.QApplication.processEvents()
            now = time.monotonic()
            if progress < round(now - before, 0):
                if degraded:
                    if gammaSonde.currentIndex() == 0:
                        teneurUp += float(self.rtkubeWidget.gammaTUp.text())
                    else:
                        teneurLow += float(self.rtkubeWidget.gammaTLow.text())
                    inclination += float(self.rtkubeWidget.gammaInc.text())
                before = now
                progress += 1

        # Wait more to be sure that all the recent measurements that
        # will be used with the fixed_measure were done on the same location
        time.sleep(1.5)

        if degraded:
            self.logger.logBar(self.tr("You can now point the slab"))
            mapTool = CtrlCraftAffectUpdateMapTool(self, teneurUp / 4.0, teneurLow / 4.0, inclination / 4.0)
            self.mapCanvas.setMapTool(mapTool)
        else:
            self.database.affectRadiometrySlab(gammaSonde.currentIndex() == 0)
            self.mapCanvas.refreshAllLayers()
            radioUp, radioLow = self.database.getAverageLastFourRadiometry()
            if gammaSonde.currentIndex() == 0:
                radioLow = 0
            else:
                radioUp = 0
            self.logger.logBar(
                self.tr("Up probe: ")
                + "{}\n".format(str(round(radioUp or 0, 2)))
                + self.tr("Down probe: ")
                + "{}".format(str(round(radioLow or 0, 2))),
                duration=5,
            )
