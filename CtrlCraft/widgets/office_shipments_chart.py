# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Office shipments chart
"""

import os

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from qgis.core import QgsGeometry
from qgis.PyQt import uic
from qgis.PyQt.QtGui import QIcon, QPixmap
from qgis.PyQt.QtWidgets import (
    QDialog,
    QHBoxLayout,
    QPushButton,
    QScrollArea,
    QWidget,
)

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), "ui", "chart.ui"))


class CtrlCraftOfficeShipmentsChart(QDialog, FORM_CLASS):
    def __init__(self, data, canvas, maxSlabs, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.data = data
        self.canvas = canvas
        self.maxSlabs = maxSlabs
        self.currentIdSlabs = []

    def init(self, deposit, panel, blast):
        # pylint: disable=attribute-defined-outside-init
        shipments = self.data.shipments(deposit, panel, blast)

        # Match the data
        dataChartSlab = []
        dataChartSlabProbe = []
        dataChartPortic = []

        for shipment in shipments:
            unloadTime = shipment[5]

            if not unloadTime:
                continue

            # do not use seconds because the portic datetime is in minutes
            # only
            times = unloadTime.split(":")
            unloadTime = "{}:{}".format(times[0], times[1])

            valSlab = float(shipment[6]) if shipment[6] else 0.0
            valSlabProbe = float(shipment[7]) if shipment[7] else 0.0
            valIdSlabs = shipment[8]

            dataChartSlab.append(valSlab)
            dataChartSlabProbe.append(valSlabProbe)
            dataChartPortic.append(0)  # supposed to be gate data but gate management is obsolete
            self.currentIdSlabs.append(valIdSlabs)

        # Build chart
        fig, ax = plt.subplots()
        ax.scatter(dataChartPortic, dataChartSlabProbe, c="b", s=30, alpha=0.5, picker=5)
        ax.scatter(dataChartPortic, dataChartSlab, c="r", s=30, alpha=0.5, picker=5)

        ax.set_xlabel(self.tr("Gate grade"))
        ax.set_ylabel(self.tr("Slabs grade (estimated: red, probe: blue)"))
        ax.set_title(self.tr("Correlation between probe and gate"))

        ax.grid(True)
        fig.tight_layout()

        chartCanvas = FigureCanvas(fig)
        chartCanvas.mpl_connect("pick_event", self.onClick)

        toolbar = NavigationToolbar(chartCanvas, self)

        self.vLayout.addWidget(toolbar)
        self.vLayout.addWidget(chartCanvas)

        iconName = "iconSlab_small.png"
        path = os.path.join(os.path.dirname(__file__), "img", iconName)
        pix = QPixmap(path)
        icon = QIcon(pix)

        self.scrollArea = QScrollArea()
        self.buttons = QHBoxLayout()
        for i in range(0, self.maxSlabs):
            button = QPushButton("Slab {}".format(i + 1))
            button.setIcon(icon)
            button.setEnabled(False)
            button.setProperty("slab", i)
            button.clicked.connect(self.zoomOnSlab)
            self.buttons.addWidget(button)
        self.scrollWidget = QWidget()
        self.scrollWidget.setLayout(self.buttons)
        self.scrollArea.setWidget(self.scrollWidget)
        self.vLayout.addWidget(self.scrollArea)

    def onClick(self, event):
        index = event.ind
        slabs = self.currentIdSlabs[index[0]]

        for i in range(self.buttons.count()):
            self.buttons.itemAt(i).widget().setEnabled(False)

        for i, slab in enumerate(slabs):
            self.buttons.itemAt(i).widget().setEnabled(True)
            wkt = self.data.slabWkt(slab)
            self.buttons.itemAt(i).widget().setProperty("wkt", wkt)

    def zoomOnSlab(self, event):  # pylint: disable=unused-argument
        sender = self.sender()

        wkt = sender.property("wkt")
        geom = QgsGeometry.fromWkt(wkt)

        self.canvas.setExtent(geom.boundingBox())
        self.canvas.zoomOut()
