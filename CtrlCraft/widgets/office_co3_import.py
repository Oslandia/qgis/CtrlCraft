# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import configparser
import csv
import os
from pathlib import Path

from qgis.core import Qgis
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDialog, QFileDialog

from CtrlCraft.logger import CtrlCraftLogger
from CtrlCraft.utils.misc import CtrlCraftProgress

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), "ui", "import.ui"))


class CtrlCraftOfficeCO3Import(QDialog, FORM_CLASS):
    """
    Dialog to import Slabs in database.
    """

    def __init__(self, data, projIni, deposit, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.data = data
        self.projIni = projIni
        self.deposit = deposit
        self.config = configparser.ConfigParser()
        self.config.read(self.projIni)
        self.updated = False

        self.__populateProjComboBox()
        self.mCancelButton.clicked.connect(self.__close)
        self.mImportButton.clicked.connect(self.__import)
        self.mSelectButton.clicked.connect(self.__select)
        self.mLineEdit.textChanged.connect(self.__projDef)

        self.mProgressBar.setValue(0)
        self.mLabel.setText("")

    def __close(self):
        self.close()

    def __select(self):
        dlg = QFileDialog()
        dlg.setNameFilter("CSV files (*.txt *.csv)")

        filenames = []
        if dlg.exec():
            filenames = dlg.selectedFiles()

        if filenames:
            filename = filenames[0]

            # read header to fill combobox
            with open(filename, "r", encoding="utf-8") as csvfile:
                reader = csv.reader(csvfile, delimiter=",")
                header = next(reader)

                if self.__isValid(header):
                    self.mLineEdit.setText(filename)
                else:
                    CtrlCraftLogger().logBar("Invalid format", Qgis.Critical)

    def __populateProjComboBox(self):
        for deposit in self.config:
            self.mProjComboBox.addItem(deposit)
        self.mProjComboBox.setCurrentIndex(
            self.mProjComboBox.findText(self.deposit) if self.mProjComboBox.findText(self.deposit) != -1 else 0
        )
        self.mProjComboBox.setEnabled(False)

    def __projDef(self):
        filename = self.mLineEdit.text()
        deposit = self.__getDepositTranslationName(filename)
        if self.mProjComboBox.findText(deposit) != -1:
            self.mProjComboBox.setCurrentIndex(self.mProjComboBox.findText(deposit))

    def __getDepositTranslationName(self, filename):
        with open(filename, encoding="utf-8") as importFile:
            co3Deposit = importFile.readlines()[1].split(",")[1]
        deposit = co3Deposit if co3Deposit in list(self.config.keys()) else "DEFAULT"
        return deposit

    def __import(self):
        filename = self.mLineEdit.text()
        if not filename:
            return
        deposit = self.__getDepositTranslationName(filename)
        transX = float(self.config[deposit]["x"]) if deposit != "DEFAULT" else 0
        transY = float(self.config[deposit]["y"]) if deposit != "DEFAULT" else 0
        transZ = float(self.config[deposit]["z"]) if deposit != "DEFAULT" else 0
        self.mSelectButton.setEnabled(False)
        self.mCancelButton.setEnabled(False)
        self.mImportButton.setEnabled(False)

        newCo3, nlines = CtrlCraftOfficeCO3Import.doImport(
            filename,
            self.data,
            transX,
            transY,
            transZ,
            CtrlCraftProgress(self.mLabel, self.mProgressBar),
        )

        if newCo3 > 0:
            self.updated = True

        msg = self.tr("{} co3 holes imported with success ({} ignored)!").format(newCo3, nlines - newCo3)
        self.mLabel.setText(msg)
        self.mCancelButton.setEnabled(True)

    @staticmethod
    def doImport(filename, data, transX=0, transY=0, transZ=0, progress=None):  # pylint: disable=R0914,
        if progress:
            progress.reset("Insert slabs in database...")

        filePath = Path(filename)
        filePath = filePath.absolute()
        with filePath.open("r", encoding="utf-8") as csvfile:
            nlines = len(list(csv.reader(csvfile, delimiter=","))) - 1

        newCo3 = 0
        with filePath.open("r", encoding="utf-8") as csvfile:
            reader = csv.reader(csvfile, delimiter=",")

            nline = 0
            header = None
            for row in reader:
                if not header:
                    header = True
                    continue

                deposit = row[1]
                panel = row[2]
                blast = row[3]
                depth = float(row[8])
                co3 = float(row[9])
                x = float(row[5]) + transX
                y = float(row[6]) + transY
                z = float(row[7]) + transZ

                if not data.co3(x, y):
                    data.co3Create(deposit, panel, blast, depth, co3, x, y, z)
                    newCo3 += 1

                # Update progress bar
                nline += 1
                if progress:
                    progress.setValue(int(nline * 100 / nlines))

            if progress:
                progress.setValue(100)

            if progress:
                progress.reset("Create index...")
            data.co3CreateIndex()

            if progress:
                progress.setValue(100)

        return newCo3, nlines

    def __isValid(self, header):
        validHeader = [
            "INDEX",
            "GISEMENT",
            "PANNEAU_TIR",
            "NOM_TIR",
            "X",
            "Y",
            "Z",
            "PROFONDEUR_FOREE",
            "CO3",
        ]

        return header == validHeader
