# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Configure the thresholds for the styling
- in the probe default qml file
- in the slabs default qml file
- in the atlas default qml file
- in the database

The widget makes the interface for the user to choose the new thresholds
"""

import os
import xml.etree.ElementTree as ET

from qgis.core import QgsProject
from qgis.gui import QgsDoubleSpinBox
from qgis.PyQt.QtWidgets import (
    QCheckBox,
    QDialog,
    QHBoxLayout,
    QLabel,
    QMessageBox,
    QPushButton,
    QVBoxLayout,
)


class CtrlCraftSlabThreshold(QDialog):  # pylint: disable=too-many-instance-attributes
    """Dialog used to define slab symbology threshold"""

    def __init__(self, project, data, parent=None):
        """Constructor of the dialog used to define slab symbology threshold"""
        super().__init__(parent)

        self.__project = project
        self.__data = data
        self.setWindowTitle(self.tr("Slab thresholds"))

        self.__slabsDefaultStylePath = os.path.join(os.path.dirname(__file__), "..", "qgis/slabs_defaut.qml")
        self.__probeDefaultStylePath = os.path.join(os.path.dirname(__file__), "..", "qgis/probe_defaut.qml")
        self.__atlasStylePath = os.path.join(os.path.dirname(__file__), "..", "qgis/atlas.qml")

        vLayout = QVBoxLayout(self)
        for i, threshold in self.__data.getClassThresholds().items():
            hLayout = QHBoxLayout()
            label = QLabel()
            label.setMinimumWidth(25)
            label.setMaximumWidth(25)
            label.setStyleSheet(f"background-color: {threshold['color']}")
            minSpinbox = QgsDoubleSpinBox()
            minSpinbox.setDecimals(6)
            minSpinbox.setSingleStep(0.1)
            minSpinbox.setMinimum(-2000)
            minSpinbox.setMaximum(2000)
            minSpinbox.setValue(threshold["min"])
            maxSpinbox = QgsDoubleSpinBox()
            maxSpinbox.setDecimals(6)
            maxSpinbox.setSingleStep(0.1)
            maxSpinbox.setMinimum(-2000)
            maxSpinbox.setMaximum(2000)
            maxSpinbox.setValue(threshold["max"])
            checkbox = QCheckBox()
            checkbox.setChecked(threshold["checked"])
            hLayout.addWidget(checkbox)
            hLayout.addWidget(label)
            hLayout.addWidget(minSpinbox)
            hLayout.addWidget(maxSpinbox)
            vLayout.addLayout(hLayout)
            if i == 0:
                maxSpinbox.setVisible(False)
                minSpinbox.setEnabled(False)
                hLayout.addStretch()

        acceptButton = QPushButton("OK")
        acceptButton.clicked.connect(self.accept)
        vLayout.addWidget(acceptButton)

    def accept(self):
        """Triggered on OK push button"""
        vlayout = self.findChild(QVBoxLayout)
        hlayouts = vlayout.findChildren(QHBoxLayout)
        thresholds = {}
        for i, hlayout in enumerate(hlayouts):
            minVal = round(hlayout.itemAt(2).widget().value(), 10)
            maxVal = round(hlayout.itemAt(3).widget().value(), 10)
            if maxVal <= minVal:
                QMessageBox.warning(
                    None,
                    self.tr("Warning"),
                    self.tr("Maximum must be greater than minimum for class {} - {}!").format(minVal, maxVal),
                )
                return
            thresholds[i] = {
                "checked": hlayout.itemAt(0).widget().isChecked(),
                "color": hlayout.itemAt(1).widget().styleSheet()[18:],
                "min": minVal,
                "max": maxVal,
            }

        updateQml(self.__slabsDefaultStylePath, thresholds)
        updateQml(self.__atlasStylePath, thresholds)
        updateQml(self.__probeDefaultStylePath, thresholds)
        self.__data.setClassThresholds(thresholds)

        lyr = self.__project.slabsLayer()
        styleManager = lyr.styleManager()
        styleManager.setCurrentStyle("filtre_atlas")
        lyr.loadNamedStyle(self.__atlasStylePath)
        styleManager.setCurrentStyle("défaut")
        lyr.loadNamedStyle(self.__slabsDefaultStylePath)
        self.__project.probeLayer().loadNamedStyle(self.__probeDefaultStylePath)
        QgsProject.instance().write()

        super().accept()


def updateQml(path, thresholds):
    """Update the QML file with thresholds values

    :param path: the path to the qml file
    :type path: str

    :param thresholds: dictionnary of the form
        {i: {'min': <value>, 'max': <value>', 'checked': <value>, 'color': <value>}}
    """

    tree = ET.parse(path)
    root = tree.getroot()
    renderer = root.find("renderer-v2")
    symbols = renderer.find("symbols").findall("symbol")
    rules = renderer.find("rules").findall("rule")
    symbolDict = {}
    for symbol in symbols:
        options = symbol.find("layer").find("Option").findall("Option")
        for option in options:
            if option.attrib["name"] == "color":
                symbolDict[symbol.attrib["name"]] = option
    for rule in rules:
        filterExpression = rule.attrib["filter"].lower()
        if filterExpression == "else":
            continue
        if '"radiometry_class" = ' in filterExpression:
            classId = int(filterExpression[-1])
            if classId == 0:
                rule.set("label", f"{thresholds[classId]['min']}")
            elif classId == 6:
                rule.set("label", f"+{thresholds[classId]['min']}")
            else:
                rule.set("label", f"{thresholds[classId]['min']} - {thresholds[classId]['max']}")
            rule.set("checkstate", "1" if thresholds[classId]["checked"] else "0")
            symbolDict[rule.attrib["symbol"]].set("value", thresholds[classId]["color"])

    tree.write(path)
