# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os

from qgis.PyQt import QtWidgets, uic

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), "ui", "level_filter.ui"))


class CtrlCraftLevelFilter(QtWidgets.QDialog, FORM_CLASS):
    def __init__(self, data, actualLevel, deposit=None, panel=None, blast=None, parent=None):
        super().__init__(parent)
        self.data = data

        self.setupUi(self)

        idx = 0
        if actualLevel == -1:
            levels = self.data.getRemovedLevels(deposit, panel, blast)
        else:
            levels = self.data.getLevels(deposit, panel, blast)

        for i, level in enumerate(levels):
            if level[1] == actualLevel:
                idx = i
            self.mLevels.addItem(str(level[0]), level[1])

        self.mLevels.setCurrentIndex(idx)
