# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""Module to export CtrlCraft atlas"""

import os

from qgis.core import QgsLayoutExporter, QgsProject
from qgis.gui import QgsFileWidget
from qgis.PyQt import QtCore, QtWidgets, uic
from qgis.utils import OverrideCursor

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), "ui", "export_atlas.ui"))


class ExportAtlas(QtWidgets.QDialog, FORM_CLASS):
    """Class to export an atlas, with the widget"""

    def __init__(self, parent=None):
        """Constructor."""
        super().__init__(parent)
        self.setupUi(self)

        self.mAtlasOutput.setStorageMode(QgsFileWidget.SaveFile)
        self.mAtlasOutput.setFilter("*.pdf")
        defaultPath = os.path.join(QtCore.QDir().home().path(), "Desktop")
        self.mAtlasOutput.setDefaultRoot(defaultPath)
        self.mAtlasOutput.setFilePath(os.path.join(defaultPath, "export_atlas.pdf"))
        self.buttonBox.accepted.connect(self.exportAtlas)

    def exportAtlas(self):
        outputFile = self.mAtlasOutput.filePath()
        if not os.path.exists(os.path.dirname(outputFile)):
            QtWidgets.QMessageBox.warning(None, self.tr("Warning"), self.tr("Output destination does not exists!"))
            return

        if outputFile:
            with OverrideCursor(QtCore.Qt.WaitCursor):
                err = exportAtlasPdf(outputFile)

            msgBox = QtWidgets.QMessageBox()
            msgBox.setTextFormat(QtCore.Qt.RichText)

            if err[0] == QgsLayoutExporter.Success:
                msgBox.setText(
                    self.tr("Atlas successfully exported to <a href='file:///{0}'>{0}</a>".format(outputFile))
                )
            else:
                msgBox.setText(self.tr(f"Error during export atlas: {err[1]}"))

            msgBox.exec()


def exportAtlasPdf(outputFile, layoutName="export_niveaux"):
    """
    Export an atlas in PDF to outputFile
    """

    project = QgsProject.instance()

    layout = project.layoutManager().layoutByName(layoutName)

    if layout:
        exporter = QgsLayoutExporter(layout)
        settings = QgsLayoutExporter.PdfExportSettings()
        settings.dpi = 100
        settings.rasterizeWholeImage = True
        err = exporter.exportToPdf(layout.atlas(), outputFile, settings)

        return err

    return [QgsLayoutExporter.FileError, f"Layout'{layoutName}' not found."]
