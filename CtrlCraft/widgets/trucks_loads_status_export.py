# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

from qgis.PyQt.QtWidgets import QFileDialog


class CtrlCraftTrucksLoadsStatusExport:  # pylint: disable=too-few-public-methods
    """
    Dialog to import Slabs in database.
    """

    def __init__(self, data):
        self.data = data

    def export(self):
        csv, _ = QFileDialog.getSaveFileName(None, "RTKube data", "", "CSV (*.csv);;All Files (*)")
        if csv:
            if not csv.endswith(".csv"):
                csv = "{}.{}".format(csv, "csv")
            self.data.trucksLoadsStatusExport(csv)
