# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Office shipment table
"""

import os

from qgis.PyQt import uic
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtWidgets import (
    QAbstractItemView,
    QDialog,
    QTableWidget,
    QTableWidgetItem,
)

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), "ui", "base.ui"))


class CtrlCraftOfficeShipmentsTable(QDialog, FORM_CLASS):
    def __init__(self, data, parent=None):
        """Constructor."""
        super().__init__(parent)
        self.setupUi(self)
        self.data = data

    def init(self, deposit, panel, blast):
        # Read DB Slab data
        shipments = self.data.shipments(deposit, panel, blast)

        # Build the table
        table = QTableWidget()
        table.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # initiate table
        table.setRowCount(len(shipments))

        colColors = [
            None,
            None,
            None,
            None,
            QColor(246, 255, 213),
            None,
            QColor(255, 170, 170),
            QColor(255, 170, 170),
            QColor(198, 240, 255),
        ]

        nbCols = len(colColors)
        table.setColumnCount(nbCols)

        table.setHorizontalHeaderLabels(
            (
                self.tr("Slabs"),
                self.tr("Deposit"),
                self.tr("Panel"),
                self.tr("Blast"),
                self.tr("Truck"),
                self.tr("Unload Datetime"),
                self.tr("Mean estimated grade"),
                self.tr("Mean probe grade"),
                self.tr("Gate grade"),
            )
        )

        item = table.horizontalHeaderItem(0)
        item.setToolTip(self.tr("Number of slabs in the shipment"))

        item = table.horizontalHeaderItem(1)
        item.setToolTip(self.tr("Deposit where slabs have been taken"))

        item = table.horizontalHeaderItem(2)
        item.setToolTip(self.tr("Panel where slabs have been taken"))

        item = table.horizontalHeaderItem(3)
        item.setToolTip(self.tr("Blast where slabs have been taken"))

        item = table.horizontalHeaderItem(4)
        item.setToolTip(self.tr("Name of the truck"))

        item = table.horizontalHeaderItem(5)
        item.setToolTip(self.tr("Datetime of the unloading"))

        item = table.horizontalHeaderItem(6)
        item.setToolTip(self.tr("Mean estimated grade for all slabs in the shipment"))

        item = table.horizontalHeaderItem(7)
        item.setToolTip(self.tr("Mean probe grade for all slabs in the shipment"))

        item = table.horizontalHeaderItem(8)
        item.setToolTip(self.tr("Grade of the shipment measured by the gate"))

        # set data
        tRow = 0

        for shipment in shipments:
            for col in range(0, nbCols - 1):
                table.setItem(tRow, col, QTableWidgetItem(shipment[col]))
                if colColors[col]:
                    table.item(tRow, col).setBackground(colColors[col])

            tRow += 1

            unloadTime = shipment[5]

            if not unloadTime:
                continue

            # do not use seconds because the portic datetime is in minutes only
            times = unloadTime.split(":")
            unloadTime = "{}:{}".format(times[0], times[1])

        table.horizontalHeader().setStretchLastSection(True)
        table.resizeColumnsToContents()

        self.vLayout.addWidget(table)
