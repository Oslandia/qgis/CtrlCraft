# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os

import serial.tools.list_ports
from qgis.PyQt import QtWidgets, uic
from qgis.PyQt.QtCore import (
    QRegExp,
    QSettings,
    Qt,
    pyqtSignal,
)
from qgis.PyQt.QtGui import QRegExpValidator, QValidator
from qgis.PyQt.QtWidgets import (
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QSpinBox,
)
from qgis.utils import OverrideCursor

from CtrlCraft.widgets.custom_edition import CtrlCraftCustomEdition

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), "ui", "main_parameters.ui"))

settings = QSettings()


class CtrlCraftMainParametersDock(QtWidgets.QDockWidget, FORM_CLASS):
    """DockWidget for main parameters"""

    configModified = pyqtSignal()

    def __init__(self, iface, data, parent=None):
        """Constructor."""
        super().__init__(parent)
        self.data = data
        self.setupUi(self)
        self.trucksInfo = {}
        self.nbTruck = 0

        if "CTRLCRAFT_ADVANCEDUSER" in os.environ and int(os.environ["CTRLCRAFT_ADVANCEDUSER"]) == 1:
            self.mProjButton.setEnabled(True)
            self.mGammaButton.setEnabled(True)
            self.mGammaNormalizationButton.setEnabled(True)
        else:
            self.mProjButton.setEnabled(False)
            self.mGammaButton.setEnabled(False)
            self.mGammaNormalizationButton.setEnabled(False)
        self.mCancelButton.clicked.connect(self.__close)
        self.mProjButton.clicked.connect(self.__editProj)
        self.mGammaButton.clicked.connect(self.__editGamma)
        self.mGammaNormalizationButton.clicked.connect(self.__editNormalizationGamma)
        self.mSaveButton.clicked.connect(self.__save)

        self.nbTruckSpinBox.valueChanged.connect(self.__trucksUpdate)
        self.nbTruckSpinBox.setValue(int(settings.value("/CtrlCraft/nbTrucks", 4)))
        self.__update()
        iface.addDockWidget(Qt.LeftDockWidgetArea, self)
        self.hide()

    def __close(self):
        self.close()

    def __populateGPSComboBox(self, currentValue):
        gpsDict = {0: "Undetermined", 1: "GNSS", 6: "Diff", 7: "DGNSS", 15: "Float", 23: "Fixed"}
        for mode in gpsDict.values():
            self.mGPSComboBox.addItem(mode)
        self.mGPSComboBox.setCurrentIndex(
            self.mGPSComboBox.findText(currentValue) if self.mGPSComboBox.findText(currentValue) != -1 else 0
        )

    def __populateSerialComboBoxes(self):
        self.mSerialPortRover.clear()
        self.mSerialPortBase.clear()
        self.mSerialPortSpare.clear()
        comports = serial.tools.list_ports.comports()
        self.mSerialPortRover.addItem("")
        self.mSerialPortBase.addItem("")
        self.mSerialPortSpare.addItem("")
        for i, com in enumerate(comports):
            self.mSerialPortRover.addItem(com[0])
            self.mSerialPortBase.addItem(com[0])
            self.mSerialPortSpare.addItem(com[0])
            information = "Ports: {}\nDescription: {}\nHWID: {}".format(*com)
            self.mSerialPortRover.setItemData(i + 1, information, Qt.ToolTipRole)
            self.mSerialPortBase.setItemData(i + 1, information, Qt.ToolTipRole)
            self.mSerialPortSpare.setItemData(i + 1, information, Qt.ToolTipRole)

        self.mSerialPortRover.setCurrentIndex(
            self.mSerialPortRover.findText(str(settings.value("/GeoCube/serialportrover")))
            if self.mSerialPortRover.findText(str(settings.value("/GeoCube/serialportrover"))) != -1
            else 0
        )
        self.mSerialPortBase.setCurrentIndex(
            self.mSerialPortBase.findText(str(settings.value("/GeoCube/serialportbase")))
            if self.mSerialPortBase.findText(str(settings.value("/GeoCube/serialportbase"))) != -1
            else 0
        )
        self.mSerialPortSpare.setCurrentIndex(
            self.mSerialPortSpare.findText(str(settings.value("/GeoCube/serialportspare")))
            if self.mSerialPortSpare.findText(str(settings.value("/GeoCube/serialportspare"))) != -1
            else 0
        )

    def __editGamma(self):
        """Open a edit windows for editing gamma.ini file"""
        gammaPath = os.path.join(os.path.dirname(__file__), "..", "gamma.ini")
        edit = CtrlCraftCustomEdition(gammaPath)
        edit.setWindowTitle(self.tr("Correlation coefficients"))
        self.mProjButton.setEnabled(False)
        self.mSaveButton.setEnabled(False)
        self.mCancelButton.setEnabled(False)
        edit.exec()
        self.configModified.emit()
        self.mProjButton.setEnabled(True)
        self.mSaveButton.setEnabled(True)
        self.mCancelButton.setEnabled(True)

    def __editNormalizationGamma(self):
        """Open a edit windows for editing probe_normalization.ini file"""
        gammaNPath = os.path.join(os.path.dirname(__file__), "..", "probe_normalization.ini")
        edit = CtrlCraftCustomEdition(gammaNPath)
        edit.setWindowTitle(self.tr("Normalization coefficients"))
        self.mProjButton.setEnabled(False)
        self.mSaveButton.setEnabled(False)
        self.mCancelButton.setEnabled(False)
        edit.exec()
        self.configModified.emit()
        self.mProjButton.setEnabled(True)
        self.mSaveButton.setEnabled(True)
        self.mCancelButton.setEnabled(True)

    def __editProj(self):
        """Open a edit windows for editing proj.ini file"""
        projPath = os.path.join(os.path.dirname(__file__), "..", "proj.ini")
        edit = CtrlCraftCustomEdition(projPath)
        self.mProjButton.setEnabled(False)
        self.mSaveButton.setEnabled(False)
        self.mCancelButton.setEnabled(False)
        edit.exec()
        self.configModified.emit()
        self.mProjButton.setEnabled(True)
        self.mSaveButton.setEnabled(True)
        self.mCancelButton.setEnabled(True)

    def __validate(self):
        validator = QRegExpValidator(QRegExp("[A-Z0-9]+"))
        test = True
        for i in range(1, self.nbTruck + 1):
            truckId = self.trucksInfo["Truck {}".format(i)][0].text()
            if validator.validate(truckId, 0)[0] != QValidator.Acceptable:
                QtWidgets.QMessageBox.warning(
                    None,
                    self.tr("Truck ID"),
                    self.tr(
                        "'{0}' is not a valid name:"
                        "\nTruck ID must be composed of alphanumeric upper case characters."
                    ).format(truckId),
                )
                test = False
                break
        return test

    def __cleanHBoxes(self):
        for layout in self.TVerticalLayout.children():
            for index in reversed(range(layout.count())):
                layout.itemAt(index).widget().setParent(None)

            layout = self.TVerticalLayout.itemAt(0)
            self.TVerticalLayout.removeItem(layout)
            layout.setParent(None)
            # del layout

    def __trucksUpdate(self):
        self.nbTruck = self.nbTruckSpinBox.value()
        self.trucksInfo = {}
        self.__cleanHBoxes()
        for i in range(1, self.nbTruck + 1):
            hBox = QHBoxLayout()
            hBox.addWidget(QLabel(self.tr("Truck ") + "{}".format(i)))
            lineEdit = QLineEdit()
            lineEdit.setValidator(QRegExpValidator(QRegExp("[A-Z0-9]+")))
            lineEdit.setText(settings.value("/CtrlCraft/Truck{}/id".format(i), str(i).zfill(3)))
            hBox.addWidget(lineEdit)
            spinBox = QSpinBox()
            spinBox.setValue(int(settings.value("/CtrlCraft/Truck{}/sc".format(i), 8)))
            hBox.addWidget(spinBox)
            self.trucksInfo["Truck {}".format(i)] = (lineEdit, spinBox)
            self.TVerticalLayout.addLayout(hBox)

    def __update(self):
        """Init parameters in the different boxes"""
        self.__trucksUpdate()
        self.__populateGPSComboBox(str(settings.value("/GeoCube/GPSmode")))
        self.mAngleSpinBox.setValue(int(settings.value("/GeoCube/angle", 10)))
        self.__populateSerialComboBoxes()

    def __save(self):
        """Save new parameters and actualize database"""

        with OverrideCursor(Qt.WaitCursor):
            test = self.__validate()
            if test:
                settings.setValue("/CtrlCraft/nbTrucks", self.nbTruck)
                for i in range(1, self.nbTruck + 1):
                    settings.setValue(
                        "/CtrlCraft/Truck{}/id".format(i), self.trucksInfo["Truck {}".format(i)][0].text()
                    )
                    settings.setValue(
                        "/CtrlCraft/Truck{}/sc".format(i), self.trucksInfo["Truck {}".format(i)][1].value()
                    )
                settings.setValue("/GeoCube/GPSmode", self.mGPSComboBox.currentText())
                settings.setValue("/GeoCube/angle", self.mAngleSpinBox.value())
                settings.setValue("/GeoCube/serialportrover", self.mSerialPortRover.currentText())
                settings.setValue("/GeoCube/serialportbase", self.mSerialPortBase.currentText())
                settings.setValue("/GeoCube/serialportspare", self.mSerialPortSpare.currentText())
                self.data.truckIdsChange(
                    [self.trucksInfo["Truck {}".format(i)][0].text() for i in range(1, self.nbTruck + 1)]
                )

                self.configModified.emit()

                self.close()
