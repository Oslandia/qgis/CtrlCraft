# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Dialog to import Slabs in database.
"""

import os

from qgis.core import Qgis
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import (
    QDialog,
    QFileDialog,
    QMessageBox,
    qApp,
)
from qgis.utils import iface

from CtrlCraft.logger import CtrlCraftLogger

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), "ui", "import_db.ui"))


class CtrlCraftDatabaseImport(QDialog, FORM_CLASS):
    """
    Dialog to import Slabs in database.
    """

    def __init__(self, data, rtkube, project, deposit, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.data = data
        self.rtkube = rtkube
        self.project = project
        self.deposit = deposit
        self.updated = False

        # self.config = self.__populateProjComboBox()
        self.mCancelButton.clicked.connect(self.__close)
        self.mImportButton.clicked.connect(self.__import)
        self.mSelectButton.clicked.connect(self.__select)

        self.mProgressBar.setValue(0)
        self.mLabel.setText("")

    def __close(self):
        self.close()

    def __select(self):
        filename, _ = QFileDialog.getOpenFileName(self, self.tr("Dump file"), filter="*.dump")

        if filename:
            self.mLineEdit.setText(filename)

    def __import(self):
        filename = self.mLineEdit.text()
        if not filename:
            return

        # deposit = self.mProjComboBox.currentText()
        self.mSelectButton.setEnabled(False)
        self.mCancelButton.setEnabled(False)
        self.mImportButton.setEnabled(False)

        self.mLabel.setText(self.tr("Import database..."))
        self.mProgressBar.setMinimum(0)
        self.mProgressBar.setMaximum(0)
        qApp.processEvents()

        self.rtkube.save = False
        self.rtkube.layer = None
        self.project.close()
        try:
            self.data.importDump(self.mLineEdit.text())
        except RuntimeError as e:
            CtrlCraftLogger("Import database", iface).logBar(str(e), Qgis.Critical)
            self.reject()
            return

        self.data.open()

        if self.data.isPre310():
            QMessageBox.critical(
                None, self.tr("Old dump"), self.tr("The imported dump is not compatible with CtrlCraft >= 3.1.0.")
            )
            self.reject()
            return

        # if deposit != 'DEFAULT':
        #     customProj=[float(self.config[deposit][i]) for i in ['x','y','z']]
        #     self.data.translate(customProj)
        self.project.load()

        self.rtkube.layer = self.project.probeLayer()
        self.rtkube.save = True

        self.mLabel.setText(self.tr("Database correctly imported."))
        self.mCancelButton.setEnabled(True)
        self.mProgressBar.setMaximum(100)
        self.mProgressBar.setValue(100)

        self.updated = True
