# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os

from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDialog

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), "ui", "custom_edition.ui"))


class CtrlCraftCustomEdition(QDialog, FORM_CLASS):
    """Widget to edit a file"""

    def __init__(self, path, parent=None):
        """Constructor."""
        super().__init__(parent)
        self.path = path if os.path.exists(path) else path + ".sample"
        self.setupUi(self)
        self.__initTextEdit()

        self.mCancelButton.clicked.connect(self.__close)
        self.mSaveButton.clicked.connect(self.__save)

    def __close(self):
        self.close()

    def __save(self):
        txt = self.mCustomTextEdit.toPlainText()
        self.path = self.path[:-7] if self.path[-7:] == ".sample" else self.path
        with open(self.path, "w", encoding="utf-8") as dstFile:
            dstFile.write(txt)
        self.close()

    def __initTextEdit(self):
        textStr = ""
        with open(self.path, encoding="utf-8") as text:
            for i in text.readlines():
                textStr += i
        self.mCustomTextEdit.insertPlainText(textStr)
