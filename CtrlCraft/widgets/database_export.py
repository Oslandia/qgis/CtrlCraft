# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Module for database export UI
"""

import os

from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDialog, QFileDialog, qApp

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), "ui", "export_db.ui"))


class CtrlCraftDatabaseExport(QDialog, FORM_CLASS):
    """
    Dialog to export Slabs in database.
    """

    def __init__(self, data, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.data = data

        self.mCancelButton.clicked.connect(self.__close)
        self.mExportButton.setText(self.tr("Export"))
        self.mExportButton.clicked.connect(self.__export)
        self.mSelectButton.clicked.connect(self.__select)

        self.mProgressBar.setValue(0)
        self.mLabel.setText("")

    def __close(self):
        self.close()

    def __select(self):
        filename, _ = QFileDialog.getSaveFileName(self, self.tr("Dump file"), filter="*.dump")

        if filename:
            self.mLineEdit.setText(filename)

    def __export(self):
        filename = self.mLineEdit.text()
        if not filename:
            return

        self.mSelectButton.setEnabled(False)
        self.mCancelButton.setEnabled(False)
        self.mExportButton.setEnabled(False)

        self.mLabel.setText(self.tr("Exporting database..."))
        self.mProgressBar.setMaximum(0)
        qApp.processEvents()
        self.data.export(self.mLineEdit.text())

        self.mLabel.setText(self.tr("Database correctly exported."))
        self.mCancelButton.setEnabled(True)
        self.mProgressBar.setMaximum(100)
        self.mProgressBar.setValue(100)
        self.mCancelButton.setEnabled(True)
