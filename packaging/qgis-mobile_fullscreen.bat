@echo off
set packaging=%~dp0
set OSGEO4W_ROOT=C:\osgeo4w64
call "%OSGEO4W_ROOT%\bin\o4w_env.bat"
path %OSGEO4W_ROOT%\apps\qgis-ltr\bin;%PATH%
set QGIS_PREFIX_PATH=%OSGEO4W_ROOT:\=/%/apps/qgis-ltr
set GDAL_FILENAME_IS_UTF8=YES
rem Set VSI cache to be used as buffer, see #6448
set VSI_CACHE=TRUE
set VSI_CACHE_SIZE=1000000
set QT_PLUGIN_PATH=%OSGEO4W_ROOT%\apps\qgis-ltr\qtplugins;%OSGEO4W_ROOT%\apps\qt5\plugins

start "QGIS" /B "%OSGEO4W_ROOT%\bin\qgis-ltr-bin.exe" --code "%packaging%\fullscreen.py" --globalsettingsfile "%packaging%\mobile\QGIS3.ini" --customizationfile "%packaging%\mobile\mobile.ini" --project "%OSGEO4W_ROOT%\apps\qgis-ltr\python\plugins\CtrlCraft\qgis\ctrlcraft.qgs"
