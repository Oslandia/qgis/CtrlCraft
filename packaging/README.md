# Packaging pour CtrlCraft

Le script `make_installer.sh` package CtrlCraft et toutes ses dépendences dans un fichier exe Windows d'installation pouvant alors être utilisé hors ligne.

## Lancer l'installateur

Il faut éditer `make_installer.sh` pour paramétrer `CTRLCRAFT_ONLY` et `CTRLCRAFT_VERSION`.

Mettre `CTRLCRAFT_ONLY` à 1 permet de ne packager que le plugin CtrlCraft, et créer un installateur qui ne fera que la mise à jour du plugin.  
Mettre `CTRLCRAFT_ONLY` à 0 permet de packager toutes les dépendances de CtrlCraft (QGIS LTR compris).

Note : pour faciliter le packaging à répétition (lors de tests par exemple), il est possible de mettre les fichiers téléchargés dans un dossier `cache`
situé dans le présent dossier.

## Explication sur le packaging

Le présent dossier (`packaging`) ne fait pas partie du plugin, mais une majorité de son contenu est copié dans le paquet pour être mis
dans le dossier du plugin sur le poste où le déploiement est fait. Ceci pour gérer l'action postinstall, qui va créer des raccourcis
sur le bureau ainsi que lancer l'initialisation de postgresql.

De même, les scripts de lancement .bat de CtrlCraft, et les fichiers de configuration, y sont copiés.
