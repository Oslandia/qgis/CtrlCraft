#!/usr/bin/env bash

# VARIABLE TO EDIT (see README) --------
CTRLCRAFT_ONLY=0
# --------------------------------------

OSGEO4W_REPO=http://download.osgeo.org/osgeo4w/v2
OSLANDIA_REPO=http://osgeo4w-oslandia.com/extra
DEFAULT_INSTALL_PATH=C:\\osgeo4w64
INSTALLER_NAME=ctrlcraft
HERE=`pwd`

# We have to go one directory up to build the plugin with qgis-plugin-ci tool
# so we need to make sure that we run this script from its directory
if [[ $HERE =~ /.+/packaging ]]; then
  cd ..
  qgis-plugin-ci release latest || exit -1
  CTRLCRAFT_VERSION=`ls -t CtrlCraft.*.*.*.zip | sed 's/CtrlCraft.//' | sed 's/.zip//' | head -1`
  cd $HERE
else
  echo $HERE
  echo "Run this program from the packaging directory"
  exit -1
fi

# Prepare temporary build directory where all dependencies will be downloaded and prepared
rm -r build
mkdir -p build/osgeo4w
cd build/osgeo4w

# Create the directory tree for CtrlCraft itself and for its postinstall step
mkdir -p build/apps/qgis-ltr/python/plugins/
mkdir -p build/etc/postinstall
unzip $HERE/../CtrlCraft.$CTRLCRAFT_VERSION.zip -d build/apps/qgis-ltr/python/plugins/
cp $HERE/ctrlcraft_postinstall.bat build/etc/postinstall/
mkdir build/apps/qgis-ltr/python/plugins/CtrlCraft/postinstall
cp -R $HERE/mobile build/apps/qgis-ltr/python/plugins/CtrlCraft/postinstall/
cp -R $HERE/office build/apps/qgis-ltr/python/plugins/CtrlCraft/postinstall/
cp $HERE/fullscreen.py build/apps/qgis-ltr/python/plugins/CtrlCraft/postinstall/
cp $HERE/prepare.py build/apps/qgis-ltr/python/plugins/CtrlCraft/postinstall/
cp $HERE/qgis*.bat build/apps/qgis-ltr/python/plugins/CtrlCraft/postinstall/
cp $HERE/*.ttf build/apps/qgis-ltr/python/plugins/CtrlCraft/postinstall/
mkdir -p x86_64/release/extra/qgis-ctrlcraft-plugin/

# Build CtrlCraft as a OSGeo4W package
tar -C build -cvjf x86_64/release/extra/qgis-ctrlcraft-plugin/qgis-ctrlcraft-plugin-$CTRLCRAFT_VERSION-1.tar.bz2 apps etc
rm -r build

# Create config file for self extracting archive (7z SFX)
echo ';!@Install@!UTF-8!' > config.txt
echo "Title=\"CtrlCraft $CTRLCRAFT_VERSION\"" >> config.txt

# Use standard OSGeo4W installer
wget http://download.osgeo.org/osgeo4w/v2/osgeo4w-setup.exe
7z a -m0=lzma arc.7z osgeo4w-setup.exe

if [ $CTRLCRAFT_ONLY -eq 1 ]; then
  echo "BeginPrompt=\"Vous allez mettre à jour CtrlCraft vers la version $CTRLCRAFT_VERSION dans $DEFAULT_INSTALL_PATH. Contiuer ?\"" >> config.txt
else
  INSTALLER_NAME=$INSTALLER_NAME-full
  echo "BeginPrompt=\"Vous allez installer/mettre à jour CtrlCraft vers la version $CTRLCRAFT_VERSION\navec toutes ses dépendances dans $DEFAULT_INSTALL_PATH.\n\nATTENTION : les bases de données seront perdues.\n\nContinuer ?\"" >> config.txt

  # get and build pglite
  wget -O pglite.zip https://gitlab.com/Oslandia/pglite/-/archive/v1.0.13/pglite-v1.0.13.zip
  unzip pglite.zip
  cd pglite-v1.0.13
  mkdir -p apps/Python312/Lib/site-packages
  mkdir apps/Python312/Scripts
  mkdir -p etc/postinstall
  pip3 install . -t target
  mv target/pglite apps/Python312/Lib/site-packages/
  mv target/pglite-1.0.13.dist-info apps/Python312/Lib/site-packages/
  mv target/bin/pglite.bat apps/Python312/Scripts/
  wget -O etc/pglite.conf https://gitlab.com/Oslandia/osgeo4w/-/raw/master/packages/python3-pglite/pglite.conf
  wget -O etc/postinstall/set_pglite_python_path.bat https://gitlab.com/Oslandia/osgeo4w/-/raw/master/packages/python3-pglite/set_pglite_python_path.bat
  sed -i "s/Python37/Python312/" etc/pglite.conf
  tar -cvjf python-pglite-1.0.13-1.tar.bz2 apps/Python312/Lib/site-packages/pglite apps/Python312/Scripts/pglite.bat etc/pglite.conf etc/postinstall/set_pglite_python_path.bat
  cd -
  mkdir -p x86_64/release/extra/python3-pglite
  mv pglite-v1.0.13/python-pglite-1.0.13-1.tar.bz2 x86_64/release/extra/python3-pglite/
  rm pglite.zip
  rm -rf pglite-v1.0.13

  # get postgresql, postgis, from Oslandia repository (or cache folder)
  libs="x86_64/release/extra/postgresql/postgresql-11.5-4.tar.bz2 \
    x86_64/release/extra/postgis/postgis-3.0.1-5.tar.bz2 \
    x86_64/release/proj/proj-6.3.2-1.tar.bz2 \
    x86_64/release/proj/proj-hpgn/proj-hpgn-1.0-1.tar.bz2 \
    x86_64/release/proj/proj-datumgrid/proj-datumgrid-1.8-1.tar.bz2"
  for f in $libs
  do
    if [ -f $HERE/cache/osgeo4w/$f ]; then
      mkdir -p `dirname $f`
      cp $HERE/cache/osgeo4w/$f `dirname $f`
    else
      wget -nH -r --cut-dirs=1 $OSLANDIA_REPO/$f
    fi
  done

  # get everything else from osgeo repository (or cache folder)
  wget -nc $OSGEO4W_REPO/x86_64/setup.ini
  for f in $(python $HERE/pkg_deps.py setup.ini qgis-ltr python3-pywin32 python3-pyserial python3-matplotlib)
  do
    if [ -f $HERE/cache/osgeo4w/$f ]; then
      mkdir -p `dirname $f`
      cp $HERE/cache/osgeo4w/$f `dirname $f`
    else
      wget -nH -r --cut-dirs=2 $OSGEO4W_REPO/$f
    fi
  done

  # add a step to uninstall osgeo4w + postgresql + postgis
  7z a -m0=lzma arc.7z $HERE/uninstall_ctrlcraft.bat
  echo "RunProgram=\"hidcon:uninstall_ctrlcraft.bat\"" >> config.txt
fi

7z a -m0=lzma arc.7z x86_64

# %%T is the path where the self extracting archive is extracted
echo "RunProgram=\"osgeo4w-setup.exe -R $DEFAULT_INSTALL_PATH -m CtrlCraft -k -n -g -q -L -l %%T -P qgis-ctrlcraft-plugin\"" >> config.txt
echo -e ';!@InstallEnd@!\n' >> config.txt

cd $HERE

cat 7zsd.sfx build/osgeo4w/config.txt build/osgeo4w/arc.7z > setup-${INSTALLER_NAME}-${CTRLCRAFT_VERSION}.exe
