"""
This script is executed by OSGeo4W installer at the end of CtrlCraft installation
"""

import os
import platform
import shutil

PACKAGING_DIR = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__))))
PLUGIN_DIR = os.path.join(os.environ["OSGEO4W_ROOT"], "apps", "qgis-ltr", "python", "plugins", "CtrlCraft")

# office splash screen
INI_OFFICE_FILE = os.path.join(PACKAGING_DIR, "office", "office.ini")
SPLASH_OFFICE_DIR = os.path.join(PACKAGING_DIR, "office", "").replace(os.sep, "/")

lines = []
with open(INI_OFFICE_FILE, encoding="utf8") as f:
    for line in f:
        TAG = "{{ SPLASH_DIR }}"
        if TAG in line:
            line = line.replace(TAG, SPLASH_OFFICE_DIR)
        lines.append(line)

with open(INI_OFFICE_FILE, "w", encoding="utf8") as f:
    for line in lines:
        f.write(line)

# mobile splash screen
INI_MOBILE_FILE = os.path.join(PACKAGING_DIR, "mobile", "mobile.ini")
SPLASH_MOBILE_DIR = os.path.join(PACKAGING_DIR, "mobile", "").replace(os.sep, "/")

lines = []
with open(INI_MOBILE_FILE, encoding="utf8") as f:
    for line in f:
        TAG = "{{ SPLASH_DIR }}"
        if TAG in line:
            line = line.replace(TAG, SPLASH_MOBILE_DIR)
        lines.append(line)

with open(INI_MOBILE_FILE, "w", encoding="utf8") as f:
    for line in lines:
        f.write(line)

if platform.system() == "Windows":
    import win32com.client  # pylint: disable=import-error

    # Windows shortcurt
    def createShortcut(shortcutPath, exePath, iconPath):
        shell = win32com.client.Dispatch("WScript.Shell")
        shortcut = shell.CreateShortCut(shortcutPath)
        shortcut.Targetpath = exePath
        shortcut.IconLocation = iconPath
        shortcut.save()

    # office
    path = os.path.join(os.environ["HOMEPATH"], "Desktop", "CtrlCraftOffice.lnk")
    ico = os.path.join(PACKAGING_DIR, "office", "CtrlCraftOffice.ico")
    exe = os.path.join(os.path.dirname(os.path.realpath(__file__)), "qgis-office.bat")
    createShortcut(path, exe, ico)

    # office advanced
    path = os.path.join(os.environ["HOMEPATH"], "Desktop", "CtrlCraftOffice_adm.lnk")
    ico = os.path.join(PACKAGING_DIR, "office", "CtrlCraftOffice_adm.ico")
    exe = os.path.join(os.path.dirname(os.path.realpath(__file__)), "qgis-office-advanced.bat")
    createShortcut(path, exe, ico)

    # office
    path = os.path.join(os.environ["HOMEPATH"], "Desktop", "CtrlCraftMobile.lnk")
    ico = os.path.join(PACKAGING_DIR, "mobile", "CtrlCraftMobile.ico")
    exe = os.path.join(os.path.dirname(os.path.realpath(__file__)), "qgis-mobile.bat")
    createShortcut(path, exe, ico)

    # office advanced
    path = os.path.join(os.environ["HOMEPATH"], "Desktop", "CtrlCraftMobile_adm.lnk")
    ico = os.path.join(PACKAGING_DIR, "mobile", "CtrlCraftMobile_adm.ico")
    exe = os.path.join(os.path.dirname(os.path.realpath(__file__)), "qgis-mobile-advanced.bat")
    createShortcut(path, exe, ico)

    # copy sample files
    shutil.copy(os.path.join(PLUGIN_DIR, "gamma.ini.sample"), os.path.join(PLUGIN_DIR, "gamma.ini"))
    shutil.copy(os.path.join(PLUGIN_DIR, "proj.ini.sample"), os.path.join(PLUGIN_DIR, "proj.ini"))
    shutil.copy(os.path.join(PLUGIN_DIR, "settings.py.sample"), os.path.join(PLUGIN_DIR, "settings.py"))
    shutil.copy(
        os.path.join(PLUGIN_DIR, "probe_normalization.ini.sample"),
        os.path.join(PLUGIN_DIR, "probe_normalization.ini"),
    )

    # add missing font
    os.makedirs(os.path.join(os.environ["APPDATA"], "QGIS", "QGIS3", "profiles", "default", "fonts"), exist_ok=True)
    shutil.move(
        os.path.join(PACKAGING_DIR, "OpenSans[wdth,wght].ttf"),
        os.path.join(os.environ["APPDATA"], "QGIS", "QGIS3", "profiles", "default", "fonts"),
    )
