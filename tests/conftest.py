# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
fixtures
"""

import os

import pytest

from CtrlCraft.ctrlcraft import CtrlCraft
from CtrlCraft.data import CtrlCraftData
from CtrlCraft.truck import CtrlCraftTruck


@pytest.fixture()
def ctrlcraftPlugin(ctrlcraftDb, qgis_iface, mocker) -> CtrlCraft:
    """fixture to create CtrlCraft plugin"""
    mockCtrlCraftData = mocker.patch("CtrlCraft.ctrlcraft.CtrlCraftData")
    mockCtrlCraftData.return_value = ctrlcraftDb

    mockQAction = mocker.patch("CtrlCraft.ctrlcraft.QAction")
    mockCtrlCraftRTKube = mocker.patch("CtrlCraft.ctrlcraft.RTKube")

    ctrlCraft = CtrlCraft(qgis_iface)

    ctrlCraft.undoAction = mockQAction
    ctrlCraft.rtkube = mockCtrlCraftRTKube

    # TODO: why infinite wait: ctrlCraft.initToolBarMobile()
    return ctrlCraft


@pytest.fixture()
def ctrlcraftDb() -> CtrlCraftData:
    """fixture to create db connection"""
    connStr = os.getenv(
        "POSTGRES_CONN",
        "host='Please set POSTGRES_CONN env var with pg connection string'",
    )
    db = CtrlCraftData(connStr)
    db.open()

    if not db.isValid():  # may be a real new db, try create
        db.clear()

    assert db.isValid()

    yield db

    db.close()


@pytest.fixture()
def ctrlCraftTruck_1(ctrlcraftDb, qgis_canvas) -> CtrlCraftTruck:  # pylint: disable=redefined-outer-name
    """fixture to create CtrlCraftTruck"""
    return CtrlCraftTruck("1", ctrlcraftDb, qgis_canvas)


@pytest.fixture()
def ctrlCraftTruck_2(ctrlcraftDb, qgis_canvas) -> CtrlCraftTruck:  # pylint: disable=redefined-outer-name
    """fixture to create CtrlCraftTruck"""
    return CtrlCraftTruck("2", ctrlcraftDb, qgis_canvas)
