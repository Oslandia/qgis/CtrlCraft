# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Slabs thresholds unit tests
"""

import shutil
from pathlib import Path

from qgis.PyQt.QtCore import QTimer

from CtrlCraft.widgets.slab_threshold import CtrlCraftSlabThreshold, updateQml


def test_updateQml():
    originalFilepath = Path(__file__).parent.parent.parent / "CtrlCraft" / "qgis" / "atlas.qml"
    toModifyFilepath = Path(__file__).parent.parent / "data" / "atlas.qml"
    refFilepath = Path(__file__).parent.parent / "data" / "atlas.qml.ref"
    shutil.copyfile(originalFilepath, toModifyFilepath)

    updateQml(
        toModifyFilepath,
        {
            0: {"min": -999, "max": -998.99, "checked": True, "color": "#FFFFFF"},
            1: {"min": 0, "max": 0.9, "checked": False, "color": "#B2B2B2"},
            2: {"min": 0.9, "max": 1.3, "checked": True, "color": "#00FF00"},
            3: {"min": 1.3, "max": 1.7, "checked": True, "color": "#FFE600"},
            4: {"min": 1.7, "max": 1.8, "checked": False, "color": "#FF0000"},
            5: {"min": 1.8, "max": 2.16, "checked": True, "color": "#0073FF"},
            6: {"min": 2.16, "max": 1000, "checked": True, "color": "#A800FF"},
        },
    )
    assert toModifyFilepath.read_text() == refFilepath.read_text()
    toModifyFilepath.unlink(missing_ok=False)


def test_ctrlCraftSlabThreshold(mocker):
    mockProject = mocker.Mock()
    mockData = mocker.Mock()
    mocker.patch("CtrlCraft.widgets.slab_threshold.updateQml")
    mockData.getClassThresholds.return_value = {
        0: {"min": -999, "max": -998.99, "checked": True, "color": "#FFFFFF"},
        1: {"min": 0, "max": 0.7, "checked": False, "color": "#B2B2B2"},
        2: {"min": 0.7, "max": 1.3, "checked": True, "color": "#00FF00"},
        3: {"min": 1.3, "max": 2.4, "checked": True, "color": "#FFE600"},
        4: {"min": 2.4, "max": 3.8, "checked": False, "color": "#FF0000"},
        5: {"min": 3.8, "max": 4.5, "checked": True, "color": "#0073FF"},
        6: {"min": 4.5, "max": 1000, "checked": True, "color": "#A800FF"},
    }

    widget = CtrlCraftSlabThreshold(mockProject, mockData)
    QTimer.singleShot(1000, widget.accept)
    widget.exec()
