# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Truck unit tests
"""

import pytest
from qgis.core import QgsPoint


def test_addTrucks(ctrlcraftDb):
    """test to add truck"""
    trucks = ctrlcraftDb.trucksNoOrder()
    oldSize = len(trucks)

    ctrlcraftDb.truckCreate("truck_name")

    trucks = ctrlcraftDb.trucksNoOrder()
    newSize = len(trucks)
    assert newSize > oldSize

    # could not add 2 truck with the same name
    with pytest.raises(Exception):
        ctrlcraftDb.truckCreate("truck_name")


def test_loadTruck(ctrlcraftDb, ctrlCraftTruck_1):
    """test load slab to truck"""
    # this is a point with an available slab. Z position is important
    pt = QgsPoint(320013, 2077006, 354.25)
    # before the targeted slab radiometry must be set
    ctrlcraftDb.affectRadiometrySlabDegraded(10, 5, 3, f"POINT({pt.x()} {pt.y()})")

    # check existing load
    loadSize = ctrlCraftTruck_1.loads
    assert loadSize == 0

    # add load at position pt for truck 1
    ctrlCraftTruck_1.addLoad(pt)

    # recheck the load count
    loadSize = ctrlCraftTruck_1.loads
    assert loadSize > 0


def test_unloadTruck(ctrlCraftTruck_1):
    """test unload slab from truck"""
    # check existing load
    loadSize = ctrlCraftTruck_1.loads
    assert loadSize > 0

    # add load at position pt for truck 1
    ctrlCraftTruck_1.removeLoads([1, 2, 3])

    # recheck the load count
    loadSize2 = ctrlCraftTruck_1.loads
    assert loadSize2 < loadSize
