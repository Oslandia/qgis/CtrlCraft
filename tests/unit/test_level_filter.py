# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Database unit tests
"""

from CtrlCraft.widgets.level_filter import CtrlCraftLevelFilter


def test_instanciate(qtbot, mocker):  # pylint: disable=unused-argument
    mockData = mocker.Mock()
    mockData.getRemovedLevels.return_value = [(4, 0.4), (5, 0.5), (6, 0.6)]
    mockData.getLevels.return_value = [(8, 0.8), (9, 0.9), (10, 0.1)]

    cclf = CtrlCraftLevelFilter(mockData, -1)
    assert cclf.mLevels.currentIndex() == 0
    assert cclf.mLevels.currentData() == 0.4
    assert cclf.mLevels.currentText() == "4"

    cclf = CtrlCraftLevelFilter(mockData, 0.9)
    assert cclf.mLevels.currentIndex() == 1
    assert cclf.mLevels.currentData() == 0.9
    assert cclf.mLevels.currentText() == "9"
