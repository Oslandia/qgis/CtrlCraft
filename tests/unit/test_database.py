# -----------------------------------------------------------
# Copyright (C) 2023 Oslandia
# infos at oslandia dot com
#
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------
"""
Database unit tests
"""

import pytest

from CtrlCraft.data import CtrlCraftData
from CtrlCraft.widgets.office_co3_import import CtrlCraftOfficeCO3Import
from CtrlCraft.widgets.office_slabs_import import CtrlCraftOfficeSlabsImport


def test_databaseImportSlab(ctrlcraftDb):  # pylint: disable=W0621
    """test slab import"""
    newSlabs, nlines = CtrlCraftOfficeSlabsImport.doImport("./samples/slab_sample.txt", ctrlcraftDb)
    assert nlines == 210
    assert newSlabs == 210


def test_databaseImportCo3(ctrlcraftDb):  # pylint: disable=W0621
    """test co3 import"""
    newCo3, nlines = CtrlCraftOfficeCO3Import.doImport("./samples/co3_sample.txt", ctrlcraftDb)
    assert nlines == 210
    assert newCo3 == 210


def test_databaseExport(ctrlcraftDb):  # pylint: disable=W0621
    """test database export"""
    customProj = [0, 0, 0]
    ctrlcraftDb.translate(customProj, inOut=False)

    if CtrlCraftData.isPgliteAvailable:
        ctrlcraftDb.export("/tmp/ctrlcraft_export.sql")
        ctrlcraftDb.translate(customProj)
        assert ctrlcraftDb.isValid()

    else:
        with pytest.raises(Exception):
            ctrlcraftDb.export("/tmp/ctrlcraft_export.sql")


def test_classThresholds(ctrlcraftDb):
    refThresholds = {
        0: {"min": -999, "max": -998.99, "checked": False},
        1: {"min": 3.5, "max": 5.7, "checked": True},
        2: {"min": 5.7, "max": 5.9, "checked": False},
        3: {"min": 5.9, "max": 7.6, "checked": True},
        4: {"min": 7.6, "max": 7.7, "checked": False},
        5: {"min": 7.7, "max": 8.4, "checked": True},
        6: {"min": 8.4, "max": 3000, "checked": True},
    }
    refColors = {
        0: {"color": "#FFFFFF"},
        1: {"color": "#B2B2B2"},
        2: {"color": "#00FF00"},
        3: {"color": "#FFE600"},
        4: {"color": "#FF0000"},
        5: {"color": "#0073FF"},
        6: {"color": "#A800FF"},
    }
    ctrlcraftDb.setClassThresholds(refThresholds)
    thresholds = ctrlcraftDb.getClassThresholds()

    for i in refThresholds:
        refThresholds[i].update(refColors[i])
    assert thresholds == refThresholds
