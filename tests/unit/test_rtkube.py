"""
Test file for rtkube, rtkube_dockwidget, and rtkube_serial modules.
Also testing different configurations of ini parameters files.
"""

import importlib
import time
from pathlib import Path

from pytest_mock import MockerFixture
from qgis.PyQt.QtWidgets import QApplication

from CtrlCraft.rtkube import rtkube, rtkube_dockwidget, rtkube_serial


def getSimuRtkube(mk, mc, database, probeNormalizationSuffix="ok"):
    """
    Return an instance of RTKube configured for simulation

    :param mk monkeypatch: the monkeypatch pytest fixture
    :param mc mocker: the mocker pytest fixture
    :paramm probe_normalization_suffix str: the suffix to choose the config file
    """

    # Activate simulation mode
    mk.setenv("SIMU", "1")
    importlib.reload(rtkube_serial)

    myRtkube = rtkube.RTKube(
        mc.Mock(),
        database,
        None,
        str(Path(__file__).parent.parent / "data" / "proj.ini"),
        str(Path(__file__).parent.parent / "data" / "gamma.ini"),
        str(Path(__file__).parent.parent / "data" / f"probe_normalization_{probeNormalizationSuffix}.ini"),
    )
    myRtkube.repainter = mc.Mock()
    myRtkube.init()
    return myRtkube


def getSimuRtkubeDock(mk, database):  # pylint:disable=invalid-name
    """
    Return an instance of RTKubeDockWidget configured for simulation

    :param mk monkeypatch: the monkeypatch pytest fixture
    """

    # Activate simulation mode
    mk.setenv("SIMU", "1")
    importlib.reload(rtkube_serial)

    rtkubeDock = rtkube_dockwidget.RTKubeDockWidget(
        str(Path(__file__).parent.parent / "data" / "probe_normalization_ok.ini"),
        database,
    )

    return rtkubeDock


def test_simu(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    rtkubeDock = getSimuRtkubeDock(monkeypatch, ctrlcraftDb)
    spy = mocker.spy(rtkubeDock, "dataToSMU")
    spyErrors = mocker.spy(rtkubeDock, "errors")

    # Set coefficients
    rtkubeDock.gammaCoeffs = 0.01, 0.02, 1.1, 1.2, 0.03, 0.04, 1.3, 1.4, 3.123, 3.456

    # Process for 5 seconds
    rtkubeDock.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    rtkubeDock.disconnection()

    spyErrors.assert_not_called()
    spy.emit.assert_called()


def test_simuNoCoeffs(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    An error should be shown to the user if there is a problem with the gamma coefficients
    """

    rtkubeDock = getSimuRtkubeDock(monkeypatch, ctrlcraftDb)
    spy = mocker.spy(rtkubeDock, "dataToSMU")
    spyErrors = mocker.spy(rtkubeDock, "errors")

    rtkubeDock.connection()
    for _ in range(3):
        QApplication.processEvents()
        time.sleep(1)
    rtkubeDock.disconnection()

    spy.emit.assert_not_called()
    spyErrors.assert_called_once_with(
        "Missing values (deposit or coefficient(s)), "
        "or wrong keywords, or wrong formula in gamma coefficients parameter file."
    )


def test_simuForceHighThreshold(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Force the radiometry computation to use the second coefficients (aUp2, bUp2, aLow2, bLow2)
    """

    rtkubeDock = getSimuRtkubeDock(monkeypatch, ctrlcraftDb)
    spy = mocker.spy(rtkubeDock, "dataToSMU")
    spyErrors = mocker.spy(rtkubeDock, "errors")

    # Set coefficients so that we compute radiometry values between 0 and 3500
    # and that it is always lower than the threshold
    rtkubeDock.gammaCoeffs = 0, 0, 0, 0, 1, 2, 1.1, 1.2, -1, -1

    rtkubeDock.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    rtkubeDock.disconnection()

    spyErrors.assert_not_called()
    spy.emit.assert_called()

    # Verify that we took the coeffs for the lower computation (coeffXXXX ending with a 1)
    for call in spy.emit.call_args_list:
        assert list(call.args)[-6:][:-2] == [1, 2, 1.1, 1.2]


def test_simuForceLowThreshold(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Force the radiometry computation to use the first coefficients (aUp1, bUp1, aLow1, bLow1)
    """

    rtkubeDock = getSimuRtkubeDock(monkeypatch, ctrlcraftDb)
    spy = mocker.spy(rtkubeDock, "dataToSMU")
    spyErrors = mocker.spy(rtkubeDock, "errors")

    # Set coefficients so that we compute radiometry values between 0 and 3500
    # and that it is always lower than the threshold
    rtkubeDock.gammaCoeffs = 1, 2, 1.1, 1.2, 0, 0, 0, 0, 9000000000, 9000000000

    rtkubeDock.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    rtkubeDock.disconnection()

    spyErrors.assert_not_called()
    spy.emit.assert_called()

    # Verify that we took the coeffs for the lower computation (coeffXXXX ending with a 1)
    for call in spy.emit.call_args_list:
        assert list(call.args)[-6:][:-2] == [1, 2, 1.1, 1.2]


def test_iniFilesOk(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Test with all ini files in the good formats, values, deposit
    """

    myRtkube = getSimuRtkube(monkeypatch, mocker, ctrlcraftDb)

    myRtkube.setDeposit("all_ok")
    assert myRtkube.widget.projDelta == (1, 2, 3)
    assert myRtkube.widget.gammaCoeffs == (0.23, 0.498, 1.43, 1.2, 0.222, 0.09, 1.51, 1.321, 250.0, 1250.0)

    spy = mocker.spy(myRtkube.widget, "dataToSMU")
    spyErrors = mocker.spy(myRtkube.widget, "errors")

    # Simulate
    myRtkube.widget.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    myRtkube.widget.disconnection()

    # No errors and data was sent
    spyErrors.assert_not_called()
    spy.emit.assert_called()


def test_gammaMissingDepositProjOk(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Test with a missing deposit in gamma file, everything else ok
    """

    warningMock = mocker.patch("CtrlCraft.rtkube.rtkube.QMessageBox.warning")
    myRtkube = getSimuRtkube(monkeypatch, mocker, ctrlcraftDb)

    myRtkube.setDeposit("gamma_missing_deposit_proj_ok")
    assert myRtkube.widget.projDelta == (4, 5, 6)
    assert myRtkube.widget.gammaCoeffs is None
    warningMock.assert_called_once_with(
        None,
        "Gamma coefficients error",
        "For deposit gamma_missing_deposit_proj_ok:\n"
        "Missing values (deposit or coefficient(s)), or wrong keywords, "
        "or wrong formula in gamma coefficients parameter file.",
    )

    spy = mocker.spy(myRtkube.widget, "dataToSMU")
    spyErrors = mocker.spy(myRtkube.widget, "errors")

    # Simulate
    myRtkube.widget.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    myRtkube.widget.disconnection()

    # We got the correct errors
    spyErrors.assert_called_once_with(
        "Missing values (deposit or coefficient(s)), or wrong keywords, "
        "or wrong formula in gamma coefficients parameter file."
    )
    # No data was sent
    spy.emit.assert_not_called()

    # Visual warnings are not displayed because proj is ok
    assert myRtkube.widget.depositWarning.isHidden()
    assert myRtkube.widget.XNSah.text() != "N/A"
    assert myRtkube.widget.YNSah.text() != "N/A"
    assert myRtkube.widget.ZNSah.text() != "N/A"
    assert myRtkube.widget.ZFixedNSah.text() != "N/A"


def test_projMissingDepositGammaOk(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Test with a missing deposit in proj file, everything else ok
    """

    myRtkube = getSimuRtkube(monkeypatch, mocker, ctrlcraftDb)

    myRtkube.setDeposit("proj_missing_deposit_gamma_ok")
    assert myRtkube.widget.projDelta is None
    assert myRtkube.widget.gammaCoeffs == (0.233, 0.49, 1.43, 1.255, 0.222, 0.09, 1.51, 1.321, 250.0, 1250.0)
    spy = mocker.spy(myRtkube.widget, "dataToSMU")
    spyErrors = mocker.spy(myRtkube.widget, "errors")

    # Simulate
    myRtkube.widget.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    myRtkube.widget.disconnection()

    # Everything is ok because gamma is ok
    spyErrors.assert_not_called()
    spy.emit.assert_called()

    # Visual warnings are displayed because proj is not ok
    assert myRtkube.widget.XNSah.text() == "N/A"
    assert myRtkube.widget.YNSah.text() == "N/A"
    assert myRtkube.widget.ZNSah.text() == "N/A"
    assert myRtkube.widget.ZFixedNSah.text() == "N/A"


def test_projMissingValueGammaOk(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Test with a missing value in the deposit of the proj file, everything else ok
    """

    myRtkube = getSimuRtkube(monkeypatch, mocker, ctrlcraftDb)

    myRtkube.setDeposit("proj_missing_value_gamma_ok")
    assert myRtkube.widget.projDelta is None
    assert myRtkube.widget.gammaCoeffs == (0.21, 0.49, 1.43, 1.233, 0.222, 0.09, 1.51, 1.321, 220.0, 1250.0)
    spy = mocker.spy(myRtkube.widget, "dataToSMU")
    spyErrors = mocker.spy(myRtkube.widget, "errors")

    # Simulate
    myRtkube.widget.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    myRtkube.widget.disconnection()

    # Everything is ok because gamma is ok
    spyErrors.assert_not_called()
    spy.emit.assert_called()

    # Visual warnings are displayed because proj is not ok
    assert myRtkube.widget.XNSah.text() == "N/A"
    assert myRtkube.widget.YNSah.text() == "N/A"
    assert myRtkube.widget.ZNSah.text() == "N/A"
    assert myRtkube.widget.ZFixedNSah.text() == "N/A"


def test_projBadValueGammaOk(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Test with a bad value in the deposit of the proj file, everything else ok
    """

    myRtkube = getSimuRtkube(monkeypatch, mocker, ctrlcraftDb)

    myRtkube.setDeposit("proj_bad_value_gamma_ok")
    assert myRtkube.widget.projDelta is None
    assert myRtkube.widget.gammaCoeffs == (0.53, 0.49, 1.43, 1.3, 0.222, 0.09, 1.51, 1.789, 250.0, 1250.0)
    spy = mocker.spy(myRtkube.widget, "dataToSMU")
    spyErrors = mocker.spy(myRtkube.widget, "errors")

    # Simulate
    myRtkube.widget.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    myRtkube.widget.disconnection()

    # Everything is ok because gamma is ok
    spyErrors.assert_not_called()
    spy.emit.assert_called()

    # Visual warnings are displayed because proj is not ok
    assert myRtkube.widget.XNSah.text() == "N/A"
    assert myRtkube.widget.YNSah.text() == "N/A"
    assert myRtkube.widget.ZNSah.text() == "N/A"
    assert myRtkube.widget.ZFixedNSah.text() == "N/A"


def test_gammaMissingParameterProjOk(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Test with a missing value in the deposit of the gamma file, everything else ok
    """

    warningMock = mocker.patch("CtrlCraft.rtkube.rtkube.QMessageBox.warning")
    myRtkube = getSimuRtkube(monkeypatch, mocker, ctrlcraftDb)

    myRtkube.setDeposit("gamma_missing_parameter_proj_ok")
    assert myRtkube.widget.projDelta == (13, 14, 15)
    assert myRtkube.widget.gammaCoeffs is None
    warningMock.assert_called_once_with(
        None,
        "Gamma coefficients error",
        "For deposit gamma_missing_parameter_proj_ok:\n"
        "Missing values (deposit or coefficient(s)), or wrong keywords, "
        "or wrong formula in gamma coefficients parameter file.",
    )
    spy = mocker.spy(myRtkube.widget, "dataToSMU")
    spyErrors = mocker.spy(myRtkube.widget, "errors")

    # Simulate
    myRtkube.widget.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    myRtkube.widget.disconnection()

    # We got the correct errors
    spyErrors.assert_called_once_with(
        "Missing values (deposit or coefficient(s)), or wrong keywords, "
        "or wrong formula in gamma coefficients parameter file."
    )
    # No data was sent
    spy.emit.assert_not_called()

    # Visual warnings are not displayed because proj is ok
    assert myRtkube.widget.depositWarning.isHidden()
    assert myRtkube.widget.XNSah.text() != "N/A"
    assert myRtkube.widget.YNSah.text() != "N/A"
    assert myRtkube.widget.ZNSah.text() != "N/A"
    assert myRtkube.widget.ZFixedNSah.text() != "N/A"


def test_gammaBadValueProjOk(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Test with a bad value in the deposit of the gamma file, everything else ok
    """

    warningMock = mocker.patch("CtrlCraft.rtkube.rtkube.QMessageBox.warning")
    myRtkube = getSimuRtkube(monkeypatch, mocker, ctrlcraftDb)

    myRtkube.setDeposit("gamma_bad_value_proj_ok")
    assert myRtkube.widget.projDelta == (10, 11, 12)
    assert myRtkube.widget.gammaCoeffs is None
    warningMock.assert_called_once_with(
        None,
        "Gamma coefficients error",
        "For deposit gamma_bad_value_proj_ok:\n"
        "Missing values (deposit or coefficient(s)), or wrong keywords, "
        "or wrong formula in gamma coefficients parameter file.",
    )
    spy = mocker.spy(myRtkube.widget, "dataToSMU")
    spyErrors = mocker.spy(myRtkube.widget, "errors")

    # Simulate
    myRtkube.widget.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    myRtkube.widget.disconnection()

    # We got the correct errors
    spyErrors.assert_called_once_with(
        "Missing values (deposit or coefficient(s)), or wrong keywords, "
        "or wrong formula in gamma coefficients parameter file."
    )
    # No data was sent
    spy.emit.assert_not_called()

    # Visual warnings are not displayed because proj is ok
    assert myRtkube.widget.depositWarning.isHidden()
    assert myRtkube.widget.XNSah.text() != "N/A"
    assert myRtkube.widget.YNSah.text() != "N/A"
    assert myRtkube.widget.ZNSah.text() != "N/A"
    assert myRtkube.widget.ZFixedNSah.text() != "N/A"


def test_normalizationMissingParameter(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Test with a missing parameter in the probe normalization file, everything else ok
    """

    myRtkube = getSimuRtkube(monkeypatch, mocker, ctrlcraftDb, "missing_parameter")

    myRtkube.setDeposit("all_ok")
    assert myRtkube.widget.projDelta == (1, 2, 3)
    assert myRtkube.widget.gammaCoeffs == (0.23, 0.498, 1.43, 1.2, 0.222, 0.09, 1.51, 1.321, 250.0, 1250.0)
    spy = mocker.spy(myRtkube.widget, "dataToSMU")
    spyErrors = mocker.spy(myRtkube.widget, "errors")

    # Simulate
    myRtkube.widget.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    myRtkube.widget.disconnection()

    # We got the correct errors
    spyErrors.assert_called_once_with(
        "N_sonde_haute or basse is missing in normalization parameter file for serial number 54321BA"
    )
    # No data was sent
    spy.emit.assert_not_called()

    # Visual warnings are not displayed because proj is ok
    assert myRtkube.widget.depositWarning.isHidden()
    assert myRtkube.widget.XNSah.text() != "N/A"
    assert myRtkube.widget.YNSah.text() != "N/A"
    assert myRtkube.widget.ZNSah.text() != "N/A"
    assert myRtkube.widget.ZFixedNSah.text() != "N/A"


def test_normalizationBadValue(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Test with a bad value in the probe normalization file, everything else ok
    """

    myRtkube = getSimuRtkube(monkeypatch, mocker, ctrlcraftDb, "bad_value")

    myRtkube.setDeposit("all_ok")
    assert myRtkube.widget.projDelta == (1, 2, 3)
    assert myRtkube.widget.gammaCoeffs == (0.23, 0.498, 1.43, 1.2, 0.222, 0.09, 1.51, 1.321, 250.0, 1250.0)
    spy = mocker.spy(myRtkube.widget, "dataToSMU")
    spyErrors = mocker.spy(myRtkube.widget, "errors")

    # Simulate
    myRtkube.widget.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    myRtkube.widget.disconnection()

    # We got the correct errors
    spyErrors.assert_called_once_with(
        "A parameter value is not a number in normalization parameter file for serial number 54321BA"
    )
    # No data was sent
    spy.emit.assert_not_called()

    # Visual warnings are not displayed because proj is ok
    assert myRtkube.widget.depositWarning.isHidden()
    assert myRtkube.widget.XNSah.text() != "N/A"
    assert myRtkube.widget.YNSah.text() != "N/A"
    assert myRtkube.widget.ZNSah.text() != "N/A"
    assert myRtkube.widget.ZFixedNSah.text() != "N/A"


def test_normalizationMissingSerialNumber(monkeypatch, mocker: MockerFixture, ctrlcraftDb):
    """
    Test with the wrong probe serial number in the probe normalization file, everything else ok
    """

    myRtkube = getSimuRtkube(monkeypatch, mocker, ctrlcraftDb, "missing_serial_number")

    myRtkube.setDeposit("all_ok")
    assert myRtkube.widget.projDelta == (1, 2, 3)
    assert myRtkube.widget.gammaCoeffs == (0.23, 0.498, 1.43, 1.2, 0.222, 0.09, 1.51, 1.321, 250.0, 1250.0)
    spy = mocker.spy(myRtkube.widget, "dataToSMU")
    spyErrors = mocker.spy(myRtkube.widget, "errors")

    # Simulate
    myRtkube.widget.connection()
    for _ in range(5):
        QApplication.processEvents()
        time.sleep(1)
    myRtkube.widget.disconnection()

    # We got the correct errors
    spyErrors.assert_called_once_with("Gamma probe serial number 54321BA is missing in normalization parameter file")
    # No data was sent
    spy.emit.assert_not_called()

    # Visual warnings are not displayed because proj is ok
    assert myRtkube.widget.depositWarning.isHidden()
    assert myRtkube.widget.XNSah.text() != "N/A"
    assert myRtkube.widget.YNSah.text() != "N/A"
    assert myRtkube.widget.ZNSah.text() != "N/A"
    assert myRtkube.widget.ZFixedNSah.text() != "N/A"
