# CtrlCraft

[![pipeline status](https://gitlab.com/Oslandia/qgis/ctrlcraft/badges/master/pipeline.svg)](https://gitlab.com/Oslandia/qgis/ctrlcraft/-/commits/master)
[![documentation badge](https://img.shields.io/badge/documentation-autobuilt%20with%20Sphinx-blue)](https://oslandia.gitlab.io/qgis/ctrlcraft/)
[![pylint](https://oslandia.gitlab.io/qgis/ctrlcraft/lint/pylint.svg)](https://oslandia.gitlab.io/qgis/ctrlcraft/lint/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Plugin for displaying mines, Original idea by Emmanuel Duguey.

# Documentation

User guide (French and English), technical documentation (French), and contributing guide can be found at this address : https://oslandia.gitlab.io/qgis/CtrlCraft/
