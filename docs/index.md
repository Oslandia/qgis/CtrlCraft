# {{ title }} - Documentation

> **Description:** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

----

```{toctree}
---
caption: Usage
maxdepth: 1
---
Installation <usage/installation>
User guide (EN) <usage/user_guide_en>
Guide d'utilisation (FR) <usage/user_guide_fr>
```

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
development/documentation
development/translation
development/testing
development/history
```

- [Technical documentation](https://oslandia.gitlab.io/qgis/CtrlCraft/docs_tech/index.html)
- [PDF documentation (EN)](https://oslandia.gitlab.io/qgis/CtrlCraft/docs_pdf_en/CtrlCraft%20-%20plugin%20for%20displaying%203D%20mines.pdf)
- [PDF documentation (FR)](https://oslandia.gitlab.io/qgis/CtrlCraft/docs_pdf_fr/CtrlCraft%20-%20plugin%20for%20displaying%203D%20mines.pdf)
