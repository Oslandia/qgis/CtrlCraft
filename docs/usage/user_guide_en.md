# User guide

CtrlCraft comes in 4 different versions:

- CtrlCraft Office
- CtrlCraft Mobile
- CtrlCraft Office Admin
- CtrlCraft Mobile Admin

Mobile versions are light versions with larger icons and minialist tools.  
Office versions are full versions with all the tools.  
Admin versions must be used to change specific CtrlCraft parameters.

## Interface

### Office

1. Main map
2. Menu bar
3. CtrlCraft toolbar
4. CtrlCraft charts and table toolbar
5. Layers panel
6. Radiometry panel
7. K2 panel

![](../images/interface.png)

### Mobile

With CtrlCraft Mobile version, the only available menu is CtrlCraft menu, and the charts and table toolbar is not available.

![](../images/interface_mobile.png)

## CtrlCraft Toolbar

The first elements are the selection of the current deposit, panel, and blast:

![](../images/comboboxes.png)

Then the following icons:

| icon | description |
| --- | --- |
| ![](../../CtrlCraft/img/iconUndoSlab.png){w=30px} | Unload the last slab loaded in a truck |
| ![](../../CtrlCraft/img/iconAffectRadioSlab.png){w=30px} | Perform a fixed measurement and affect the radiometry to the slab |
| ![](../../CtrlCraft/img/iconAddLandmark.png){w=30px} | Add a landmark point (K2 connection must **not** be activated) |
| ![](../../CtrlCraft/img/iconRemoveLandmark.png){w=30px} | Remove the last landmark point |
| ![](../../CtrlCraft/img/iconClearLandmarks.png){w=30px} | Remove all landmark points |
| ![](../../CtrlCraft/img/iconLithoSlab_argile.png){w=30px} | Affect `Argile` lithology to the slab |
| ![](../../CtrlCraft/img/iconLithoSlab_gres.png){w=30px} | Affect `Grès` lithology to the slab |
| ![](../../CtrlCraft/img/iconLithoSlab_argile_oxyde.png){w=30px} | Affect `Argile Oxydé` lithology to the slab |
| ![](../../CtrlCraft/img/iconLithoSlab_gres_oxyde.png){w=30px} | Affect `Grès Oxydé` lithology to the slab |
| ![](../../CtrlCraft/img/iconLithoSlab_nd.png){w=30px} | Affect `ND` lithology to the slab |
| ![](../../CtrlCraft/img/icon_select_truck.png){w=30px} | Select a truck for loading the slabs into |
| ![](../../CtrlCraft/img/icon_truck.png){w=30px} | Active the slab loading tool |
| ![](../../CtrlCraft/img/icon_truck_empty.png){w=30px} | Unload the selected truck |

The last icon represents the color of the current radiometry class of the truck.

## Charts and tables toolbar

| icon | description |
| --- | --- |
| ![](../../CtrlCraft/img/iconOffice.png){w=30px} | Open office shipment table |
| ![](../../CtrlCraft/img/iconOfficeChart.png){w=30px} | Open office shipment chart |

## Layers panel

`Landmark points` is a temporary layer. The landmark points added with the dedicated buttons on the toolbar are added in this layer.

A layer group named after the selected deposit contains all important layers:
- `co3`: shows the imported co3 data
- `topo_control`: shows the highest point amongst the measurements, and is labelled with the difference between the theoretical and the measured
  altitude, alongside the actual level it corresponds to
- `K2`: shows the last measurements points taken with the sensor
- the layer named after the selected panel and blast: shows a top view of the slabs
- `levels`: not used in the main map, but for the atlas export feature

The K2 and the slabs layers are color-coded after the radiometry class intervals defined (see {term}`Define slab threshold symbology`)

![](../images/layers_panel.png)

## Slab grade panel

This panel shows the radiometries values for the current slab where the GNSS is situated.

1. Estimated radiometry
2. Measured radiometry retrieved from fixed measurements

![](../images/radiometry_panel.png)

## K2 panel

While the K2 is connected, one measurement is made every second.

1. Tab to choose between GNSS coordinates data or Gamma data

### GNSS tab

2. Time and date from the sensor
3. Tab to choose between 3 different projections, and show coordinates
4. Data precision information
5. Z coordinate from lower sensor (always visible) matching the selected projections tab
6. Connection/Disconnection button

![](../images/rtkube_panel_gnss.png)

### Radiometry tab

1. Radiometry section with upper and lower sensors
2. Raw measurements section with upper and lower sensors
3. Current radiometry class and color
4. Current inclination
5. Z coordinate from lower sensor (always visible) matching the selected projections tab
6. Connection/Disconnection button

![](../images/rtkube_panel_gamma.png)

## Parameters panel

Right clic in a grey zone to activate CtrlCraft Parameters panel.

![](../images/activate_panels.png)

Editing a parameter must be saved by hitting the `Save` button. The project is then completely reloaded.

### Truck tab

Choose the number of trucks, their name (alphanumeric upper case only), and the number of slabs they can have.

### Rover tab

Choose the GPS mode and the acquisition max angle value.

### Gamma tab (admin mode only)

To edit normalization and correlation coefficients used to compute radiometry from gamma values.

![](../images/gamma_parameters.png)

#### Normalisation

It is mandatory to respect the following syntax:

```ini
[Probe serial number]
N_sonde_haute=1.32
N_sonde_basse=0.984
```

For example:

![](../images/gamma_dialog_normalization.png)

#### Correlation

It is mandatory to respect the following syntax:

```ini
[Deposit name]
teneur_sonde_haute_1=0.230 - normalized cps ^ 1.43
teneur_sonde_haute_2=0.222 * normalized cps ^ 1.51
seuil_cps_sonde_haute=250

Teneur_sonde_basse_1=0.490 * normalized cps ^ 1.2
Teneur_sonde_basse_2=0.090 * normalized cps ^ 1.321
seuil_cps_sonde_basse=1250
```

The syntax must respect `*` and `^` signs, and the parameter names.

The thresholds correspond to a measurement value (normalized) below (strictly) which the parameters marked `_1` are used, and below (or equal) which the
parameters marked `_2` are used.

For example :

![](../images/gamma_dialog_correlation_en.png)

### Communication tab

Choose the pivot, base, and spare bluetooth ports.

### Projection tab (admin mode only)

Go to edit the custom projection (NSah) translations

CtrlCraft works in WGS 84 / UTM zone 32N ([EPSG:32632](https://epsg.io/32632)).

When objects are imported with the dedicated [menus](#ctrlcraft-menus) ({term}`Import CO3`, {term}`Import slabs`)
a geographical translation is performed if the imported objects' deposit is present in these translations parameters.

X, Y, and Z values are added to the imported csv file coordinates (unit = meter).


It is mandatory to respect the following syntax:

```ini
[Deposit name]
x=1
y=2
z=3
```

For instance:

![](../images/translations_dialog.png)

## CtrlCraft menus

![](../images/menus.png)

{.glossary}
Open Project
: Open CtrlCraft project (automatic when starting CtrlCraft)

Reset Project
: All styling and radiometry class thresholds are cleared

Clear Database
: All data (slabs, blasts, panels, deposits, measurements, etc.) are cleared

Define slab threshold symbology
: Set radiometry values intervals for classes

Import Slabs
: Import slabs data from a csv file

Import CO3
: Import CO3 data from a csv file

Filter levels
: Select a level number to hide all above levels

Remove levels
: Select a level to be removed from the blast (can be reverted)

Revert removed levels
: Select a removed level to be re-added

Import Database
: Select a dump file to completely replace the current database with the selected one (slabs, blasts, panels, deposits, measurements, etc.)

Export Database
: Export the current database in a dump file (it can be imported in another computer or in a centralized database)

Export Atlas PDF
: Export a PDF document atlas, each page corresponding to a level

Exit QGIS
: Exit CtrlCraft
