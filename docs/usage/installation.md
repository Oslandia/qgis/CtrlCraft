# Installation

## Windows only: with an installer

**IMPORTANT:** only the major versions include the dependencies (QGIS, PostgreSQL, PostGIS, etc.).

For an **installation from scratch** you will always need to **download** and install the **lastest major** version, then **download** and install the **latest minor** version.

For an **update** which does **not change the major version**, you can download and install the lastest version with the same major version.

For an **update** which changes the major version, you need to do as an **installation from scratch**.

Go to the [release page](https://gitlab.com/Oslandia/qgis/CtrlCraft/-/releases) and download the installer(s) from the release resources *Packages* section.

Example of versionning numbers: `version 4.5.6` has **4** as a **major version**, 5 as a minor version, 6 as a patch version.

## From the plugin zip file:

1. Open the plugin manager

   ![](../images/install_1.png)

1. Go to the `Install from ZIP` section
1. Choose the plugin zip file and clic `Install Plugin`

   ![](../images/install_2.png)
