# Guide d'utilisation

CtrlCraft dispose de 4 versions différentes:

- CtrlCraft Office
- CtrlCraft Mobile
- CtrlCraft Office Admin
- CtrlCraft Mobile Admin

Les versions mobiles sont allégées, avec des iônes plus larges et moins d'outils.  
Les versions Office sont complètes.  
Les versions Admin doivent être utilisées pour changer certains paramètres spécifiques de CtrlCraft.  

## Interface

### Office

1. Carte principale
2. Barre des menus
3. Barre d'outils CtrlCraft
4. Barre d'outils CtrlCraft d'outils de suivi (tableaux et graphiques)
5. Panneau des couches
6. Panneau teneur des slabs
7. Panneau K2

![](../images/interface.png)

### Mobile

Avec la version CtrlCraft Mobile, le seul menu disponible est le menu CtrlCraft, et la barre d'outils de suivi n'est pas disponible.

![](../images/interface_mobile.png)

## Barre d'outils CtrlCraft

Les premiers éléments permettent la sélection du gisement, panneau, et tir:

![](../images/comboboxes.png)

Les icônes suivante :

| icon | description |
| --- | --- |
| ![](../../CtrlCraft/img/iconUndoSlab.png){w=30px} | Décharger le dernier slab chargée dans le camion |
| ![](../../CtrlCraft/img/iconAffectRadioSlab.png){w=30px} | Effectuer une mesure fixe et affecter la radiométrie au slab |
| ![](../../CtrlCraft/img/iconAddLandmark.png){w=30px} | Ajouter un point de repère (la connexion K2 ne doit **pas** être activée) |
| ![](../../CtrlCraft/img/iconRemoveLandmark.png){w=30px} | Enlever le dernier point de repère |
| ![](../../CtrlCraft/img/iconClearLandmarks.png){w=30px} | Effacer tous les points de repère |
| ![](../../CtrlCraft/img/iconLithoSlab_argile.png){w=30px} | Affecter la lithologie `Argile` au slab |
| ![](../../CtrlCraft/img/iconLithoSlab_gres.png){w=30px} | Affecter la lithologie `Grès` au slab |
| ![](../../CtrlCraft/img/iconLithoSlab_argile_oxyde.png){w=30px} | Affecter la lithologie `Argile Oxydé` au slab |
| ![](../../CtrlCraft/img/iconLithoSlab_gres_oxyde.png){w=30px} | Affecter la lithologie `Grès Oxydé` au slab |
| ![](../../CtrlCraft/img/iconLithoSlab_nd.png){w=30px} | Affecter la lithologie `ND` au slab |
| ![](../../CtrlCraft/img/icon_select_truck.png){w=30px} | Sélectionner un camion pour le chargement des slabs |
| ![](../../CtrlCraft/img/icon_truck.png){w=30px} | Activer l'outil de chargement de slab |
| ![](../../CtrlCraft/img/icon_truck_empty.png){w=30px} | Décharger le camion sélectionné |

La dernière icône représente la couleur de la classe de radiométrie du camion.

## Barre d'outils de tri (tableaux et graphiques)

| icon | description |
| --- | --- |
| ![](../../CtrlCraft/img/iconOffice.png){w=30px} | Ouvrir le tableau des cargaisons |
| ![](../../CtrlCraft/img/iconOfficeChart.png){w=30px} | Ouvrir le graphique des cargaisons |

## Panneau des couches

`Points de repère` est une couche temporaire. Les points de repère ajoutés avec le bouton dédié de la barre d'outils sont ajoutés dans cette couche.

Un groupe de couche avec le nom du gisement sélectionné contient toutes les couches importantes :
- `co3`: les données CO3 importées
- `topo_control`: montre le plus haut point de mesure, et affiche la différence entre l'altitude théorique et mesurée, avec le niveau correspondant
- `K2`: montre les derniers points de mesure relevés avec le capteur
- la couche avec le nom du panneau et tir sélectionné correspond à une vue zénithale des slabs
- `levels`: non utilisé dans la carte principale, mais est utile pour la fonctionnalité d'atlas

Les couches K2 et les slabs sont colorisés selon les classes de radiométrie dont les intervalles sont définis via le menu dédié (voir {term}`Définir la symbologie des slabs`)

![](../images/layers_panel.png)

## Panneau teneur des slabs

Ce panneau montre les valeurs de radiométrie du slab où se situe actuellement le capteur GNSS.

1. Radiométrie estimée
2. Radiométrie déterminée à partir des mesures fixes

![](../images/radiometry_panel.png)

## Panneau K2

Tant que le K2 est connecté, une mesure est effectuée chaque seconde.

1. Onglet pour sélectionner la vue coordonnées GNSS ou données Gamma

### GNSS tab

2. Date et heure du capteur
3. Onglet de projection, 3 projections disponibles pour voir les coordonnées
4. Informations de précision sur la donée
5. Coordonnée Z sonde basse (toujours visible) correspondant à l'onglet des projections sélectionné
6. Bouton de connexion/déconnexion

![](../images/rtkube_panel_gnss.png)

### Onglet données Gamma

1. Section Teneur avec sondes haute et basse
2. Section Coups Par Seconde avec sondes haute et basse
3. Radiométrie actuelle (couleur et classe)
4. Inclinaison actuelle
5. Coordonnée Z sonde basse (toujours visible) correspondant à l'onglet des projections sélectionné
6. Bouton de connexion/déconnexion

![](../images/rtkube_panel_gamma.png)

## Panneau paramètres

Cliquer droit dans une zone grise pour activer le panneau de paramètres.

![](../images/activate_panels.png)

L'édition d'un paramètre doit être sauvegardée en cliquant sur le bouton `Sauvegarder`. Le projet est alors complètement rechargé.

### Onglet Camions

Choisir le nombre de camions, leur nom (caractères alphanumériques en majuscule), et le nombre de slabs qu'ils peuvent avoir.

### Onglet Pivot

Choisir le mode GPS et l'angle maximum d'acquisition.

### Onglet Gamma

Il permet d'éditer les coefficients de normalisation et de corrélation utilisés pour le calcul de la teneur.

![](../images/gamma_parameters.png)

#### Normalisation

Il est nécessaire de bien respecter la syntaxe suivante :

```ini
[Numéro de série de la sonde]
N_sonde_haute=1.32
N_sonde_basse=0.984
```

Par exemple :

![](../images/gamma_dialog_normalization.png)

#### Corrélation

Il est nécessaire de bien respecter la syntaxe suivante :

```ini
[Nom du gisement]
teneur_sonde_haute_1=0.230 - cps normalisé ^ 1.43
teneur_sonde_haute_2=0.222 * cps normalisé ^ 1.51
seuil_cps_sonde_haute=250

Teneur_sonde_basse_1=0.490 * cps normalisé ^ 1.2
Teneur_sonde_basse_2=0.090 * cps normalisé ^ 1.321
seuil_cps_sonde_basse=1250
```

La syntaxe doit respecter les signes `*` et `^`, et les noms des paramètres.

Les seuils correspondent à une valeur de mesure (normalisée) en-dessous (strictement) de laquelle les paramètres marqués `_1` sont utilisés, et au-dessus (ou égal)
de laquelle les paramètres marqués `_2` sont utilisés.

Par exemple :

![](../images/gamma_dialog_correlation.png)

### Onglet Ports de communication

Choisir les ports bluetooth pour le pivot, la base, et le module de secours.

### Onglet Paramètres de projection (mode admin seulement)

Pour éditer les translations de projection NSah.

CtrlCraft fonctionne en projection WGS 84 / UTM zone 32N ([EPSG:32632](https://epsg.io/32632)).

Lors de l'importation d'objets via les [menus](#menus-ctrlcraft) dédiés ({term}`Importer carbonates`,
{term}`Importer des slabs`), une translation géographique est effectuée
si le gisement des objets importés est présent dans ces paramètres de translations.

Les valeurs de X, Y, et Z sont ajoutées aux coordonnées du fichier csv importé (unité = mètre).

Il est nécessaire de bien respecter la syntaxe suivante :

```ini
[Nom du gisement]
x=1
y=2
z=3
```

Par exemple :

![](../images/translations_dialog.png)

## Menus CtrlCraft

![](../images/menus_fr.png)

{.glossary}
Ouvrir projet
: Ouvrir le projet CtrlCraft (automatique à l'ouverture)

Réinitialiser projet
: Tous les styles et les intervalles de classes de radiométrie sont réinitialisés

Effacer la base de données
: Toutes les données (slabs, tirs, panneaux, gisements, mesures, etc.) sont effacées

Définir la symbologie des slabs
: Définir les intervalles de valeur de radiométrie pour les classes

Importer des slabs
: Importer des données de slabs à partir d'un fichier csv

Importer carbonates
: Importer des données de carbonates à partir d'un fichier csv

Filtre de niveau
: Sélectionner un numéro de niveau pour cacher tous les niveaux au-dessus

Supprimer les niveaux
: Sélectionner un niveau à supprimer du tir (réversible)

Revenir sur les niveaux supprimés
: Sélectionner un numéro de niveau à ré-ajouter

Importer une base de données
: Sélectionner un fichier dump pour remplacer complètement la base de données actuelle par celle sélectionnée (slabs, tirs, panneaux, gisements, mesures, etc.)

Exporter la base de données
: Exporter la base de données actuelle dans un fichier dump (qui pourra être importé dans un autre ordinateur ou base de données centralisée)

Export de l'atlas en PDF
: Exporter un document d'atlas au format PDF, chaque page correspondant à un niveau

Fermer QGIS
: Fermer CtrlCraft
