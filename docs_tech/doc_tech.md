---
title: |
    CtrlCraft

    Documentation technique
subtitle: Python, PostgreSQL, QGIS
author: Jacky Volpes
otherlogo: ./images/orano_ctrlcraft_logo.png
date: 20 novembre 2024
---

\newpage

# Fonctionnement général

CtrlCraft est un plugin QGIS écrit en Python.

Il est open source et peut être trouvé à cette adresse : [https://gitlab.com/Oslandia/qgis/CtrlCraft](https://gitlab.com/Oslandia/qgis/CtrlCraft).

Il utilise une base de données PostgreSQL, via le package Python [pglite](https://github.com/Oslandia/pglite).

Pour faciliter son installation, il est packagé entièrement dans un installateur contenant QGIS, PostgreSQL, et autres dépendances Python tierces (voir section [Packaging]).


# Décalage de projection géographique

CtrlCraft stocke les décalages entre la projection Nord Sahara et WGS84 projeté dans un fichier **proj.ini** stocké dans le dossier d'installation du plugin dans la tablette
(typiquement `C:\osgeo4w64\apps\qgis-ltr\python\plugins\CtrlCraft\proj.ini`) au format suivant :

```
[Deposit_name]
x=10
y=-330
z=3

[Deposit_name_2]
x=15
y=-275
z=7
```

Lors de l'import d'un fichier SURPAC (menu utilisateur "Importer des slabs"), CtrlCraft vérifie si le gisement importé est présent dans le fichier **proj.ini**. Si c'est le cas une translation est appliquée sur
toutes les coordonnées : on ajoute les valeurs de translation aux coordonnées importées.

$$
X/Y/Z_{WGS84} = X/Y/Z_{Nord Sahara} + X/Y/Z_{translation}
$$

\newpage

# Packaging

Le packaging de CtrlCraft est effectué via la méthode de l'OSGeo4W, agrémentée d'une surcouche développée par Oslandia pour la dépendance avec PostGIS et PostgreSQL.

Le script `make_installer.sh` package CtrlCraft et toutes ses dépendences dans un fichier exe Windows d'installation pouvant alors être utilisé hors ligne.

## Création de l'installateur

Il faut éditer `make_installer.sh` pour paramétrer `CTRLCRAFT_ONLY`.

Mettre `CTRLCRAFT_ONLY` à 1 permet de ne packager que le plugin CtrlCraft, et créer un installateur qui ne fera que la mise à jour du plugin.  
Mettre `CTRLCRAFT_ONLY` à 0 permet de packager toutes les dépendances de CtrlCraft (QGIS LTR compris).

:::info
Note: pour faciliter le packaging à répétition (lors de tests par exemple), il est possible de mettre les fichiers téléchargés dans un dossier `cache`
situé dans le dossier `packaging`.
:::

## Post-install

Le dossier `packaging` ne fait pas partie du plugin, mais une majorité de son contenu est copié dans le paquet pour être mis
dans le dossier du plugin sur le poste où le déploiement est fait. Ceci pour gérer l'action postinstall, qui va créer des raccourcis
sur le bureau ainsi que lancer l'initialisation de postgresql.

De même, les scripts de lancement .bat de CtrlCraft, et les fichiers de configuration, y sont copiés.

## Déploiement continu

Un pipeline de déploiement continu est effectué par GitLab à chaque tag, et à chaque requête de fusion.

L'installateur est téléversé dans le [registre de paquet](https://gitlab.com/Oslandia/qgis/CtrlCraft/-/packages),
et ajouté à la [page de release](https://gitlab.com/Oslandia/qgis/CtrlCraft/-/releases) si c'est un tag.

![](./images/packaging.png)

:::warning
Le lien suivant est permannent et permet de télécharger la dernière version de CtrlCraft :
:::
[https://gitlab.com/Oslandia/qgis/CtrlCraft/-/releases/permalink/latest/downloads/CtrlCraft_installer](https://gitlab.com/Oslandia/qgis/CtrlCraft/-/releases/permalink/latest/downloads/CtrlCraft_installer)


# Traduction

CtrlCraft est disponible en anglais et en français, gérée par le système d'internationalisation de PyQt.

1. Les éléments d'interface dans le plugin sont écrits en anglais
2. Se placer dans le dossier `CtrlCraft/i18n/`
3. Mettre à jour les traductions manquantes avec

   ```bash
   pylupdate5 CtrlCraft.pro
   ```
4. Traduire les traductions manquantes avec un logiciel de traduction type **linguist**
5. Compiler les nouvelles traductions avec

   ```bash
   lrelease CtrlCraft_fr.ts
   ```

# Mesures

Tant que la connexion avec le K2 est active, CtrlCraft "tire" une nouvelle trame du K2 chaque seconde. Ces informations sont stockées dans la
table `ctrlcraft.probe` (voir détails sur cette table en [annexes]).

Lorsque l'utilisateur déclenche la fonctionnalité "Appliquer la teneur à un slab" représentée par cette icône ![](../CtrlCraft/img/iconAffectRadioSlab.png){width=40px}

1. une nouvelle mesure fixe est enregistrée dans la table `ctrlcraft.fixed_measure` contenant la moyenne des 5 mesures les plus récentes de la table `ctrlcraft.probe`
2. cette mesure fixe et associée au slab le plus au-dessus de la pile de slabs situé sous les mesures
2. la valeur de radiométrie du slab est mise à jour selon la formule suivante :

$$
\frac{
  \sum_{M_{up}}  { \left( a_{up}  \cdot \left( \frac{1}{Ni_{up}}  \cdot \sum_{Ni_{up}}  { \left( \gamma_{i_{up}}  \right) } \cdot n_{up}  \right)^{ b_{up}  } \right) }
+ \sum_{M_{low}} { \left( a_{low} \cdot \left( \frac{1}{Ni_{low}} \cdot \sum_{Ni_{low}} { \left( \gamma_{i_{low}} \right) } \cdot n_{low} \right)^{ b_{low} } \right) }
}{M_{up} + M_{low}}
$$

Avec

- $\gamma_{i_{[up/low]}}$ = la mesure du capteur corrigée du temps mort
- $a_{[up/low]}$ = le coefficient de corrélation multiplicatif
- $b_{[up/low]}$ = le coefficient de corrélation logarithmique
- $n_{[up/low]}$ = le coefficient de normalisation
- $M_{[up/low]}$ = le nombre de mesures fixes
- $Ni_{[up/low]}$ = le nombre de mesures probe pour la mesure fixe i

La correction du temps mort est effectuée directement après la réception de la mesure du capteur. C'est donc la mesure corrigée du temps mort qui
est enregistrée en base de données. La formule appliquée est la suivante :

$$
\gamma_{corrigée} = \frac{ \gamma }{ 1 - \gamma \cdot 0.000005 }
$$

**Attention :** les coefficients dépendent de la valeur de la mesure et de la sonde utilisée (voir section ci-dessous).

## Fichier de paramètres

Le coefficient de normalisation $n_{[up/low]}$ dépend de la sonde gamma et est paramétré dans le fichier de configuration `probe_normalization.ini`.

Chaque section correspond à un numéro de série de sonde gamma.

```ini
[54321BA]
N_sonde_haute=3
N_sonde_basse=1
```

Les coefficients de corrélation $a_{[up/low]}$ et $b_{[up/low]}$ dépendent d'un seuil de mesure et sont paramétrés dans le fichier de configuration `gamma.ini`.

```ini
[TAOSSA]
teneur_sonde_haute_1=0.230 * cps normalisé ^ 1.43
teneur_sonde_haute_2=0.222 * cps normalisé ^ 1.51
seuil_cps_sonde_haute=250

Teneur_sonde_basse_1=0.490 * cps normalisé ^ 1.2
Teneur_sonde_basse_2=0.090 * cps normalisé ^ 1.321
seuil_cps_sonde_basse=1250
```

**À noter :** ces fichiers **doivent** être renseignés car il n'y a aucune valeur par défaut.


## Mode dégradé

Lorsque la connexion ne peut par être établie via le port du pivot, CtrlCraft tente de se connecter via le port du module de secours.
Si cette dernière connexion est réussie, CtrlCraft passe en mode dégradé, sinon un bandeau d'erreur est affiché.

Le module de secours n'a pas de GPS, donc en mode dégradé CtrlCraft demande à l'utilisateur de pointer le slab auquel il souhaite associer la mesure.

\newpage

# Plugin Python

CtrlCraft est un plugin à QGIS codé en Python.

Le dossier du plugin à proprement parler est le dossier [CtrlCraft](https://gitlab.com/Oslandia/qgis/CtrlCraft/-/tree/master/CtrlCraft/) situé dans le dépôt.

## Fichiers projet

Dans le dossier [qgis](https://gitlab.com/Oslandia/qgis/CtrlCraft/-/tree/master/CtrlCraft/qgis) se trouvent les fichiers propres à QGIS :

- les styles de couches pour l'atlas, les slabs, les mesures
- le fichier projet `ctrlcraft.qgs` qui est le projet ouvert lorsque CtrlCraft est ouvert, et peut être enregistré
- le fichier projet `template.qgs` qui est le projet de base utilisé pour revenir à une version originale du projet `ctrlcraft.qgs`

## Fichiers templates

Les fichiers en *.sample comme `proj.ini.sample` et `gamma.ini.sample` sont des exemples de fichiers de configuration.

Pour que CtrlCraft fonctionne, il faut les dupliquer et les renommer en enlevant `.sample`, et les remplir avec le contenu adéquat.
Le contenu de base est fonctionnel, et ils sont mis à jour via les fonctionnalité du panneau de paramètres
CtrlCraft du plugin, disponible parmi les panneaux d'interface de QGIS et mis à disposition par le plugin.

## Dépendances

CtrlCraft nécessite plusieurs dépendances, listées dans le répertoire [requirements](https://gitlab.com/Oslandia/qgis/CtrlCraft/-/tree/master/requirements).

- matplotlib pour l'affichage des graphes de suivi
- pglite pour gérer postgresql
- psycopg2 pour interfacer le plugin avec la base de données
- pyserial pour l'interface bluetooth

Ces dépendances sont embarquées lors du packaging (voir section [Packaging]).

# Simulation

CtrlCraft dispose de plusieurs modes de simulation du capteur.

La variable d'environnement `SIMU` peut être renseignée avec un chiffre pour activer le mode correspondant :

- 1 : les mesures sont reçues dans un intervalle de coordonnées géographique déterminé correspondant à un slab de
  l'[échantillon de slabs](https://gitlab.com/Oslandia/qgis/CtrlCraft/-/blob/master/samples/slab_sample.txt).
- 2 : les mesures GPS lon/lat/alt sont envoyées avec des valeurs à -1, ce qui déclenche le mode dégradé (voir section [Mode dégradé])
- 3 : les mesures GPS sont simulées comme pour la méthode 1 mais les mesures gamma sont envoyées avec des valeurs à -1
- 4 : les mesures sont simulées comme pour la méthode 1, mais au bout de 10 secondes les mesures gamma restent identiques


# Base de données

CtrlCraft utilise une base de données PostgreSQL locale sur l'ordinateur sur lequel il est installé.

La base de données est nommée **ctrlcraft** et toutes les données sont dans le schéma nommé **ctrlcraft**.

Le code SQL est disponible sur le dépôt GitLab à [cette adresse](https://gitlab.com/Oslandia/qgis/CtrlCraft/-/blob/master/CtrlCraft/utils/ctrlcraft.sql).

Le schéma de la structure de données est présenté dans les sections suivantes, le détail est en [annexes].

\newpage

## Tables

Les tables suivantes sont utilisées par CtrlCraft dans la vue cartographique :

- `co3` est la couche SIG des co3 importés

#### Schéma relationnel

![](./images/db_diagram_tables.png)

## Vues

Les vues suivantes sont utilisées par CtrlCraft dans la vue cartographique :

- `slabs_available` est la couche SIG qui montre les slabs du gisement sélectionné, c'est une vue filtrée de la table `slabs`
- `probe_last_positions` est la couche SIG qui montre les derniers points de mesure enregistrés lorsque le K2 est connecté, c'est une vue filtrée de la table `probe`
- `topo_control` affiche le point de mesure de la vue `probe_last_positions` avec l'altitude la plus élevée, avec la différence entre l'altitude mesurée et l'altitude théorique de la vue `levels`

La vue suivante est générée par une fonction SQL :

- `truck_loads_status` est généré par `generate_truck_load_status_view` qui prend en paramètre les intervalles de classe de teneur à donner

#### Schéma

![](./images/db_diagram_views.png)

\newpage

## Types

Le type `lithology` est un ENUM contenant les valeurs suivantes :

- Argile
- Grès
- Argile Oxydé
- Grès Oxydé
- ND

\newpage


# ANNEXES

## Tables de la base de données

### ctrlcraft.slabs

| champ | description |
| --- | ---------- |
| id | UUID (Universal Unique IDentifier) pour identifier le slab |
| unit | le nom de l'unité auquel le slab appartient* |
| deposit | le nom du gisement auquel le slab appartient* |
| panel | le nom du panel auquel le slab appartient* |
| blast | le nom du tir auquel le slab appartient |
| smu | l'identifiant (clé étrangère) vers la table `smu` auquel le slab appartient |
| load | l'identifiant vers la table `truck_loads` (renseigné lorsque le slab est chargé) |
| radiometry | la radiométrie estimée* |
| radiometry_probe | la radiométrie mesurée (renseignée après qu'une mesure fixe ait été effectuée) |
| lithology |  la lithologie* |
| geom | la géométrie MultiPolygonZ en WGS84 projeté (EPSG:32632) |
| centroid_z | la coordonnée Z du centroïde du slab |
| affectedradio | un indicateur sous la forme "N-M" où N est le nombre de mesures fixes avec sonde haute et M avec sonde basse |

*champ renseigné au moment de l'import de slabs de fichiers SURPAC

\newpage

### ctrlcraft.smus

| champ | description |
| --- | --------------- |
| id | UUID (Universal Unique IDentifier) pour identifier le SMU |
| smu_id | le nom du SMU* |
| unit | le nom de l'unité auquel le SMU appartient* |
| deposit | le nom du gisement auquel le SMU appartient* |
| panel | le nom du panel auquel le SMU appartient* |
| blast | le nom du tir auquel le SMU appartient* |
| radiometry | la radiométrie estimée* |
| lithology |  la lithologie* |
| percentage |  le pourcentage* |
| trans_[x/y/z] | la translation Nord Sahara utilisée au moment de l'import de slabs pour passer en WGZ84 projeté (EPSG:32632)** |
| geom | la géométrie PointZ du SMU en WGS84 projeté (EPSG:32632) |

*champ renseigné au moment de l'import de slabs de fichiers SURPAC

\*\*récupéré du fichier `proj.ini`, voir section [Décalage de projection géographique]

### ctrlcraft.truck_loads

Une entrée est ajoutée à cette table chaque fois qu'un slab est chargé dans un camion.

| champ | description |
| --- | --------------- |
| id | UUID (Universal Unique IDentifier) pour identifier le chargement |
| truck | l'identifiant du camion |
| load_time | l'heure de chargement (heure locale de l'ordinateur) |
| unload_time | l'heure de déchargement (heure locale de l'ordinateur) |

### ctrlcraft.trucks

| champ | description |
| --- | --------------- |
| id | nom du camion |
| loads | la liste (SQL ARRAY) des identifiants de la table `truck_loads` dans ce camion |

\newpage

### ctrlcraft.probe

| champ | description |
| --- | --------- |
| id | UUID (Universal Unique IDentifier) pour identifier la mesure |
| radiometry | la valeur calculée de teneur sonde haute |
| alt | l'altitude reçue du capteur |
| inclination | l'inclinaison reçue du capteur |
| gammaup | la valeur en cps du capteur sonde haute |
| gammadown | la valeur en cps du capteur sonde basse |
| teneurup | la valeur calculée de teneur sonde haute |
| teneurdown | la valeur calculée de teneur sonde basse |
| time | l'heure de mesure reçue du capteur |
| serial_number_gps | le numéro de série du GPS du capteur |
| serial_number_gamma | le numéro de série de la sonde gamma du capteur |
| status | valeur de "calculation" du capteur* |
| coeff_a_up | le coefficient multiplicateur appliqué pour le calcul de la teneurup |
| coeff_a_low | le coefficient multiplicateur appliqué pour le calcul de la teneurlow |
| coeff_b_up | le coefficient puissance appliqué pour pour le calcul de la teneurup |
| coeff_b_low | le coefficient puissance appliqué pour pour le calcul de la teneurlow |
| coeff_n_up | le coefficient de normalisation appliqué pour convertir gammaup en teneurup |
| coeff_n_low | le coefficient de normalisation appliqué pour convertir gammaup en teneurlow |
| geom | la géométrie Point de l'emplacement de la mesure en WGS84 projeté (EPSG:32632) |

*Valeurs possibles : 0 (Undetermined), 1 (GNSS), 6: (Diff), 7: (DGNSS), 15: (Float), 23: (Fixed)

\newpage

### ctrlcraft.fixed_measure

| champ | description |
| --- | ---------- |
| id | UUID (Universal Unique IDentifier) pour identifier la mesure fixe |
| slabid | l'identifiant du slab auquel la mesure fixe est attribuée |
| radioup | la moyenne des mesures de teneurup des lignes de la table `probe` concernées |
| radiolow | la moyenne des mesures de teneurlow des lignes de la table `probe` concernées |
| inclination | la moyenne des mesures de inclination des lignes de la table `probe` concernées |
| modedeg | 1 si la mesure fixe a été effectuée en mode dégradé (voir section [Mode dégradé]) |
| probe_ids | la liste (SQL ARRAY) des identifiants de slabs concernés par cette mesure fixe |
| geom | le centroïde des géométries des lignes de la table `probe` concernées |

### ctrlcraft.co3

| champ | description |
| --- | --- |
| deposit | le gisement* |
| panel | le panneau* |
| blast | le tir* |
| depth | la profondeur* |
| co3 | la valeur de co3* |
| geom | la géométrie PointZ en WGS84 projeté (EPSG:32632)* ** |

*Champ renseigné au moment de l'import d'un fichier de co3

\*\*Translation effectuée avec les valeurs renseignées dans `proj.ini`, voir section [Décalage de projection géographique]

### ctrlcraft.co3_line

Non utilisée dans CtrlCraft. Certainement historique.

| champ | description |
| --- | --- |
| id | UUID (Universal Unique IDentifier) |
| comments | commentaire |
| geom | LineString en WGS84 projeté (EPSG:32632)* ** |

\newpage

### ctrlcraft.settings

Table historique pour stocker des paramètres.

Actuellement utilisée uniquement pour la durée d'affichage des dernières mesures de la table `probe` sur la carte.

La valeur de "probe_interval" est à 20, ce qui affiche les mesures des 20 dernières secondes mesurées.

| champ | description |
| --- | --- |
| name | le nom du paramètre |
| value | la valeur du paramètre |

\newpage

## Vues de la base de données

### ctrlcraft.deposit

Cette vue montre tous les gisements de la table `ctrlcraft.smus`.

| champ | description |
| --- | --- |
| id | le nom du gisement |

### ctrlcraft.panels

Cette vue montre tous les panneaux de la table `ctrlcraft.smus`.

| champ | description |
| --- | --- |
| id | le nom du panneau |
| deposit | le nom du gisement |

### ctrlcraft.blasts

Cette vue montre tous les tirs de la table `ctrlcraft.smus`.

| champ | description |
| --- | --- |
| id | le nom du tir |
| panel | le nom du panneau |
| deposit | le nom du gisement |

\newpage

### ctrlcraft.truck_loads_status

Cette vue montre chaque déchargement de camion qui a eu lieu.

| champ | description |
| --- | ---------- |
| count | le nombre de slabs dans ce camion au moment du déchargement |
| slabs | une liste SQL ARRAY des identifiants des slabs de ce déchargement |
| lithologies | une liste SQL ARRAY des lithologies des slabs de ce déchargement |
| truck | le nom du camion |
| deposit | le nom du gisement |
| blast | le nom du tir |
| panel | le nom du panneau |
| radiometry | la radiométrie estimée moyenne des slabs de ce déchargement |
| radiometry_probe | la radiométrie mesurée moyenne des slabs de ce déchargement |
| quality | la classe (M1, M2, M3, M4, M5, M6, NULL) de radiometry_probe |
| unload_time | l'heure de déchargement |

### ctrlcraft.probe_last_positions

Cette vue montre les mesures à afficher dans la carte de CtrlCraft, correspondant aux
mesures des "probe_interval" dernières secondes (voir [ctrlcraft.settings])

Voir [ctrlcraft.probe] pour la description des champs.

### ctrlcraft.slabs_available

Cette vue montre les slabs à afficher dans la carte de CtrlCraft, correspondant aux
slabs non chargés et avec une radiométrie estimée non nulle.

Voir [ctrlcraft.slabs] pour la description des champs.

\newpage

### ctrlcraft.levels

Cette vue montre les niveaux et est utilisée dans CtrlCraft comme couche de couverture de l'atlas.

| champ | description |
| --- | ---------- |
| centroid_z | l'altitude du niveau |
| id | l'identifiant du niveau |
| lvl | le niveau de 1 à N au sein du tir |
| deposit | le nom du gisement |
| panel | le nom du panneau |
| blast | le nom du tir |
| geom | la géométrie Polygon correspondant à l'enveloppe des slabs de tout le niveau en WGS84 projeté (EPSG:32632) |

### ctrlcraft.topo_control

Cette vue contient une seule ligne.

| champ | description |
| --- | ---------- |
| centroid_z | l'altitude théorique de la slab avec l'altitude la plus élevée de la vue `probe_last_positions` |
| lvl | le niveau de 1 à N correspondant à centroid_z |
| maxlvl | le niveau maximum (N) au sein du tir |
| alt | l'altitude mesurée |
| geom | point de mesure de la table `probe` situé sur avec l'altitude la plus élevée en WGS84 projeté (EPSG:32632) |
| delta | différence alt (mesure) - centroid_z (théorie) |

### ctrlcraft.measurements_slab

Cette vue affiche toutes les slabs qui ont été chargées (mesurées ou non). Un camion avec l'identifiant -1 signifie que la
slab est masquée dans CtrlCraft, mais non chargée effectivement.

| champ | description |
| --- | ---------- |
| slab | l'identifiant de la slab |
| unit | le nom de l'unité auquel la slab appartient* |
| deposit | le nom du gisement auquel la slab appartient |
| panel | le nom du panel auquel la slab appartient |
| blast | le nom du tir auquel la slab appartient |
| lithology | la lithologie de la slab |
| truck | le camion dans lequel la slab a été chargée |
| load_time | l'heure de chargement |
| unload_time | l'heure de déchargement |
| u_krigee | la radiométrie estimée via krigeage |
| co3 | la valeur de co3 sur la zone de cette slab |
| radiometry_probe | la valeur mesurée de radiométrie |
| delta_x | la valeur de translation X utilisée lors de l'import de cette slab dans CtrlCraft (voir section [Décalage de projection géographique])|
| delta_y | idem pour y (voir ci-dessus) |
| delta_z | idem pour z (voir ci-dessus) |
| x_wgs84 | coordonnées X en WGS84 projeté (EPSG:32632) du centroïde de la slab |
| y_wgs84 | idem pour Y (voir ci-dessus) |
| z_wgs84 | idem pour Z (voir ci-dessus) |
| lon_wgs84 | longitude en WGS84 non projeté du centroïde de la slab |
| lat_wgs84 | latitude en WGS84 non projeté du centroïde de la slab  |
| x_meas | coordonnées X en WGS84 projeté (EPSG:32632) du centroïde des points de mesure |
| y_meas | idem pour Y (voir ci-dessus) |
| z_meas | idem pour Z (voir ci-dessus) |
| lon_meas | longitude en WGS84 non projeté du centroïde des points de mesure |
| lat_meas | latitude en WGS84 non projeté du centroïde des points de mesure |
| nb_meas | nombre de mesures uniques utilisées pour calculer la radiométrie de la slab |
| date_last | date et heure de la première mesure utilisée pour calculer la radiométrie de la slab |
| date_first | date et heure de la dernière mesure utilisée pour calculer la radiométrie de la slab |
| cps_low | moyenne des mesures sonde basse (coups par seconde) effectuées sur cette slab |
| cps_up | moyenne des mesures sonde haute (coups par seconde) effectuées sur cette slab |
| affectedradio | texte sous la forme "N-M" où N et le nombre de mesures fixes effectuées avec la sonde haute et M avec la sonde basse |
| a_low | le coefficient multiplicateur appliqué pour le calcul de la teneurlow |
| a_up | le coefficient multiplicateur appliqué pour le calcul de la teneurup |
| b_low | le coefficient puissance appliqué pour pour le calcul de la teneurlow |
| b_up | le coefficient puissance appliqué pour pour le calcul de la teneurup |
| n_low | le coefficient de normalisation appliqué pour convertir gammaup en teneurlow |
| n_up | le coefficient de normalisation appliqué pour convertir gammaup en teneurup |
| serial_number_probe | liste des numéros de série des sondes gamma utilisées pour les mesures sur cette slab |
| serial_number_cube | liste des numéros de série des GPS utilisées pour les mesures sur cette slab |

### ctrlcraft.measurements_slab_not_loaded

Cette vue affiche toutes les slabs qui ont été mesurés et non chargés. Les champs sont identiques à la vue [ctrlcraft.measurements_slab].
