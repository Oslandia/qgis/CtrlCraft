.. CtrlCraft documentation master file, created by
   sphinx-quickstart on Fri Apr 14 15:14:43 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CtrlCraft's technical documentation
===================================

`Go back to the main documentation <https://oslandia.gitlab.io/qgis/CtrlCraft>`__

:download:`Download the technical documentation PDF (French) <../doc_tech.pdf>`

Python API documentation
------------------------

.. toctree::
   :maxdepth: 3
   :caption: Modules:

   modules


Indices and tables
..................

* :ref:`genindex`
* :ref:`modindex`
